*** Settings ***
Resource    ../resources/constants.robot
Library    ../py_sources/logigear/api/BuildingApi.py    ${USERNAME}    ${PASSWORD}
Library    ../py_sources/logigear/api/CampusApi.py    

Suite Setup    Run Keyword    Log    Suite setup
    
Suite Teardown    Run Keyword    Log    Suite teardown


Test Setup    Run Keywords    Log    General test setup
    ...    AND    Run Keyword If    '${PREV_TEST_STATUS}' == 'FAIL'    Fatal Error    Precondition test failed
   
Test Teardown    Run Keywords    Run Keyword If    '${PREV_TEST_STATUS}' == 'FAIL'    Fatal Error    Skip
    ...    AND    Log    General test teardown
    ...    AND    Delete Campus    Lam Campus Test

*** Test Cases ***   
Test case 1
    Delete Building    Lam Test
    ${buildingID}=    Add New Building    Lam Building Test 
    ${rs}=    Get Buildings
    Delete Building    idValue=${buildingID}   
    ${campusID}=    Add New Campus    Lam Campus Test
    ${rs}=    Get Campuses
