*** Settings ***
Library    OperatingSystem
Library    ../../py_sources/logigear/page_objects/system_manager/installation/UninstallPageDesktop.py
Library   ../../py_sources/logigear/setup.py    

*** Variables ***
${locationBuild}    \\\\192.168.170.216\\Quareo build share\\imVision_System_Manager_9.3.0.3223
${settingSNMPLocation}    C:\\Program Files\\CommScope\\imVision System Manager\\Services\\SNMPServices\\Settings.xml

*** Test Cases ***
Install SM Core By Commandline
    Uninstall Sm Build
    Delete Sm Database    IMVISION
    Install Sm By Cmd    ${locationBuild}
    Replace Substring In Text File    ${settingSNMPLocation}    <TestHarness>0</TestHarness>    <TestHarness>4</TestHarness>
    Copy Directory    \\\\data-server\\LogiGearVN\\Systimax Automation\\8 - Setup\\TestData    C:\\Program Files\\CommScope\\imVision System Manager\\Services\\SNMPServices    
    Restart Service    imVision SNMP Server
    Start Service    World Wide Web Publishing Service