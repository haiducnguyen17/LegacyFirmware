*** Settings ***

Resource    ../../../../resources/constants.robot
Resource    ../../../../resources/icons_constants.robot
Library   ../../../../py_sources/logigear/setup.py  
Library   ../../../../py_sources/logigear/page_objects/system_manager/sm_simulator/SmSimulatorPage${PLATFORM}.py

Suite Setup    Run Keyword    Open Sm Simulator Page
Suite Teardown    Close Browser

*** Variables ***
${IP_ADD}    127.0.0.1
${RACK_NAME}    A03102AD
${PANEL_NAME}    7212100045B2

${Value}    Value
${Communication}    Communication
${IP}    IP
${FWVersion}    FWVersion
${NMFWVersion}    NMFWVersion
${RackSerialNumber}    RackSerialNumber
${RackNumber}    RackNumber
${Status}    Status
${DisplayNumber}    DisplayNumber
${ControllerType}    ControllerType
${NM IP Address}    NM IP Address
${SerialNumber}    SerialNumber
${Order}    Order
${Display}    Display
${PanelType}    PanelType
${Managed By}    Managed By
${Total Panels}    Total Panels
${Physical Order}    Physical Order
${Manage}    Manage
${Add}    Add   
 
*** Test Cases ***
ISS-59-04 Verify that user is able to add object into left tree
    # 1. Launch imVision Simulator page
    Delete Tree Node If Exist On Sm Simulator     ${SITE_NODE}/${IP_ADD}
    
    # 2. Highlight Site on left pane, select Manager > Add
    Click Tree Node On Sm Simulator    ${SITE_NODE}
    Select Sub Menu Item    ${Manage}    ${Add}    

    # 3. Observe the result
    # *Step 3: "Add children Zone of Site" window is displayed
    Check Fw Version Textbox Exist
    
    # 4. Input valid value into "Add children Zone of Site" window:
        # _IP: 127.0.0.1
        # _FWVersion: V1.0
        # _NMVersion: NMVer 2.0
        # _Communication: SOCKET
    Add Children For Object Site    ${IP_ADD}    V1.0    NMVer 2.0    SOCKET
    
    # 5. Click "Submit" button and observe the result
    # *Step 5: Zone IP 127.0.0.1 is added into left tree
    Check Tree Node Exist On Sm Simulator    ${SITE_NODE}/${IP_ADD}    
    
    # 6. Select Zone IP is added above and select Manage > Add
    Click Tree Node On Sm Simulator    ${SITE_NODE}/${IP_ADD}
    Select Sub Menu Item    ${Manage}    ${Add}

    # 7. Observe the result
    # *Step 7: Add children Rack of 127.0.0.1 is displayed
    Check Serial Number Textbox Exist
    
    # 8. Enter valid value into "Add children Panel of A03102AD" window or let default value
    Click Add Children Submit Button
   
    # 9. Observe the result
    # *Step 10: Rack: A03102AD is added into Zone IP
    # Check Tree Node Exist On Sm Simulator    ${SITE_NODE}/${IP_ADD}/${RACK_NAME} 
    
    # 10. Select Rack added above then select Managed > Add
    Click Tree Node On Sm Simulator    ${SITE_NODE}/${IP_ADD}/${RACK_NAME}
    Select Sub Menu Item    ${Manage}    ${Add} 
    
    # 11. Observe the result
    # *Step 11: "Add children Panel of A03102AD" is displayed
    Check Panel Serial Number Textbox Exist
    
    # 12. Enter valid value into "Add children Rack of 127.0.0.1" window or let default value (e.g. Serial number: A03102AD)
    Click Add Children Submit Button
    
    # 13. Observe the result
    # *Step 13: Panel is added to Rack A03102AD
    Check Tree Node Exist On Sm Simulator    ${SITE_NODE}/${IP_ADD}/${RACK_NAME}/${PANEL_NAME}
    
ISS-59-06 Verify that content pane and properties pane shows data for selected zone IP
    # 1. Launch imVision Simulator page
    # 2. Click Site and observe the result
    # _Content pane also shows the communication types for each IP address.
    Click Tree Node On Sm Simulator    ${SITE_NODE}
    Check Table Row Map With Header On Sm Simulator Content Table    ${Communication},${IP},${FWVersion},${NMFWVersion}    SOCKET,127.0.0.1,V1.0,NMVer 2.0    3
    
    # 3. Click to Zone IP and observe the result
    # _ Content pane shows racks' information under the zone.
    # _ Property pane shows the property of the selected zone.
    Click Tree Node On Sm Simulator    ${SITE_NODE}/${IP_ADD}
    Check Table Row Map With Header On Sm Simulator Content Table    ${RackSerialNumber},${RackNumber},${Status},${DisplayNumber},${ControllerType}    ${RACK_NAME},1,Ok,1,imVision Controller X    1
    Check Table Row Map With Header On Sm Simulator Properties Table    ${Name},${Value}    ${Communication},SOCKET    1
    Check Table Row Map With Header On Sm Simulator Properties Table    ${Name},${Value}    ${NM IP Address},${IP_ADD}    2
    
    # 4. Expand Zone IP then select Rack under zone and observer the result
    # _ Content pane shows panels' information under the zone.
    # _ Property pane shows the property of the selected rack.
    Click Tree Node On Sm Simulator    ${SITE_NODE}/${IP_ADD}/${RACK_NAME}
    Check Table Row Map With Header On Sm Simulator Content Table    ${SerialNumber},${Status},${Order},${Display},${PanelType}    ${PANEL_NAME},Ok,1 - 1,1,1100 24port    1
    Check Table Row Map With Header On Sm Simulator Properties Table    ${Name},${Value}    ${Managed By},0x${RACK_NAME}    2
    Check Table Row Map With Header On Sm Simulator Properties Table    ${Name},${Value}    ${Total Panels},1    3
    
    # 5. Expand Rack then select Panel under Rack and observe the result 
    # _ Content pane shows ports' information under the zone.
    # _ Property pane shows the property of the selected panel.
    Click Tree Node On Sm Simulator    ${SITE_NODE}/${IP_ADD}/${RACK_NAME}/${PANEL_NAME}
    Check Table Row Map With Header On Sm Simulator Properties Table    ${Name},${Value}    ${Physical Order},1    2
    Check Table Row Map With Header On Sm Simulator Properties Table    ${Name},${Value}    ${SerialNumber},${PANEL_NAME}    4  
    
ISS-59-05 Verify that user is able to delete object into left tree
    # 1. Launch imVision Simulator page
    # 2. Select Panel under Rack
    # 3. Select Managed > Delete
    # 4. Click [OK] button on warning popup displays
    Delete Tree Node On Sm Simulator    ${SITE_NODE}/${IP_ADD}/${RACK_NAME}/${PANEL_NAME}
    
    # 5. Observe the result
    # Object is deleted successfully
    Check Tree Node Not Exist On Sm Simulator    ${SITE_NODE}/${IP_ADD}/${RACK_NAME}/${PANEL_NAME}
    
    # 6. Select Rack under Zone IP
    # 7. Select Managed > Delete
    # 8. Click [OK] button on warning popup displays
    Delete Tree Node On Sm Simulator    ${SITE_NODE}/${IP_ADD}/${RACK_NAME}
    
    # 9. Observe the result
    # Object is deleted successfully
    Check Tree Node Not Exist On Sm Simulator    ${SITE_NODE}/${IP_ADD}/${RACK_NAME}
    
    # 10. Select Zone IP under Site
    # 11. Select Managed > Delete
    # 12. Click [OK] button on warning popup displays
    Delete Tree Node On Sm Simulator    ${SITE_NODE}/${IP_ADD}
    
    # 13. Observe the result    
    # Object is deleted successfully
    Check Tree Node Not Exist On Sm Simulator    ${SITE_NODE}/${IP_ADD}