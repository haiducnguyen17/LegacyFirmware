*** Settings ***
Resource    ../../../../resources/icons_constants.robot
Resource    ../../../../resources/constants.robot
Library    ../../../../py_sources/logigear/setup.py    
Library    ../../../../py_sources/logigear/page_objects/system_manager/sm_simulator/SmSimulatorPage${PLATFORM}.py  

Suite Setup    Open Sm Simulator Page

Suite Teardown    Close Browser

Default Tags    imVision Simulator

*** Variables ***
${MAIN_MENU_MANAGE}    Manage
${MAIN_MENU_TEMPLATE}    Template
${MAIN_MENU_SYNCHRONIZE}    Synchronize
${MAIN_MENU_HELP}    Help
${SUB_MENU_ADD}    Add
${SUB_MENU_DELETE}    Delete
${SUB_MENU_UPLOAD}    Upload
${SUB_MENU_DOWNLOAD}    Download
${OBJECTS_TAB}    Objects
${ALARMS_TAB}    Alarms
${JOBS_TAB}    Jobs

*** Test Cases ***
ISS-59_01_Verify that there are 3 main panes for imVision Simulator page
    
    [Tags]    imVision Sanity
    
    # 1. Launch imVision Simulator page
  	# 2. Observe the main page
	# VP:
	# *Step 2: There are 3 panes: 
	# _Left tree (Site tree) shows all the zones' IP addresses.
	# _Content pane shows 3 tabs
	# _Properties pane
	Check Sim Site Tree Exist
	Check Sim Contents Pane Exist
	Check Sim Properties Pane Exist
	
ISS-59_02_Verify that there are 4 items on header menu     
        
    [Tags]    imVision Sanity
    
    # 1. Launch imVision Simulator page
    # 2. Observe the header of page
    # VP:  There are 4 items on header of page: Manage, Template, Synchronize, Help 
    Check Dropdown Menu Item Exist On Main Menu    ${MAIN_MENU_MANAGE}
    Check Dropdown Menu Item Exist On Main Menu    ${MAIN_MENU_TEMPLATE}
    Check Menu Item Exist On Main Menu    ${MAIN_MENU_SYNCHRONIZE}
    Check Menu Item Exist On Main Menu    ${MAIN_MENU_HELP}
    
    # 3. Mouse-over on "Manage" and "Template" menu item and observe the sub-items display
    # VP:*Step 3: Each Item have sub-items as below:
    # Manage > Add, Delete
    # Template > Upload, Download
    Check Sub Menu Item Exist    ${MAIN_MENU_MANAGE}    ${SUB_MENU_ADD}
    Check Sub Menu Item Exist    ${MAIN_MENU_MANAGE}    ${SUB_MENU_DELETE}
    Check Sub Menu Item Exist    ${MAIN_MENU_TEMPLATE}    ${SUB_MENU_UPLOAD}
    Check Sub Menu Item Exist    ${MAIN_MENU_TEMPLATE}    ${SUB_MENU_DOWNLOAD}

ISS-59_03_Verify that there are 3 tabs on contents pane
        
    [Tags]    imVision Sanity
    
    # 1. Launch imVision Simulator page
	# 2. Observe the contents pane
	# VP:Content pane shows 3 tabs: Objects, Alarms, Jobs.
	# 3. Switch between three tabs
	# VP:User can switch around between 3 tabs
	Select Sim Contents Tab    ${OBJECTS_TAB}
	Select Sim Contents Tab    ${ALARMS_TAB}
	Select Sim Contents Tab    ${JOBS_TAB}
	
ISS-59_07_Verify that user can go to Help page
    
    [Tags]    imVision Sanity
    
    # 1. Launch imVision Simulator page
	# 2. Click on "Help" on Header menu
	# 3. observe the result
	# VP: Step 3: Help page is opened in new tab
	Select Menu Item On Main Menu    ${MAIN_MENU_HELP}
	Switch Window    NEW
	Check Current Window Title Contain    Imvision Simulator RESTful API documentation