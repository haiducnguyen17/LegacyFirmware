*** Settings ***
Library   ../../../py_sources/logigear/setup.py        
Library   ../../../py_sources/logigear/page_objects/system_manager/site_manager/SiteManagerPage${PLATFORM}.py
Library   ../../../py_sources/logigear/page_objects/system_manager/login/LoginPage${PLATFORM}.py
Library    DataDriver	file=../../../test-data/snmpupdate/DataSNMPUpdate.xls    sheet_name=ver9_9.1E_9.2A
Resource    ../../../resources/constants.robot     

Suite Setup    Run Keywords    Open SM Login Page
    ...    AND    Login To SM    ${USERNAME}    ${PASSWORD}
    
Suite Teardown    Close Browser

Test Template     Check SNMPUpdate Switch

*** Test Cases ***   
Check SNMP Update Manage Switches version 9 with the switch is in ${treenode} and object ${objectname} exist    UserData    UserData

*** Keywords ***
Check SNMPUpdate Switch
	[Arguments]    ${treenode}    ${objectname}
	
    Check Object Exist On Content Table    ${treenode}    ${objectname}