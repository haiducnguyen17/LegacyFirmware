*** Settings ***
Library   ../../../../../py_sources/logigear/setup.py
Resource    ../../../../../resources/constants.robot

Library   ../../../../../py_sources/logigear/api/BuildingApi.py    ${USERNAME}    ${PASSWORD}
Library   ../../../../../py_sources/logigear/api/CampusApi.py                     
Library   ../../../../../py_sources/logigear/page_objects/system_manager/site_manager/SiteManagerPage${PLATFORM}.py    
Library   ../../../../../py_sources/logigear/page_objects/system_manager/login/LoginPage${PLATFORM}.py
Library   ../../../../../py_sources/logigear/page_objects/system_manager/administration/AdministrationPage${PLATFORM}.py    
Library    ../../../../../py_sources/logigear/page_objects/system_manager/administration/spaces/AdmGeomapOptionsPage${PLATFORM}.py
Library    ../../../../../py_sources/logigear/page_objects/system_manager/zone_header/ZoneHeaderPage${PLATFORM}.py
Library    ../../../../../py_sources/logigear/page_objects/system_manager/administration/spaces/AdmLayerImageFilesPage${PLATFORM}.py     
Library    ../../../../../py_sources/logigear/page_objects/system_manager/site_manager/spaces/ReplaceImagePage${PLATFORM}.py 

Default Tags    Spaces
Force Tags    SM-30285

Suite Setup    Run Keywords    Open SM Login Page
    ...    AND    Login To SM    ${USERNAME}    ${PASSWORD}
    ...    AND    Select Main Menu    ${Administration}/${Spaces}    ${Layer Image Files}
    ...    AND    Delete Layer Image Files    ${SM_30285_IMAGE_01}
    ...    AND    Delete Layer Image Files    ${SM_30285_IMAGE_02} 
    ...    AND    Delete Layer Image Files    ${SM_30285_IMAGE_03}
    ...    AND    Add Layer Image Files    ${SM_30285_IMAGE_01}    ${SM_30285_LOCATION_01}
    ...    AND    Add Layer Image Files    ${SM_30285_IMAGE_02}    ${SM_30285_LOCATION_02}
    ...    AND    Add Layer Image Files    ${SM_30285_IMAGE_03}    ${SM_30285_LOCATION_03} 

*** Variables ***
${SM_30285_IMAGE_01}    Layer_SM_30285_01
${SM_30285_IMAGE_02}    Layer_SM_30285_02
${SM_30285_IMAGE_03}    AutoCAD_Layer_SM_30285
${SM_30285_LOCATION_01}    C:\\Pre Condition\\Layers\\logigear_building.jpg
${SM_30285_LOCATION_02}    C:\\Pre Condition\\Layers\\logigear_bughunting.jpg
${SM_30285_LOCATION_03}    C:\\Pre Condition\\Layers\\848102521_2000.dwg
${NUMBER_BETWEEN_1_AND_1000}    Enter a number between 1 and 1000 for real length.
${city_name}    CT_SM30285_01
${campus_name}    CP_SM30285_01
${building_name}    BD_SM30285_01
${cable_vault_name}    CV_SM30285_01
${floor_name}    F_SM30285_01    
${room_name}    R_SM30285_01
${cubicle_name}    CB_SM30285_01
${rack_name}    Rack_SM30285_01
${rack_name_2}    Rack_SM30285_02
${position_rack_name}    1:1 ${rack_name}
${position_rack_name_2}    1:2 ${rack_name_2}     
    
*** Test Cases ***
SM-30285-01_Verify user is able to set scale for City space
    [Setup]    Run Keywords    Open SM Login Page
    ...    AND    Login To SM    ${USERNAME}    ${PASSWORD}
    ...    AND    Delete Tree Node If Exist On Site Manager    ${SITE_NODE}/${city_name}
                  
    [Teardown]    Run Keywords    Delete Tree Node On Site Manager     ${SITE_NODE}/${city_name}
    ...    AND    Close Browser
       
	# 1. Launch and log into SM Web
	# 2. Go to Administration > Spaces > Layer Image Files
    # 3. Add two layer image files (non-autocad files) 
    # 4. Go to "Site Manager" page
    Select Main Menu    ${Site Manager}	
    
	# 5. Add City
	Add City    ${SITE_NODE}    ${city_name}
    	
	# 6. Select City and change to Spaces view
	Click Tree Node On Site Manager    ${SITE_NODE}/${city_name}
	Go To Another View On Site Manager    ${Spaces}
	
    # 7. Observe the result
    # * Step 7: The Floating button is hidden
    Check Floating Button Not Exist On Space

	# 8. Click Unlock button
	Toggle Spaces View Lock State    state=Unlock

	# 9. Assign layer image and observe
	Assign Layer Image To Object    ${SITE_NODE}/${city_name}    ${SM_30285_IMAGE_01}
	
    # * Step 9: The Floating button is shown	
    Check Floating Button Exist On Space

	# 10. Expand Floating box then click Scale Setting
	Open Scale Setting
	
	# * Step 10: Scale value box is displayed:
	Check Scale Value Box Exist On Space
	# _There are 2 points to define real length in a floating box with length value and unit (Meter/Feet)
	Check Scale Setting Unit    m
	Switch Scale Setting Unit    ft
	Check Scale Setting Unit    ft
	
	# _The Scale button show an editing mode icon
	Check Save Close Scale Button Exist On Space
	
	# 11. Enter length less 1 and greater 1000
	# * Step 11: Dialog message "Enter a number between 1 and 1000 for real length." appear
	Set Scale Value    0    
	Check Popup Error Message    ${NUMBER_BETWEEN_1_AND_1000}    yes
	Set Scale Value    1001
	Check Popup Error Message    ${NUMBER_BETWEEN_1_AND_1000}    yes  

	# 12. Change unit
	# * Step 12: User is able to change unit
	# 13. Enter length between 1 and 1000, save and click to show ruler, observe the result
	# * Step 13: 
	# - The setting is automatically saved
	# - The space ruler will update when saving the length data
    Set Scale Value    100    m 
    
	# 14. Select scale setting, and close Floating box, and open the box again and select scale setting, observe the result
	# * Step 14: The saved data is still displayed
	Collapse Floating Box On Spaces
	Open Scale Setting
	Check Scale Setting Value    100
	Check Scale Setting Unit    m
	
	# 15. Assign another layer image 
	Assign Layer Image To Object    ${SITE_NODE}/${city_name}    ${SM_30285_IMAGE_02}

	# 16. Open the box again and select scale setting, observe the result
	Open Scale Setting
	
	# * Step 16: The data is reset.
    Check Scale Setting Value    ${EMPTY}
    
SM-30285-02_Verify user is able to set scale for Campus space
    [Setup]    Run Keywords    Delete Campus    ${campus_name}
    ...    AND    Add New Campus    ${campus_name}
    ...    AND    Open SM Login Page        
    ...    AND    Login To SM    ${USERNAME}    ${PASSWORD}
                  
    [Teardown]    Run Keywords    Close Browser    
    ...    AND    Delete Campus    ${campus_name}
       
	# 1. Launch and log into SM Web
	# 2. Go to Administration > Spaces > Layer Image Files
    # 3. Add two layer image files (non-autocad files) 
    # 4. Go to "Site Manager" page
    Select Main Menu    ${Site Manager}	
    
	# 5. Add Campus	
	# 6. Select City and change to Spaces view
	Click Tree Node On Site Manager    ${SITE_NODE}/${campus_name}
	Go To Another View On Site Manager    ${Spaces}
	
    # 7. Observe the result
    # * Step 7: The Floating button is hidden
    Check Floating Button Not Exist On Space

	# 8. Click Unlock button
	Toggle Spaces View Lock State    state=Unlock

	# 9. Assign layer image and observe
	Assign Layer Image To Object    ${SITE_NODE}/${campus_name}    ${SM_30285_IMAGE_01}
	
    # * Step 9: The Floating button is shown	
    Check Floating Button Exist On Space

	# 10. Expand Floating box then click Scale Setting
	Open Scale Setting
	
	# * Step 10: Scale value box is displayed:
	Check Scale Value Box Exist On Space
	# _There are 2 points to define real length in a floating box with length value and unit (Meter/Feet)
	Check Scale Setting Unit    m
	Switch Scale Setting Unit    ft
	Check Scale Setting Unit    ft
	
	# _The Scale button show an editing mode icon
	Check Save Close Scale Button Exist On Space
	
	# 11. Enter length less 1 and greater 1000
	# * Step 11: Dialog message "Enter a number between 1 and 1000 for real length." appear
	Set Scale Value    0    
	Check Popup Error Message    ${NUMBER_BETWEEN_1_AND_1000}    yes
	Set Scale Value    1001
	Check Popup Error Message    ${NUMBER_BETWEEN_1_AND_1000}    yes  

	# 12. Change unit
	# * Step 12: User is able to change unit
	# 13. Enter length between 1 and 1000, save and click to show ruler, observe the result
	# * Step 13: 
	# - The setting is automatically saved
	# - The space ruler will update when saving the length data
    Set Scale Value    100    m 
    
	# 14. Select scale setting, and close Floating box, and open the box again and select scale setting, observe the result
	# * Step 14: The saved data is still displayed
	Collapse Floating Box On Spaces
	Open Scale Setting
	Check Scale Setting Value    100
	Check Scale Setting Unit    m
	
	# 15. Assign another layer image 
	Assign Layer Image To Object    ${SITE_NODE}/${campus_name}    ${SM_30285_IMAGE_02}

	# 16. Open the box again and select scale setting, observe the result
	Open Scale Setting
	
	# * Step 16: The data is reset.
    Check Scale Setting Value    ${EMPTY}
    
SM-30285-03_Verify user is able to set scale for Building space
    [Setup]    Run Keywords    Delete Building     ${building_name}
    ...    AND    Add New Building    ${building_name}
    ...    AND    Open SM Login Page
    ...    AND    Login To SM    ${USERNAME}    ${PASSWORD}
                  
    [Teardown]    Run Keywords    Close Browser    
    ...    AND    Delete Building     ${building_name}
       
	# 1. Launch and log into SM Web
	# 2. Go to Administration > Spaces > Layer Image Files
    # 3. Add two layer image files (non-autocad files) 
    # 4. Go to "Site Manager" page
    Select Main Menu    ${Site Manager}	
    
	# 5. Add Building	
	# 6. Select City and change to Spaces view
	Click Tree Node On Site Manager    ${SITE_NODE}/${building_name}
	Go To Another View On Site Manager    ${Spaces}
	
    # 7. Observe the result
    # * Step 7: The Floating button is hidden
    Check Floating Button Not Exist On Space

	# 8. Click Unlock button
	Toggle Spaces View Lock State    state=Unlock

	# 9. Assign layer image and observe
	Assign Layer Image To Object    ${SITE_NODE}/${building_name}    ${SM_30285_IMAGE_01}
	
    # * Step 9: The Floating button is shown	
    Check Floating Button Exist On Space

	# 10. Expand Floating box then click Scale Setting
	Open Scale Setting
	
	# * Step 10: Scale value box is displayed:
	Check Scale Value Box Exist On Space
	# _There are 2 points to define real length in a floating box with length value and unit (Meter/Feet)
	Check Scale Setting Unit    m
	Switch Scale Setting Unit    ft
	Check Scale Setting Unit    ft
	
	# _The Scale button show an editing mode icon
	Check Save Close Scale Button Exist On Space
	
	# 11. Enter length less 1 and greater 1000
	# * Step 11: Dialog message "Enter a number between 1 and 1000 for real length." appear
	Set Scale Value    0    
	Check Popup Error Message    ${NUMBER_BETWEEN_1_AND_1000}    yes
	Set Scale Value    1001
	Check Popup Error Message    ${NUMBER_BETWEEN_1_AND_1000}    yes  

	# 12. Change unit
	# * Step 12: User is able to change unit
	# 13. Enter length between 1 and 1000, save and click to show ruler, observe the result
	# * Step 13: 
	# - The setting is automatically saved
	# - The space ruler will update when saving the length data
    Set Scale Value    100    m 
    
	# 14. Select scale setting, and close Floating box, and open the box again and select scale setting, observe the result
	# * Step 14: The saved data is still displayed
	Collapse Floating Box On Spaces
	Open Scale Setting
	Check Scale Setting Value    100
	Check Scale Setting Unit    m
	
	# 15. Assign another layer image 
	Assign Layer Image To Object    ${SITE_NODE}/${building_name}    ${SM_30285_IMAGE_02}

	# 16. Open the box again and select scale setting, observe the result
	Open Scale Setting
	
	# * Step 16: The data is reset.
    Check Scale Setting Value    ${EMPTY}

SM-30285-04_Verify user is able to set scale for Cable Vault space
    [Setup]    Run Keywords    Open SM Login Page
    ...    AND    Login To SM    ${USERNAME}    ${PASSWORD}
    ...    AND    Delete Tree Node If Exist On Site Manager    ${SITE_NODE}/${cable_vault_name}
                  
    [Teardown]    Run Keywords    Delete Tree Node On Site Manager     ${SITE_NODE}/${cable_vault_name}
    ...    AND    Close Browser
       
	# 1. Launch and log into SM Web
	# 2. Go to Administration > Spaces > Layer Image Files
    # 3. Add two layer image files (non-autocad files) 
    # 4. Go to "Site Manager" page
    Select Main Menu    ${Site Manager}	
    
	# 5. Add Cable Vault
	Add Cable Vault    ${SITE_NODE}    ${cable_vault_name}
    	
	# 6. Select City and change to Spaces view
	Click Tree Node On Site Manager    ${SITE_NODE}/${cable_vault_name}
	Go To Another View On Site Manager    ${Spaces}
	
    # 7. Observe the result
    # * Step 7: The Floating button is hidden
    Check Floating Button Not Exist On Space

	# 8. Click Unlock button
	Toggle Spaces View Lock State    state=Unlock

	# 9. Assign layer image and observe
	Assign Layer Image To Object    ${SITE_NODE}/${cable_vault_name}    ${SM_30285_IMAGE_01}
	
    # * Step 9: The Floating button is shown	
    Check Floating Button Exist On Space

	# 10. Expand Floating box then click Scale Setting
	Open Scale Setting
	
	# * Step 10: Scale value box is displayed:
	Check Scale Value Box Exist On Space
	# _There are 2 points to define real length in a floating box with length value and unit (Meter/Feet)
	Check Scale Setting Unit    m
	Switch Scale Setting Unit    ft
	Check Scale Setting Unit    ft
	
	# _The Scale button show an editing mode icon
	Check Save Close Scale Button Exist On Space
	
	# 11. Enter length less 1 and greater 1000
	# * Step 11: Dialog message "Enter a number between 1 and 1000 for real length." appear
	Set Scale Value    0    
	Check Popup Error Message    ${NUMBER_BETWEEN_1_AND_1000}    yes
	Set Scale Value    1001
	Check Popup Error Message    ${NUMBER_BETWEEN_1_AND_1000}    yes  

	# 12. Change unit
	# * Step 12: User is able to change unit
	# 13. Enter length between 1 and 1000, save and click to show ruler, observe the result
	# * Step 13: 
	# - The setting is automatically saved
	# - The space ruler will update when saving the length data
    Set Scale Value    100    m 
    
	# 14. Select scale setting, and close Floating box, and open the box again and select scale setting, observe the result
	# * Step 14: The saved data is still displayed
	Collapse Floating Box On Spaces
	Open Scale Setting
	Check Scale Setting Value    100
	Check Scale Setting Unit    m
	
	# 15. Assign another layer image 
	Assign Layer Image To Object    ${SITE_NODE}/${cable_vault_name}    ${SM_30285_IMAGE_02}

	# 16. Open the box again and select scale setting, observe the result
	Open Scale Setting
	
	# * Step 16: The data is reset.
    Check Scale Setting Value    ${EMPTY}
    
SM-30285-05_Verify user is able to set scale for Floor space
    [Setup]    Run Keywords    Delete Building    ${building_name}
    ...    AND    Add New Building    ${building_name}      
    ...    AND    Open SM Login Page
    ...    AND    Login To SM    ${USERNAME}    ${PASSWORD}   
                  
    [Teardown]    Run Keywords    Close Browser    
    ...    AND    Delete Building    ${building_name}
       
	# 1. Launch and log into SM Web
	# 2. Go to Administration > Spaces > Layer Image Files
    # 3. Add two layer image files (non-autocad files) 
    # 4. Go to "Site Manager" page
    Select Main Menu    ${Site Manager}	
    
	# 5. Add Floor
	Add Floor    ${SITE_NODE}/${building_name}    ${floor_name}    
	
	# 6. Select City and change to Spaces view
	Click Tree Node On Site Manager    ${SITE_NODE}/${building_name}/${floor_name}
	Go To Another View On Site Manager    ${Spaces}
	
    # 7. Observe the result
    # * Step 7: The Floating button is hidden
    Check Floating Button Not Exist On Space

	# 8. Click Unlock button
	Toggle Spaces View Lock State    state=Unlock

	# 9. Assign layer image and observe
	Assign Layer Image To Object    ${SITE_NODE}/${building_name}/${floor_name}    ${SM_30285_IMAGE_01}
	
    # * Step 9: The Floating button is shown	
    Check Floating Button Exist On Space

	# 10. Expand Floating box then click Scale Setting
	Open Scale Setting
	
	# * Step 10: Scale value box is displayed:
	Check Scale Value Box Exist On Space
	# _There are 2 points to define real length in a floating box with length value and unit (Meter/Feet)
	Check Scale Setting Unit    m
	Switch Scale Setting Unit    ft
	Check Scale Setting Unit    ft
	
	# _The Scale button show an editing mode icon
	Check Save Close Scale Button Exist On Space
	
	# 11. Enter length less 1 and greater 1000
	# * Step 11: Dialog message "Enter a number between 1 and 1000 for real length." appear
	Set Scale Value    0    
	Check Popup Error Message    ${NUMBER_BETWEEN_1_AND_1000}    yes
	Set Scale Value    1001
	Check Popup Error Message    ${NUMBER_BETWEEN_1_AND_1000}    yes  

	# 12. Change unit
	# * Step 12: User is able to change unit
	# 13. Enter length between 1 and 1000, save and click to show ruler, observe the result
	# * Step 13: 
	# - The setting is automatically saved
	# - The space ruler will update when saving the length data
    Set Scale Value    100    m 
    
	# 14. Select scale setting, and close Floating box, and open the box again and select scale setting, observe the result
	# * Step 14: The saved data is still displayed
	Collapse Floating Box On Spaces
	Open Scale Setting
	Check Scale Setting Value    100
	Check Scale Setting Unit    m
	
	# 15. Assign another layer image 
	Assign Layer Image To Object    ${SITE_NODE}/${building_name}/${floor_name}    ${SM_30285_IMAGE_02}

	# 16. Open the box again and select scale setting, observe the result
	Open Scale Setting
	
	# * Step 16: The data is reset.
    Check Scale Setting Value    ${EMPTY}
    
SM-30285-06_Verify user is able to set scale for Room space
    [Setup]    Run Keywords    Delete Building    ${building_name}
    ...    AND    Add New Building    ${building_name}     
    ...    AND    Open SM Login Page
    ...    AND    Login To SM    ${USERNAME}    ${PASSWORD} 
                  
    [Teardown]    Run Keywords    Close Browser
    ...    AND    Delete Building    ${building_name}
       
	# 1. Launch and log into SM Web
	# 2. Go to Administration > Spaces > Layer Image Files
    # 3. Add two layer image files (non-autocad files) 
    # 4. Go to "Site Manager" page
    Select Main Menu    ${Site Manager}	
    
	# 5. Add Room
	Add Floor    ${SITE_NODE}/${building_name}    ${floor_name}
	Add Room    ${SITE_NODE}/${building_name}/${floor_name}    ${room_name}    
	
	# 6. Select City and change to Spaces view
	Click Tree Node On Site Manager    ${SITE_NODE}/${building_name}/${floor_name}/${room_name}
	Go To Another View On Site Manager    ${Spaces}
	
    # 7. Observe the result
    # * Step 7: The Floating button is hidden
    Check Floating Button Not Exist On Space

	# 8. Click Unlock button
	Toggle Spaces View Lock State    state=Unlock

	# 9. Assign layer image and observe
	Assign Layer Image To Object    ${SITE_NODE}/${building_name}/${floor_name}/${room_name}    ${SM_30285_IMAGE_01}
	
    # * Step 9: The Floating button is shown	
    Check Floating Button Exist On Space

	# 10. Expand Floating box then click Scale Setting
	Open Scale Setting
	
	# * Step 10: Scale value box is displayed:
	Check Scale Value Box Exist On Space
	# _There are 2 points to define real length in a floating box with length value and unit (Meter/Feet)
	Check Scale Setting Unit    m
	Switch Scale Setting Unit    ft
	Check Scale Setting Unit    ft
	
	# _The Scale button show an editing mode icon
	Check Save Close Scale Button Exist On Space
	
	# 11. Enter length less 1 and greater 1000
	# * Step 11: Dialog message "Enter a number between 1 and 1000 for real length." appear
	Set Scale Value    0    
	Check Popup Error Message    ${NUMBER_BETWEEN_1_AND_1000}    yes
	Set Scale Value    1001
	Check Popup Error Message    ${NUMBER_BETWEEN_1_AND_1000}    yes  

	# 12. Change unit
	# * Step 12: User is able to change unit
	# 13. Enter length between 1 and 1000, save and click to show ruler, observe the result
	# * Step 13: 
	# - The setting is automatically saved
	# - The space ruler will update when saving the length data
    Set Scale Value    100    m 
    
	# 14. Select scale setting, and close Floating box, and open the box again and select scale setting, observe the result
	# * Step 14: The saved data is still displayed
	Collapse Floating Box On Spaces
	Open Scale Setting
	Check Scale Setting Value    100
	Check Scale Setting Unit    m
	
	# 15. Assign another layer image 
	Assign Layer Image To Object    ${SITE_NODE}/${building_name}/${floor_name}/${room_name}    ${SM_30285_IMAGE_02}

	# 16. Open the box again and select scale setting, observe the result
	Open Scale Setting
	
	# * Step 16: The data is reset.
    Check Scale Setting Value    ${EMPTY}
    
SM-30285-07_Verify user is able to set scale for Cubicle space
    [Setup]    Run Keywords    Delete Building    ${building_name}
    ...    AND    Add New Building    ${building_name}     
    ...    AND    Open SM Login Page
    ...    AND    Login To SM    ${USERNAME}    ${PASSWORD}
                  
    [Teardown]    Run Keywords    Close Browser    
    ...    AND    Delete Building    ${building_name}
       
	# 1. Launch and log into SM Web
	# 2. Go to Administration > Spaces > Layer Image Files
    # 3. Add two layer image files (non-autocad files) 
    # 4. Go to "Site Manager" page
    Select Main Menu    ${Site Manager}	
    
	# 5. Add Cubicle
	Add Floor    ${SITE_NODE}/${building_name}    ${floor_name}
	Add Cubicle    ${SITE_NODE}/${building_name}/${floor_name}    ${cubicle_name}    
	
	# 6. Select City and change to Spaces view
	Click Tree Node On Site Manager    ${SITE_NODE}/${building_name}/${floor_name}/${cubicle_name}
	Go To Another View On Site Manager    ${Spaces}
	
    # 7. Observe the result
    # * Step 7: The Floating button is hidden
    Check Floating Button Not Exist On Space

	# 8. Click Unlock button
	Toggle Spaces View Lock State    state=Unlock

	# 9. Assign layer image and observe
	Assign Layer Image To Object    ${SITE_NODE}/${building_name}/${floor_name}/${cubicle_name}    ${SM_30285_IMAGE_01}
	
    # * Step 9: The Floating button is shown	
    Check Floating Button Exist On Space

	# 10. Expand Floating box then click Scale Setting
	Open Scale Setting
	
	# * Step 10: Scale value box is displayed:
	Check Scale Value Box Exist On Space
	# _There are 2 points to define real length in a floating box with length value and unit (Meter/Feet)
	Check Scale Setting Unit    m
	Switch Scale Setting Unit    ft
	Check Scale Setting Unit    ft
	
	# _The Scale button show an editing mode icon
	Check Save Close Scale Button Exist On Space
	
	# 11. Enter length less 1 and greater 1000
	# * Step 11: Dialog message "Enter a number between 1 and 1000 for real length." appear
	Set Scale Value    0    
	Check Popup Error Message    ${NUMBER_BETWEEN_1_AND_1000}    yes
	Set Scale Value    1001
	Check Popup Error Message    ${NUMBER_BETWEEN_1_AND_1000}    yes  

	# 12. Change unit
	# * Step 12: User is able to change unit
	# 13. Enter length between 1 and 1000, save and click to show ruler, observe the result
	# * Step 13: 
	# - The setting is automatically saved
	# - The space ruler will update when saving the length data
    Set Scale Value    100    m 
    
	# 14. Select scale setting, and close Floating box, and open the box again and select scale setting, observe the result
	# * Step 14: The saved data is still displayed
	Collapse Floating Box On Spaces
	Open Scale Setting
	Check Scale Setting Value    100
	Check Scale Setting Unit    m
	
	# 15. Assign another layer image 
	Assign Layer Image To Object    ${SITE_NODE}/${building_name}/${floor_name}/${cubicle_name}    ${SM_30285_IMAGE_02}

	# 16. Open the box again and select scale setting, observe the result
	Open Scale Setting
	
	# * Step 16: The data is reset.
    Check Scale Setting Value    ${EMPTY}
    
SM-30285-08_Verify user is unable to set scale for OIL only
    [Setup]    Run Keywords    Delete Building    ${building_name}
    ...    AND    Add New Building    ${building_name}     
    ...    AND    Open SM Login Page
    ...    AND    Login To SM    ${USERNAME}    ${PASSWORD}  
                  
    [Teardown]    Run Keywords    Close Browser    
    ...    AND    Delete Building    ${building_name}
       
	# 1. Launch and log into SM Web
	# 2. Go to Administration > Spaces > Layer Image Files
    # 3. Add two layer image files (non-autocad files) 
    # 4. Go to "Site Manager" page
    Select Main Menu    ${Site Manager}	
    
	# 5. Add Building > Floor > Room -> Rack
	Add Floor    ${SITE_NODE}/${building_name}    ${floor_name}
	Add Room    ${SITE_NODE}/${building_name}/${floor_name}    ${room_name}    
	Add Rack    ${SITE_NODE}/${building_name}/${floor_name}/${room_name}    ${rack_name}
	
	# 6. Select Rack and change to Spaces view
	Click Tree Node On Site Manager    ${SITE_NODE}/${building_name}/${floor_name}/${room_name}/${position_rack_name}
	Go To Another View On Site Manager    ${Spaces}
	
    # 7. Observe the result
    # * Step 7: The Floating button is hidden
    Check Floating Button Not Exist On Space

	# 8. Click Unlock button
	Toggle Spaces View Lock State    state=Unlock

	# * Step 8: The Floating button is hidden
	Check Floating Button Not Exist On Space
	
SM-30285-11_Verify that space view will get the scale directly and scale setting button is not available when it is a AUTOCAD background
    [Setup]    Run Keywords    Open SM Login Page
    ...    AND    Login To SM    ${USERNAME}    ${PASSWORD}
    ...    AND    Delete Tree Node If Exist On Site Manager    ${SITE_NODE}/${cable_vault_name}
                  
    [Teardown]    Run Keywords    Delete Tree Node On Site Manager     ${SITE_NODE}/${cable_vault_name}
    ...    AND    Close Browser
       
	# 1. Launch and log into SM Web
    # 2. Go to Administration > Spaces > Layer Image Files
    # 3. Add two layer image files (autocad files) 
    # 4. Go to "Site Manager" page    
    Select Main Menu    ${Site Manager}	
    
	# 5. Add Cable Vault and Rack 001, 002 under Cable Vault
	Add Cable Vault    ${SITE_NODE}    ${cable_vault_name}
	Add Rack    ${SITE_NODE}/${cable_vault_name}    ${rack_name}
	Add Rack    ${SITE_NODE}/${cable_vault_name}    ${rack_name_2}    position=2        
    	
	# 6. Select Cable Vault and change to Spaces view
	Click Tree Node On Site Manager    ${SITE_NODE}/${cable_vault_name}
	Go To Another View On Site Manager    ${Spaces}
	
    # 7. Unlock Spaces view then assign layer image for Cable Vault
    Toggle Spaces View Lock State    state=Unlock
    Assign Layer Image To Object    ${SITE_NODE}/${cable_vault_name}    ${SM_30285_IMAGE_03}
    
    # 8. Places Rack 001, 002 into Cable Vault
    Place Object On Space    ${SITE_NODE}/${cable_vault_name}    ${SITE_NODE}/${cable_vault_name}/${position_rack_name}    10,10                
    Place Object On Space    ${SITE_NODE}/${cable_vault_name}    ${SITE_NODE}/${cable_vault_name}/${position_rack_name_2}    20,20
        
    # 9. Click Floating menu button and observe
    Expand Floating Box On Spaces
    
    # * Step 9: 
        # _The Scale Setting button is not available
        # _space view will get the scale directly
    Check Scale Setting Button Not Displays On Geo Map
    
SM-30285-10_Verify that user is able to set scale and changes floor plan background if there is (are) placed object(s) on Spaces view
    [Setup]    Run Keywords    Open SM Login Page
    ...    AND    Login To SM    ${USERNAME}    ${PASSWORD}
    ...    AND    Delete Tree Node If Exist On Site Manager    ${SITE_NODE}/${cable_vault_name}
                  
    [Teardown]    Run Keywords    Delete Tree Node On Site Manager     ${SITE_NODE}/${cable_vault_name}
    ...    AND    Close Browser
       
	# 1. Launch and log into SM Web
    # 2. Go to Administration > Spaces > Layer Image Files
    # 3. Add two layer image files (non-autocad files) 
    # 4. Go to "Site Manager" page
    Select Main Menu    ${Site Manager}	
    
	# 5. Add Cable Vault and Rack 001, 002 under it
	Add Cable Vault    ${SITE_NODE}    ${cable_vault_name}
	Add Rack    ${SITE_NODE}/${cable_vault_name}    ${rack_name}
	Add Rack    ${SITE_NODE}/${cable_vault_name}    ${rack_name_2}    position=2        
    	
	# 6. Select Cable Vault and change to Spaces view
	Click Tree Node On Site Manager    ${SITE_NODE}/${cable_vault_name}
	Go To Another View On Site Manager    ${Spaces}
	
    # 7. Unlock Spaces view then assign layer image for Cable Vault
    Toggle Spaces View Lock State    state=Unlock
    Assign Layer Image To Object    ${SITE_NODE}/${cable_vault_name}    ${SM_30285_IMAGE_01}
    
    # 8. Places Rack 001, 002 into Cable Vault
    Place Object On Space    ${SITE_NODE}/${cable_vault_name}    ${SITE_NODE}/${cable_vault_name}/${position_rack_name}    10,10                
    Place Object On Space    ${SITE_NODE}/${cable_vault_name}    ${SITE_NODE}/${cable_vault_name}/${position_rack_name_2}    20,20
    
    # 9. Click Floating menu button and observe
    # * Step 9: The Floating button is shown
    Check Floating Button Exist On Space
    Open Scale Setting
    Check Scale Value Box Exist On Space
   
    # 10. Click Scale Setting
    # 11. Enter length between 1 and 1000, then leave the length input focus and observe
    Set Scale Value    100    ft    saveScale=${False}
    # * Step 11: 
    # - The setting is automatically saved
    # - The space ruler will update when saving the length data
    Check Scale Setting Value    100
	Check Scale Setting Unit    ft
    
    # 12. Change to another image file via Replace Image Layers
    Assign Layer Image To Object    ${SITE_NODE}/${cable_vault_name}    ${SM_30285_IMAGE_02}
    Preview And Submit Replace Image
    
    # 13. Observe the result
    Open Scale Setting
    # * Step 13: The data is reset.
    Check Scale Setting Value    ${EMPTY}   
    