*** Settings ***
Resource    ../../../../../resources/bug_report.robot
Resource    ../../../../../resources/constants.robot
Resource    ../../../../../resources/icons_constants.robot

Library   ../../../../../py_sources/logigear/setup.py
Library   ../../../../../py_sources/logigear/api/BuildingApi.py    ${USERNAME}    ${PASSWORD}
Library    ../../../../../py_sources/logigear/api/CampusApi.py                    
Library   ../../../../../py_sources/logigear/page_objects/system_manager/site_manager/SiteManagerPage${PLATFORM}.py    
Library   ../../../../../py_sources/logigear/page_objects/system_manager/login/LoginPage${PLATFORM}.py
Library    ../../../../../py_sources/logigear/page_objects/system_manager/administration/spaces/AdmLayerImageFilesPage${PLATFORM}.py   
Library    ../../../../../py_sources/logigear/page_objects/system_manager/zone_header/ZoneHeaderPage${PLATFORM}.py

Default Tags    Spaces
Force Tags    SM-30283

*** Test Cases ***
SM-30283-01_Verify that the origin and the floating menu are shown on floorplan
    
    [Setup]    Run Keywords    Open SM Login Page
    ...    AND    Login To SM    ${USERNAME}    ${PASSWORD}
    ...    AND    Set Test Variable    ${campus_name}    Campus_30283
    ...    AND    Set Test Variable    ${building_name}    Building_30283
    ...    AND    Set Test Variable    ${city_name}    City_30283
    ...    AND    Set Test Variable    ${cablevault_name}    Cable Vault_30283
    ...    AND    Delete Building    ${building_name}
    ...    AND    Delete Campus    ${campus_name}
    ...    AND    Delete Tree Node If Exist On Site Manager    ${SITE_NODE}/${cablevault_name}
    ...    AND    Delete Tree Node If Exist On Site Manager    ${SITE_NODE}/${city_name}        

    [Teardown]    Run Keywords    Delete Tree Node On Site Manager     ${SITE_NODE}/${city_name}
    ...    AND    Delete Campus     ${campus_name}
    ...    AND    Delete Tree Node On Site Manager     ${SITE_NODE}/${cablevault_name}
    ...    AND    Delete Building     ${building_name}
    ...    AND    Select Main Menu    ${Administration}/${Spaces}    ${Layer Image Files}
    ...    AND    Delete Layer Image Files    ${sm_30283_image_name_1}
    ...    AND    Delete Layer Image Files    ${sm_30283_image_name_2}
    ...    AND    Delete Layer Image Files    ${sm_30283_image_name_3}
    ...    AND    Delete Layer Image Files    ${sm_30283_image_name_4}
    ...    AND    Close Browser
    
    ${sm_30283_image_name_1} =    Set Variable    Layer_SM_30283_01  
    ${sm_30283_image_location_1} =    Set Variable    C:\\Pre Condition\\Layers\\logigear_building.jpg
    ${sm_30283_image_name_2} =    Set Variable    Layer_SM_30283_02  
    ${sm_30283_image_location_2} =    Set Variable    C:\\Pre Condition\\Layers\\848102521_2004.dwg
    ${sm_30283_image_name_3} =    Set Variable    Layer_SM_30283_03  
    ${sm_30283_image_location_3} =    Set Variable    C:\\Pre Condition\\Layers\\mainFramePic.png
    ${sm_30283_image_name_4} =    Set Variable    Layer_SM_30283_04  
    ${sm_30283_image_location_4} =    Set Variable    C:\\Pre Condition\\Layers\\848102521_2013.dwg
    
    # 1. Login to Site Manager
    # 2. Go to Administration > Spaces, add some layer image files with filestype AutoCAD DWG (2000, 2004, 2007, 2010 and 2013), DXF, BMP, JPG, JPEG, TIF, TIFF, PNG, GIF, EMF and WMF.

    Select Main Menu    ${Administration}/${Spaces}    ${Layer Image Files} 
    Delete Layer Image Files    ${sm_30283_image_name_1}
    Delete Layer Image Files    ${sm_30283_image_name_2}
    Delete Layer Image Files    ${sm_30283_image_name_3}
    Delete Layer Image Files    ${sm_30283_image_name_4}
    Add Layer Image Files    ${sm_30283_image_name_1}    ${sm_30283_image_location_1}
    Add Layer Image Files    ${sm_30283_image_name_2}    ${sm_30283_image_location_2}
    Add Layer Image Files    ${sm_30283_image_name_3}    ${sm_30283_image_location_3}
    Add Layer Image Files    ${sm_30283_image_name_4}    ${sm_30283_image_location_4}
    
    Select Main Menu    ${Site Manager}	
   
    # 3. Add the following objects to Site tree:
    # _Building 01
    # _City 01
    # _Campus 01
    # _Cable Vault 01
    Add City    ${SITE_NODE}    ${city_name}
    Add Campus    ${SITE_NODE}    ${campus_name}    
    Add Cable Vault    ${SITE_NODE}    ${cablevault_name}
    Add Building    ${SITE_NODE}    ${building_name}    
    # 5. Go to Spaces, select the following objects in Site tree, unlock and observe:
    # _Building 01
    # _City 01
    # _Campus 01
    # _Cable Vault 01
    Go To Another View On Site Manager    ${Spaces}
    # Verify Point: _No the origin is shown on the floorplan
    # Verify Point: _No the floating menu is shown on the floorplan
    Toggle Spaces View Lock State    ${SITE_NODE}/${city_name}    state=Unlock
    Check Object Image Not Exist On Map View    objectName=Origin    
    Check Floating Menu Not Exist On Map View    
    
    Toggle Spaces View Lock State    ${SITE_NODE}/${campus_name}    state=Unlock
    Check Object Image Not Exist On Map View    objectName=Origin    
    Check Floating Menu Not Exist On Map View  
    
    Toggle Spaces View Lock State    ${SITE_NODE}/${cablevault_name}    state=Unlock
    Check Object Image Not Exist On Map View    objectName=Origin    
    Check Floating Menu Not Exist On Map View  
    
    Toggle Spaces View Lock State    ${SITE_NODE}/${building_name}    state=Unlock
    Check Object Image Not Exist On Map View    objectName=Origin    
    Check Floating Menu Not Exist On Map View  

    # 6. Assign an image to the objects above and observe
    # Verify Point: _The origin is shown on the floorplan, default is on the bottom left.
    # Verify Point: _The floating menu is shown on the floorplan
    Assign Layer Image To Object    ${SITE_NODE}/${city_name}     ${sm_30283_image_name_1}   
    Assign Layer Image To Object    ${SITE_NODE}/${campus_name}     ${sm_30283_image_name_2}
    Assign Layer Image To Object    ${SITE_NODE}/${cablevault_name}     ${sm_30283_image_name_3}
    Assign Layer Image To Object    ${SITE_NODE}/${building_name}     ${sm_30283_image_name_4}

    # 7. Naviagte to Contents and back to Space
    # 8. Observe the buttons
    # Verify Point: _The origin still is displayed on the bottom left
    # Verify Point: _The floating menu still is displayed 
    Go To Another View On Site Manager    ${Devices}
    Go To Another View On Site Manager    ${Spaces}
    
    Check Object Image Exist On Map View    ${SITE_NODE}/${city_name}    objectName=${Origin}    
    Check Floating Menu Exist On Map View 
  
    Check Object Image Exist On Map View    ${SITE_NODE}/${campus_name}    objectName=${Origin}    
    Check Floating Menu Exist On Map View
    
    Check Object Image Exist On Map View    ${SITE_NODE}/${cablevault_name}    objectName=${Origin}    
    Check Floating Menu Exist On Map View
    
    Check Object Image Exist On Map View    ${SITE_NODE}/${building_name}    objectName=${Origin}    
    Check Floating Menu Exist On Map View
    
    # 9. Click Hide Tree and Hide Properties
    Toggle Pane On Spaces View    Hide
    # 10. Observe the buttons
    # Verify Point: _The origin still is displayed
    Check Object Image Exist On Map View    objectName=${Origin}    
    Toggle Pane On Spaces View    Show
    
(Bulk_SM-30283-02-03)_Verify that the floating menu should be hidden when warning message is opened ,Verify that the origin is located to the origin point directly    
    
    [Setup]    Run Keywords    Open SM Login Page
    ...    AND    Login To SM    ${USERNAME}    ${PASSWORD}
    ...    AND    Set Test Variable    ${cablevault_name}    Cable Vault_30283_2
    ...    AND    Delete Tree Node If Exist On Site Manager     ${SITE_NODE}/${cablevault_name}

    [Teardown]    Run Keywords    Delete Tree Node On Site Manager     ${SITE_NODE}/${cablevault_name}
    ...    AND    Select Main Menu    ${Administration}/${Spaces}    ${Layer Image Files}
    ...    AND    Delete Layer Image Files    ${sm_30283_image_name_02_1}
    ...    AND    Close Browser
    
    ${rack_name} =    Set Variable    Rack 001
    ${tree_rack_name} =    Set Variable    ${SITE_NODE}/${cablevault_name}/1:1 Rack 001
    ${assign_no_image} =    Set Variable    << No Image >>
    ${warning_message} =    Set Variable    Selecting << No Image >> will clear all placed objects from the current space. Do you want to proceed?
    ${sm_30283_image_name_02_1} =    Set Variable    Layer_SM_30283_02_01  
    ${sm_30283_image_location_02_1} =    Set Variable    C:\\Pre Condition\\Layers\\logigear_building.jpg
    
    # 2. Go to Administration > Spaces, add some layer image files with filestype AutoCAD or non AutoCAD  
    Select Main Menu    ${Administration}/${Spaces}    ${Layer Image Files} 
    Delete Layer Image Files    ${sm_30283_image_name_02_1}
    Add Layer Image Files    ${sm_30283_image_name_02_1}    ${sm_30283_image_location_02_1}
    Select Main Menu    ${Site Manager}	
    
    # 3. Add Cable Vault 01/Rack 001 to Site tree
    Add Cable Vault    ${SITE_NODE}    ${cablevault_name}
    Add Rack    ${SITE_NODE}/${cablevault_name}    ${rack_name}    
    # 4. Go to Spaces
    Go To Another View On Site Manager    ${Spaces}
    # 5. Select Cable Vault 01 object and unlock
    Toggle Spaces View Lock State    ${SITE_NODE}/${cablevault_name}    state=Unlock
    # 6. Assign an image and place the object (Rack 001) to layer
    # 7. Then assign No image to layer
    Assign Layer Image To Object    ${SITE_NODE}/${cablevault_name}     ${sm_30283_image_name_02_1}  
    Place Object On Space    ${SITE_NODE}/${cablevault_name}    ${tree_rack_name}    10,10
    Assign Layer Image To Object    ${SITE_NODE}/${cablevault_name}     ${assign_no_image}  
    # 8. Observe the result while the warning message is opening
    Check Warning Message    ${warning_message}
    # Verify Point: The floating menu should be hidden below the gray background
    Check Floating Menu Not Exist On Map View    
    
    # 9. Assign an image to the objects above and observe
    Assign Layer Image To Object    ${SITE_NODE}/${cablevault_name}     ${sm_30283_image_name_02_1}
    # 10.Select one objects above and go to Spaces view
    # 11.Click locate Origin point
    # 12.Observe the result
    # Verify Point:the Origin point is located to the origin point directly
    Locate Origin Point
    # Check Origin Point Located
    Show Label On Space
    Check Located Object Point    ${Origin}

    