*** Settings ***
Resource    ../../../../../resources/bug_report.robot
Resource    ../../../../../resources/constants.robot
Resource    ../../../../../resources/icons_constants.robot

Library   ../../../../../py_sources/logigear/setup.py               
Library   ../../../../../py_sources/logigear/page_objects/system_manager/site_manager/SiteManagerPage${PLATFORM}.py    
Library   ../../../../../py_sources/logigear/page_objects/system_manager/login/LoginPage${PLATFORM}.py
Library   ../../../../../py_sources/logigear/page_objects/system_manager/administration/AdministrationPage${PLATFORM}.py    
Library    ../../../../../py_sources/logigear/page_objects/system_manager/administration/spaces/AdmGeomapOptionsPage${PLATFORM}.py
Library    ../../../../../py_sources/logigear/page_objects/system_manager/zone_header/ZoneHeaderPage${PLATFORM}.py
Library    ../../../../../py_sources/logigear/page_objects/system_manager/administration/users/AdmSystemManagerUsersPage${PLATFORM}.py
Library    ../../../../../py_sources/logigear/page_objects/system_manager/administration/spaces/AdmLayerImageFilesPage${PLATFORM}.py   
Library   ../../../../../py_sources/logigear/page_objects/system_manager/tools/database_tools/RestoreDatabasePage${PLATFORM}.py

Default Tags    Spaces
Force Tags    SM-30529

Suite Setup    Run Keywords    Open SM Login Page
    ...    AND    Login To SM    ${USERNAME}    ${PASSWORD}
    ...    AND    Select Main Menu    ${Administration}/${Spaces}    ${Layer Image Files}
    ...    AND    Delete Layer Image Files    ${SM_30529_IMAGE_01}
    ...    AND    Delete Layer Image Files    ${SM_30529_IMAGE_02} 
    ...    AND    Add Layer Image Files    ${SM_30529_IMAGE_01}    ${SM_30529_LOCATION_01}
    ...    AND    Add Layer Image Files    ${SM_30529_IMAGE_02}    ${SM_30529_LOCATION_02}
    ...    AND    Logout Sm 

Suite Teardown    Close Browser

*** Variables ***
${SM_30529_IMAGE_01}    Layer_SM_30529_01
${SM_30529_IMAGE_02}    Layer_SM_30529_02
${SM_30529_CV_01}    CV_SM_30529_01
${SM_30529_CV_02}    CV_SM_30529_02
${SM_30529_LOCATION_01}    C:\\Pre Condition\\Layers\\logigear_building.jpg
${SM_30529_LOCATION_02}    C:\\Pre Condition\\Layers\\logigear_bughunting.jpg
${RACK_NAME}    Rack 001
${POSITION_RACK_NAME}    1:1 ${RACK_NAME}
${RESTORE_FILE_NAME}    User_LDAP.bak
${TC_LDAP_USER}    cuong.cheng
${Edit origin and scale}    Edit origin and scale
${Replace image}    Replace image

*** Test Cases ***
SM-30529-01_Verify that user is able to moving the origin and opening scale setting when have the "Edit origin and scale" privilege
    [Setup]    Run Keywords    Login To Sm    ${USERNAME}    ${PASSWORD}         
    ...    AND    Select Main Menu    ${Site Manager}    
    ...    AND    Delete Tree Node If Exist On Site Manager    ${SITE_NODE}/${SM_30529_CV_01}
    ...    AND    Delete Tree Node If Exist On Site Manager    ${SITE_NODE}/${SM_30529_CV_02}
    [Teardown]    Run Keyword    Logout Sm
    # * On SM:
    # 1. Go to Administration Tab -> System Manage User
    Select Main Menu    ${Administration}/${Users}
    
    # 2. Add a user "user_01" with checked on: 
        # - Space: "Edit origin and scale".
        # - Location is "Site"
        # - Site Manager checkbox.
    Delete User    user_01
    Add User    01    User    user_01    ${PASSWORD}    None    ${Spaces}    ${Edit origin and scale}    locations=${SITE_NODE}
    Edit User Information    userName=user_01    userPrivileges=${Site Manager}
    
    # 3. Go to Administration > Spaces, add some layer image files with filestype AutoCAD or non AutoCAD
    # 4. Login to SM with "admin"
    # 5. Add Cable Vault 01/1:1 Rack 001
    # 6. Add Cable Vault 02/1:1 Rack 001
    Select Main Menu    ${Site Manager}
    Add Cable Vault    ${SITE_NODE}    ${SM_30529_CV_01}        
    Add Cable Vault    ${SITE_NODE}    ${SM_30529_CV_02}
    Add Rack    ${SITE_NODE}/${SM_30529_CV_01}    ${RACK_NAME}    
    Add Rack    ${SITE_NODE}/${SM_30529_CV_02}    ${RACK_NAME}
    
    # 7. Go to Spaces, assign an image to Cable Vault 01.
    Click Tree Node On Site Manager    ${SITE_NODE}/${SM_30529_CV_01}
	Go To Another View On Site Manager    ${Spaces}
	Toggle Spaces View Lock State    state=Unlock
	Assign Layer Image To Object    ${SITE_NODE}/${SM_30529_CV_01}    ${SM_30529_IMAGE_01}
	
    # 8. Logout and login to SM with "user_01".
    Logout Sm
    Login To SM    user_01    ${PASSWORD}
    
    # 9. Go to Spaces, select the "Cable Vault 02" in Site tree, unlock and observe
    Click Tree Node On Site Manager    ${SITE_NODE}/${SM_30529_CV_02}
	Go To Another View On Site Manager    ${Spaces}
	Toggle Spaces View Lock State    state=Unlock

    # _The Assign Image checkbox is disabled.
    Check Select Layer Image Combobox Disabled
    
    # 10. Go to Spaces, select the "Cable Vault 01" in Site tree, unlock and observe
    Click Tree Node On Site Manager    ${SITE_NODE}/${SM_30529_CV_01}
    Toggle Spaces View Lock State    state=Unlock
    
    # _The Assign Image checkbox is disabled.
    Check Select Layer Image Combobox Disabled
    
    # 11. Try to edit Origin and click on "Click to open scale setting"
    Show Label On Space
    ${x1}    ${y1}    Get Object Position On Space    ${Origin}
    Modify Object Position On Space    objectName=${Origin}    position=400,300
    ${x2}    ${y2}    Get Object Position On Space    ${Origin}
    
    # _User_01 is able to edit the Origin
    Should Not Be Equal As Integers    ${x1}    ${x2}
    Should Not Be Equal As Integers    ${y1}    ${y2}
          
    #, able to opening scale setting and edit the scale setting.
    Open Scale Setting
    Check Scale Value Box Exist On Space
    Set Scale Value    100    m
  
	Open Scale Setting
    Check Scale Setting Value    100
	Check Scale Setting Unit    m

SM-30529-02_Verify that user is able to enable/disable image list with "Replace image" privilege
    [Setup]    Run Keywords    Login To Sm    ${USERNAME}    ${PASSWORD}     
    ...    AND    Select Main Menu    ${Site Manager}    
    ...    AND    Delete Tree Node If Exist On Site Manager    ${SITE_NODE}/${SM_30529_CV_01}
    [Teardown]    Run Keyword    Logout Sm
    # * On SM:
    # 1. Go to Administration Tab -> System Manage User
    Select Main Menu    ${Administration}/${Users}
    
    # 2. Add a user "user_02" with checked on:
        # - Space: "Replace Image".
        # - Location is "Site"
        # - Site Manager checkbox.
    Delete User    user_02
    Add User    02    User    user_02    ${PASSWORD}    None    ${Spaces}    ${Replace image}    locations=${SITE_NODE}
    Edit User Information    userName=user_02    userPrivileges=${Site Manager}    
    
    # 3. Go to Administration > Spaces, add some layer image files (Image 01 and Image 02)
    # 4. Login to SM with "user_02"
    Logout Sm
    Login To SM    user_02    ${PASSWORD}
    
    # 5. Add Cable Vault 01/1:1 Rack 001
    Add Cable Vault    ${SITE_NODE}    ${SM_30529_CV_01}        
    Add Rack    ${SITE_NODE}/${SM_30529_CV_01}    ${RACK_NAME}
    
    # 6. Go to Spaces, select the "Cable Vault 01" in Site tree, unlock and observe
    Click Tree Node On Site Manager    ${SITE_NODE}/${SM_30529_CV_01}
    Go To Another View On Site Manager    ${Spaces}
    Toggle Spaces View Lock State    state=Unlock
    
    # _The Assign Image checkbox is enabled.
    Check Select Layer Image Combobox Enabled 

    # 7. Assign the Image 01 and place the object (Rack 001) to layer
    # _"User_02" is able to assign image for object.
    Check Layer Image Is Assigned    ${SITE_NODE}/${SM_30529_CV_01}    ${SM_30529_IMAGE_01}
    Place Object On Space    ${SITE_NODE}/${SM_30529_CV_01}    ${SITE_NODE}/${SM_30529_CV_01}/${POSITION_RACK_NAME}   10,10 

    # 8. Continue assign Image 02 to layer.
    # _"User_02" is able to assign image for object.
    Assign Layer Image To Object    ${SITE_NODE}/${SM_30529_CV_01}    ${SM_30529_IMAGE_02}    isReplaced=${True}
    Check Layer Image Is Assigned    ${SITE_NODE}/${SM_30529_CV_01}    ${SM_30529_IMAGE_02}
    
SM-30529-03_Verify that user is able to edit origin and enable/disable image list when checked both Spaces in Privileges
    [Setup]    Run Keywords    Login To Sm    ${USERNAME}    ${PASSWORD}     
    ...    AND    Select Main Menu    ${Site Manager}    
    ...    AND    Delete Tree Node If Exist On Site Manager    ${SITE_NODE}/${SM_30529_CV_01}
    [Teardown]    Run Keyword    Logout Sm
    # * On SM:
    # 1. Go to Administration Tab -> System Manage User
    Select Main Menu    ${Administration}/${Users}
    
    # 2. Add a user "user_03" with checked: 
        # - Space: included both checkboxes ("Replace Image", "Edit origin and scale")
        # - Location is "Site"
        # - Site Manager checkbox.
    Delete User    user_03
    Add User    03    User    user_03    ${PASSWORD}    None    ${Spaces}    ${Replace image},${Edit origin and scale}    locations=${SITE_NODE}
    Edit User Information    userName=user_03    userPrivileges=${Site Manager}           

    # 3. Go to Administration > Spaces, add some layer image files (Image 01 and Image 02)
    # 4. Login to SM with "user_03"
    Logout Sm
    Login To SM    user_03    ${PASSWORD}
    
    # 5. Add Cable Vault 01/1:1 Rack 001
    Add Cable Vault    ${SITE_NODE}    ${SM_30529_CV_01}        
    Add Rack    ${SITE_NODE}/${SM_30529_CV_01}    ${RACK_NAME}
    
    # 6. Go to Spaces, select the "Cable Vault 01" in Site tree, unlock and observe
    Click Tree Node On Site Manager    ${SITE_NODE}/${SM_30529_CV_01}
    Go To Another View On Site Manager    ${Spaces}
    Toggle Spaces View Lock State    state=Unlock
    
    # _The Assign Image checkbox is enabled.
    Check Select Layer Image Combobox Enabled  
    
    # 7. Assign the Image 01 and place the object (Rack 001) to layer
    Assign Layer Image To Object    ${SITE_NODE}/${SM_30529_CV_01}    ${SM_30529_IMAGE_01}
    Place Object On Space    ${SITE_NODE}/${SM_30529_CV_01}    ${SITE_NODE}/${SM_30529_CV_01}/${POSITION_RACK_NAME}   10,10
    
    # _"User_03" is able to assign image for object.
    Check Layer Image Is Assigned    ${SITE_NODE}/${SM_30529_CV_01}    ${SM_30529_IMAGE_01}
    
    # 8. Try to edit Origin and click on "Click to open scale setting"
    Show Label On Space
    ${x1}    ${y1}    Get Object Position On Space    ${Origin}
    Modify Object Position On Space    objectName=${Origin}   position=400,300
    ${x2}    ${y2}    Get Object Position On Space    ${Origin}
    
    # _User_03 is able to edit the Origin
    Should Not Be Equal As Integers    ${x1}    ${x2}
    Should Not Be Equal As Integers    ${y1}    ${y2}
          
    #, able to opening scale setting and edit the scale setting.
    Open Scale Setting
    Check Scale Value Box Exist On Space
    Set Scale Value    100    m
  
	Open Scale Setting
    Check Scale Setting Value    100
	Check Scale Setting Unit    m

SM-30529-04_Verify that the LDAP user is able to edit origin and enable/disable image list when checked both Spaces in Privileges
    [Setup]    Run Keywords    Login To Sm    ${USERNAME}    ${PASSWORD}     
    ...    AND    Select Main Menu    ${Site Manager}    
    ...    AND    Delete Tree Node If Exist On Site Manager    ${SITE_NODE}/${SM_30529_CV_01}
    [Teardown]    Run Keyword    Logout Sm
    
    # * On SM:
    # 1. Go to Administration Tab -> System Manage User
    Select Main Menu    ${Administration}/${Users}
    
    # 2. Add a LDAP user (ex: cuong.cheng) with checked: 
        # - Space: included both checkboxes ("Replace Image", "Edit origin and scale")
        # - Location is "Site"
        # - Site Manager checkbox.
    Delete User    ${DOMAIN_USERNAME}
    Add User    Vo    Tung    ${DOMAIN_USERNAME}    None    None    ${Spaces}    ${Replace image},${Edit origin and scale}    locations=${SITE_NODE}    LDAP=${True}
    Edit User Information    userName=${DOMAIN_USERNAME}    userPrivileges=${Site Manager}
         
    # 3. Go to Administration > Spaces, add some layer image files (Image 01 and Image 02)
    # 4. Login to SM with LDAP user
    Logout Sm
    Login To SM    ${DOMAIN_USERNAME}    ${EMAIL_PASSWORD}
    
    # 5. Add Cable Vault 01/1:1 Rack 001
    Add Cable Vault    ${SITE_NODE}    ${SM_30529_CV_01}        
    Add Rack    ${SITE_NODE}/${SM_30529_CV_01}    ${RACK_NAME}
    
    # 6. Go to Spaces, select the "Cable Vault 01" in Site tree, unlock and observe
    Click Tree Node On Site Manager    ${SITE_NODE}/${SM_30529_CV_01}
    Go To Another View On Site Manager    ${Spaces}
    Toggle Spaces View Lock State    state=Unlock
    
    # _The Assign Image checkbox is enabled.
    Check Select Layer Image Combobox Enabled 
    
    # 7. Assign the Image 01 and place the object (Rack 001) to layer
    Assign Layer Image To Object    ${SITE_NODE}/${SM_30529_CV_01}    ${SM_30529_IMAGE_01}
    Place Object On Space    ${SITE_NODE}/${SM_30529_CV_01}    ${SITE_NODE}/${SM_30529_CV_01}/${POSITION_RACK_NAME}   10,10
    
    # _LDAP user is able to assign image for object.
    Check Layer Image Is Assigned    ${SITE_NODE}/${SM_30529_CV_01}    ${SM_30529_IMAGE_01}
    
    # 8. Try to edit Origin and click on "Click to open scale setting"
    Show Label On Space
    ${x1}    ${y1}    Get Object Position On Space    ${Origin}
    Modify Object Position On Space    objectName=${Origin}    position=400,300
    ${x2}    ${y2}    Get Object Position On Space    ${Origin}
    
    # _User_ldap is able to edit the Origin
    Should Not Be Equal As Integers    ${x1}    ${x2}
    Should Not Be Equal As Integers    ${y1}    ${y2}
          
    #, able to opening scale setting and edit the scale setting.
    Open Scale Setting
    Check Scale Value Box Exist On Space
    Set Scale Value    100    m
  
	Open Scale Setting
    Check Scale Setting Value    100
	Check Scale Setting Unit    m
    
    # 9. Login with Admin, Go to Administration Tab -> System Manage User
    Logout Sm
    Login To SM    ${USERNAME}    ${PASSWORD}
    
    # 10. Edit LDAP user with uncheck on Space checkbox in Privilege section.
    Select Main Menu    ${Administration}/${Users}
    Edit User Information    userName=${DOMAIN_USERNAME}    userPrivileges=${Spaces}    userPrivilegesItems=${Replace image},${Edit origin and scale}    privilegesState=${False}
        
    # 11. Logout and login again with LDAP user
    Logout Sm
    Login To SM    ${DOMAIN_USERNAME}    ${EMAIL_PASSWORD}
    
    # 12. Try to assign image to layer or edit the Origin.
    Click Tree Node On Site Manager    ${SITE_NODE}/${SM_30529_CV_01}
    Go To Another View On Site Manager    ${Spaces}
    Toggle Spaces View Lock State    state=Unlock
    
    # _The Assign Image checkbox is disabled.
    Check Select Layer Image Combobox Disabled
    
    # _LDAP user is not able to edit the Origin.
    Show Label On Space
    ${x1}    ${y1}    Get Object Position On Space    ${Origin}
    Modify Object Position On Space    objectName=${Origin}    position=400,300
    ${x2}    ${y2}    Get Object Position On Space    ${Origin}
    
    Should Be Equal As Integers    ${x1}    ${x2}
    Should Be Equal As Integers    ${y1}    ${y2}

SM-30529-05_Verify that those new privileges are unchecked except for admin after upgrade to Needle by default
    [Setup]    Run Keywords    Login To Sm    ${USERNAME}    ${PASSWORD}     
    ...    AND    Select Main Menu    ${Site Manager}    
    [Teardown]    Run Keyword    Logout Sm
    
    # "* On SM:
    # 1. Restore the database in Precondition C:\Pre Condition\Convert Database\User_LDAP.bak
    Restore Database    ${RESTORE_FILE_NAME}    ${DIR_FILE_BK}
    Login To SM    ${USERNAME}    ${PASSWORD}
    
    # 2. Go to Administration Tab -> System Manage User.
    Select Main Menu    ${Administration}/${Users}
    
    # 3. Open the Edit user for ""admin""
    # _Those new privileges are checked for Admin user.
    Check User Privileges State    ${USERNAME}    ${Spaces}    ${Replace image},${Edit origin and scale}        
    
    # 4. Open the Edit user for another user (ex: LDAP user, user_01"")"
    # _Those new privileges are un-checked for Non-Admin user (LDAP user, user_01")
    Check User Privileges State    ${TC_LDAP_USER}    ${Spaces}    ${Replace image},${Edit origin and scale}    ${False} 
