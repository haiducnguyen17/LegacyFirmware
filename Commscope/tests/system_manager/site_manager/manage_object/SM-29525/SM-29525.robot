*** Settings ***
Resource    ../../../../../resources/constants.robot
Resource    ../../../../../resources/icons_constants.robot

Library    ../../../../../py_sources/logigear/setup.py  
Library    ../../../../../py_sources/logigear/api/BuildingApi.py    ${USERNAME}    ${PASSWORD}          
Library    ../../../../../py_sources/logigear/page_objects/system_manager/site_manager/SiteManagerPage${PLATFORM}.py    
Library    ../../../../../py_sources/logigear/page_objects/system_manager/login/LoginPage${PLATFORM}.py
Library    ../../../../../py_sources/logigear/page_objects/system_manager/administration/AdministrationPage${PLATFORM}.py
Library    ../../../../../py_sources/logigear/page_objects/system_manager/zone_header/ZoneHeaderPage${PLATFORM}.py
Library    ../../../../../py_sources/logigear/page_objects/system_manager/site_manager/connections/upgrade_to_ipatch/UpgradeToiPatchPage${PLATFORM}.py
Library    ../../../../../py_sources/logigear/page_objects/system_manager/tools/database_tools/RestoreDatabasePage${PLATFORM}.py   
Library    ../../../../../py_sources/logigear/page_objects/system_manager/administration/list/AdmListPage${PLATFORM}.py
Library    ../../../../../py_sources/logigear/page_objects/system_manager/administration/user_defined_fields/AdmObjectFieldsPage${PLATFORM}.py
Library    ../../../../../py_sources/logigear/page_objects/system_manager/site_manager/connections/cabling/CablingPage${PLATFORM}.py    

*** Variables ***
${CITY_NAME}    City_29525
${CAMPUS_NAME}    Campus 01
${BUILDING_NAME}    Building 01
${CABLE_VAULT_NAME}    Cable Vault 01
${FLOOR_NAME}    Floor 01 
${ROOM_NAME}    Room 01
${RACK_NAME}    Rack 001
${RACK_NAME_2}    Rack 002
${CABINET_NAME}    Cabinet 001
${AUXILIARY_NAME}    Auxiliary 001
${RACK_NAME_FULL}    1:1 Rack 001
${RACK_NAME_2_FULL}    1:4 Rack 002
${CABINET_NAME_FULL}    1:2 Cabinet 001
${AUXILIARY_NAME_FULL}    1:3 Auxiliary 001
${SPLICE_ENCLOSURE_1}    Splice_Enclosure_01
${SPLICE_ENCLOSURE_2}    Splice_Enclosure_02
${SPLICE_ENCLOSURE_3}    Splice_Enclosure_03
${SPLICE_ENCLOSURE_4}    Splice_Enclosure_04
${TRAY_NAME_1}    Tray 01
${TRAY_NAME_2}    Tray 02
${TRAY_NAME_3}    Tray 03
${TRAY_NAME_4}    Tray 04
${TRAY_NAME_5}    Tray 05
${TRAY_NAME_6}    Tray 06
${ENCLOSURE_1U_FILENAME}    SpliceEnclosureInRack_1U.png
${ENCLOSURE_2U_FILENAME}    EnclosureInRack_2U.png
${DIR_ENCLOSURE_2U}    ${DIR_PRECONDITION_PICTURE}\\${ENCLOSURE_2U_FILENAME}
${LOC_RACVIEWIMG_2}    ${DIR_RACKVIEWIMG_COMMSCOPE}\\${ENCLOSURE_2U_FILENAME}
${ENCLOSURE_1U}    Splice Enclosure In Rack
${ENCLOSURE_2U}    Enclosure 2U
${RACK_VIEW_FRONT}    Rack View (Front)
${RACK_VIEW_ZEROU}    Rack View (Zero U)
${Auxiliary Rack}    Auxiliary Rack
${Cabinet (42U)}    Cabinet (42U)
${LIST_TYPE}    Equipment Images
${PROPERTIES_RACK}    Name,Rack Units,Location in Rack,Position,Rack (7 ft - 45U),Description,Location,Slots Capacity,Slots Usage,Slots Available
${PROPERTIES_CABINET}    Name,Rack Units,Location in Rack,Position,Cabinet (42U),Description,Location,Slots Capacity,Slots Usage,Slots Available
${PROPERTIES_AUXILIARY}    Name,Rack Units,Location in Rack,Position,Auxiliary Rack,Description,Location,Slots Capacity,Slots Usage,Slots Available
${FRONT}    Front
${ZERO U}    Zero U
${RACK UNITS}    Rack Units
${Object Fields}    Object Fields
${Splice Enclosure}    Splice Enclosure

*** Test Cases ***
(Bulk SM-29525_01-02) Verify that new image can show correctly when add Splice Enclosure to Rack/Cabinets/Auxiliary Racks under Cable Vault and Building
    [Tags]    SM-29525    Manage Object
    
    [Setup]    Run Keywords   Open SM Login Page
    ...    AND    Login To SM    ${USERNAME}    ${PASSWORD}
    ...    AND    Delete Tree Node If Exist On Site Manager     ${SITE_NODE}/${CITY_NAME}
    ...    AND    Select Main Menu    ${Administration}    Lists
    ...    AND    Remove File    ${LOC_RACVIEWIMG_2}
    ...    AND    Delete List On Administration    ${LIST_TYPE}    ${ENCLOSURE_2U}
    ...    AND    Add List On Administration    ${LIST_TYPE}    ${ENCLOSURE_2U}    ${DIR_ENCLOSURE_2U}
    
    [Teardown]    Run Keywords    Select Main Menu    ${Site Manager}
    ...    AND    Delete Tree Node On Site Manager    ${SITE_NODE}/${CITY_NAME}
    ...    AND    Select Main Menu    ${Administration}    Lists
    ...    AND    Delete List On Administration    ${LIST_TYPE}    ${ENCLOSURE_2U}
    ...    AND    Remove File    ${LOC_RACVIEWIMG_2}
    ...    AND    Close Browser
    
    # 1.Launch and log into SM
    # 2.Add City 01/Campus 01/CV 01/Rack 01, Cabinet 02, Auxiliary Rack 03 to CV 01 / Add City 01/Building 1/Rack 01, Cabinet 02, Auxiliary Rack 03 to Building 01
    Select Main Menu    ${Site Manager}
    ${tree_node_city} =    Add City    ${SITE_NODE}    ${CITY_NAME}
    ${tree_node_campus} =    Add Campus    ${SITE_NODE}/${CITY_NAME}    ${CAMPUS_NAME}
    Create Object    ${tree_node_city}    ${BUILDING_NAME}    ${FLOOR_NAME}    ${ROOM_NAME}
    ${tree_node_room}    Set Variable    ${tree_node_city}/${BUILDING_NAME}/${FLOOR_NAME}/${ROOM_NAME}
    ${tree_node_cable_vault} =    Add Cable Vault    ${tree_node_campus}    ${CABLE_VAULT_NAME}    
    ${tree_node_rack_bd} =     Add Rack    ${tree_node_room}    ${RACK_NAME}    position=1
    ${tree_node_cabinet_bd} =    Add Rack    ${tree_node_room}    ${CABINET_NAME}    rackType=${Cabinet (42U)}    position=2
    ${tree_node_auxiliary_bd} =    Add Rack    ${tree_node_room}    ${AUXILIARY_NAME}    rackType=${Auxiliary Rack}    position=3    capacityU=42
    ${tree_node_rack_cv} =     Add Rack    ${tree_node_cable_vault}    ${RACK_NAME}    position=1
    ${tree_node_cabinet_cv} =    Add Rack    ${tree_node_cable_vault}    ${CABINET_NAME}    rackType=${Cabinet (42U)}    position=2
    ${tree_node_auxiliary_cv} =    Add Rack    ${tree_node_cable_vault}    ${AUXILIARY_NAME}    rackType=${Auxiliary Rack}    position=3    capacityU=42
    ${tree_node_contain_object}    Create List    ${tree_node_rack_bd}    ${tree_node_cabinet_bd}    ${tree_node_auxiliary_bd}    ${tree_node_rack_cv}    ${tree_node_cabinet_cv}    ${tree_node_auxiliary_cv}
    ${splice_enclosure_names}    Create List    ${SPLICE_ENCLOSURE_1}    ${SPLICE_ENCLOSURE_2}    ${SPLICE_ENCLOSURE_3}    ${SPLICE_ENCLOSURE_1}    ${SPLICE_ENCLOSURE_2}    ${SPLICE_ENCLOSURE_3}
    ${splice_tray_name}    Create List    ${TRAY_NAME_1}    ${TRAY_NAME_2}    ${TRAY_NAME_3}    ${TRAY_NAME_1}    ${TRAY_NAME_2}    ${TRAY_NAME_3}
    ${properties_name_list}    Create List    ${PROPERTIES_RACK}    ${PROPERTIES_CABINET}    ${PROPERTIES_AUXILIARY}    ${PROPERTIES_RACK}    ${PROPERTIES_CABINET}    ${PROPERTIES_AUXILIARY}
    ${properties_rack_list}    Create List    ${RACK_NAME_FULL}    ${CABINET_NAME_FULL}    ${AUXILIARY_NAME_FULL}    ${RACK_NAME_FULL}    ${CABINET_NAME_FULL}    ${AUXILIARY_NAME_FULL}

    # 3.Add Splice Enclosure 01-02-03/Tray 01-02-03 to Rack 01, Cabinet 02, Auxiliary Rack 03
    # _User can add Enclosure successfully
    :FOR    ${i}    IN RANGE    0    len(${tree_node_contain_object})
    \    Add Splice Enclosure    ${tree_node_contain_object}[${i}]    ${splice_enclosure_names}[${i}]    description=${ENCLOSURE_1U}    location=${tree_node_contain_object}[${i}]
    \    Check Tree Node Exist On Site Manager    ${tree_node_contain_object}[${i}]/${splice_enclosure_names}[${i}]    
    \    Add Splice Tray    ${tree_node_contain_object}[${i}]/${splice_enclosure_names}[${i}]    ${splice_tray_name}[${i}]

    # 4.Select Rack 01, Cabinet 02, Auxiliary Rack 03
    # 5.Observe Rack View (Front), Rack View (Zero U)
    # _Default Image of Enclosure is displayed at 1st position (Front)
    # _Rack View (Zero U) is empty
    :FOR    ${i}    IN RANGE    0    len(${tree_node_contain_object})
    \    Click Tree Node On Site Manager    ${tree_node_contain_object}[${i}]    
    \    Go To Another View On Site Manager    ${RACK_VIEW_FRONT}
    \    Check Image Exist On Rack View    ${splice_enclosure_names}[${i}]    ${ENCLOSURE_1U_FILENAME}
    \    Go To Another View On Site Manager    ${RACK_VIEW_ZEROU}
    \    Check Image Not Exist On Rack View    ${splice_enclosure_names}[${i}]    ${ENCLOSURE_1U_FILENAME}    rackViewType=${ZERO U}

    # 6.Click on the Image at Rack View (Front) and observe
    # _Enclosure is selected on Site tree, Properties pane is displayed at right
    :FOR    ${i}    IN RANGE    0    len(${tree_node_contain_object})
    \    Click Tree Node On Site Manager    ${tree_node_contain_object}[${i}]    
    \    Go To Another View On Site Manager    ${RACK_VIEW_FRONT}
    \    Select Object On Rack View Front    ${splice_enclosure_names}[${i}]
    \    Wait For Object Exist On Content Table    ${splice_tray_name}[${i}]    
    \    Check Object Properties On Properties Pane    ${properties_name_list}[${i}]    ${splice_enclosure_names}[${i}],${1},${FRONT},${1},${properties_rack_list}[${i}],${ENCLOSURE_1U},${tree_node_contain_object}[${i}],${6},${1},${5}
    \    Check Equipment Image Exist On Properties Pane    ${ENCLOSURE_1U_FILENAME}
    \    Check Tree Node Selected On Site Manager    ${tree_node_contain_object}[${i}]/${splice_enclosure_names}[${i}]

    # 7.Edit Enclosure 01-02-03 then assign it Image 01
    :FOR    ${i}    IN RANGE    0    len(${tree_node_contain_object})
    \    Edit Splice Enclosure    ${tree_node_contain_object}[${i}]/${splice_enclosure_names}[${i}]    equipmentImageName=${ENCLOSURE_2U}

    # 8.Select Rack 01, Cabinet 02, Auxiliary Rack 03
    # 9.Observe Rack View (Front), Rack View (Zero U)
    # _Image 01 of Enclosure is displayed at 1st position (Front)
    # _Rack View (Zero U) is empty
    :FOR    ${i}    IN RANGE    0    len(${tree_node_contain_object})
    \    Click Tree Node On Site Manager    ${tree_node_contain_object}[${i}]    
    \    Go To Another View On Site Manager    ${RACK_VIEW_FRONT}
    \    Check Image Exist On Rack View    ${splice_enclosure_names}[${i}]    ${ENCLOSURE_2U_FILENAME}
    \    Go To Another View On Site Manager    ${RACK_VIEW_ZEROU}
    \    Check Image Not Exist On Rack View    ${splice_enclosure_names}[${i}]    ${ENCLOSURE_2U_FILENAME}    rackViewType=${ZERO U}

    # 10.Click on the Image at Rack View (Front) and observe
    # _Enclosure is selected on Site tree, Properties pane is displayed at right
    :FOR    ${i}    IN RANGE    0    len(${tree_node_contain_object})
    \    Click Tree Node On Site Manager    ${tree_node_contain_object}[${i}]    
    \    Go To Another View On Site Manager    ${RACK_VIEW_FRONT}
    \    Select Object On Rack View Front    ${splice_enclosure_names}[${i}]
    \    Wait For Object Exist On Content Table    ${splice_tray_name}[${i}]    
    \    Check Object Properties On Properties Pane    ${properties_name_list}[${i}]    ${splice_enclosure_names}[${i}],${1},${FRONT},${1},${properties_rack_list}[${i}],${ENCLOSURE_1U},${tree_node_contain_object}[${i}],${6},${1},${5}
    \    Check Equipment Image Exist On Properties Pane    ${ENCLOSURE_2U_FILENAME}
    \    Check Tree Node Selected On Site Manager    ${tree_node_contain_object}[${i}]/${splice_enclosure_names}[${i}]    
    
    # 11.Edit Enclosure 01-02-03
    # 12.Change the "Location in Rack" from Front to Zero U then observe
    # _Rack Unit is not change and automatically grey out
    :FOR    ${i}    IN RANGE    0    len(${tree_node_contain_object})
    \    Edit Splice Enclosure    ${tree_node_contain_object}[${i}]/${splice_enclosure_names}[${i}]    locationInRack=${ZERO U}    confirmSave=${False}
    \    Check Object Field Status On Properties Window    ${RACK UNITS}    Disable
    \    Click Save Add Object Button
    \    Click Tree Node On Site Manager    ${tree_node_contain_object}[${i}]/${splice_enclosure_names}[${i}]    
    \    Check Object Properties On Properties Pane    ${RACK UNITS}    ${1}
    
    # 13.Observe Rack View (Front) and Rack View (Zero U)
    # _Image 01 of Enclosure does not show at Rack View (Front)
    # _Image 01 of Enclosure 01 shown at Rack View (Zero U)
    :FOR    ${i}    IN RANGE    0    len(${tree_node_contain_object})
    \    Click Tree Node On Site Manager    ${tree_node_contain_object}[${i}]    
    \    Go To Another View On Site Manager    ${RACK_VIEW_FRONT}
    \    Check Image Not Exist On Rack View    ${splice_enclosure_names}[${i}]    ${ENCLOSURE_2U_FILENAME}
    \    Go To Another View On Site Manager    ${RACK_VIEW_ZEROU}
    \    Check Image Exist On Rack View    ${splice_enclosure_names}[${i}]    ${ENCLOSURE_2U_FILENAME}    rackViewType=${ZERO U}
   
SM-29525_03 Verify that user can edit Splice Enclosure in Auxiliary Rack/Rack/Cabinet    
    
    [Tags]    SM-29525    Manage Object
    
    [Setup]    Run Keywords    Set Test Variable    ${Building_name_03}    Building_29525_03
    ...    AND    Open SM Login Page
    ...    AND    Login To SM    ${USERNAME}    ${PASSWORD}
    ...    AND    Delete Building     ${Building_name_03}
    ...    AND    Select Main Menu    ${Administration}    Lists
    ...    AND    Remove File    ${LOC_RACVIEWIMG_2}
    ...    AND    Delete List On Administration    ${LIST_TYPE}    ${ENCLOSURE_2U}
    ...    AND    Add List On Administration    ${LIST_TYPE}    ${ENCLOSURE_2U}    ${DIR_ENCLOSURE_2U}
    
    [Teardown]    Run Keywords    Reload Page
    ...    AND    Delete Building     ${Building_name_03}   
    ...    AND    Select Main Menu    ${Administration}    Lists
    ...    AND    Remove File    ${LOC_RACVIEWIMG_2}
    ...    AND    Delete List On Administration    ${LIST_TYPE}    ${ENCLOSURE_2U} 
    ...    AND    Close Browser
    
    # 1. Launch and log into SM
    # 2. Add Building 01/Floor 01/Room 01 OR Cable Vault 01 to Site
    # 3. Add Auxiliary Rack 001 (OR Rack 001 OR Cabinet 001) to Room 01 OR Cable Vault 01
    # 4. Add Splice Enclosure 01 to Auxiliary Rack 001 (OR Rack 001 OR Cabinet 001)
    Select Main Menu    ${Site Manager}
    Create Object    ${SITE_NODE}    ${Building_name_03}    ${FLOOR_NAME}
    ${tree_node_room} =    Add Room   ${SITE_NODE}/${Building_name_03}/${FLOOR_NAME}    ${ROOM_NAME}
    ${tree_node_rack_bd} =     Add Rack    ${tree_node_room}    ${RACK_NAME}    position=${1}
    ${tree_node_cabinet_bd} =    Add Rack    ${tree_node_room}    ${CABINET_NAME}    rackType=${Cabinet (42U)}    position=${2}
    ${tree_node_auxiliary_bd} =    Add Rack    ${tree_node_room}    ${AUXILIARY_NAME}    rackType=${Auxiliary Rack}    position=${3}    capacityU=42
    ${tree_node_contain_object}    Create List    ${tree_node_rack_bd}    ${tree_node_cabinet_bd}    ${tree_node_auxiliary_bd}
    ${splice_enclosure_names}    Create List    ${SPLICE_ENCLOSURE_1}    ${SPLICE_ENCLOSURE_2}    ${SPLICE_ENCLOSURE_3}
    ${properties_name_list}    Create List    ${PROPERTIES_RACK}    ${PROPERTIES_CABINET}    ${PROPERTIES_AUXILIARY}
    ${properties_rack_list}    Create List    ${RACK_NAME_FULL}    ${CABINET_NAME_FULL}    ${AUXILIARY_NAME_FULL}
    :FOR    ${i}    IN RANGE    0    len(${tree_node_contain_object})
    \    Add Splice Enclosure    ${tree_node_contain_object}[${i}]    ${splice_enclosure_names}[${i}]
    \    Check Tree Node Exist On Site Manager    ${tree_node_contain_object}[${i}]/${splice_enclosure_names}[${i}]  
      
    # 1. Select Splice Enclosure 01 in Site pane then click Edit button on toolbar OR
	# Select Auxiliary Rack 001 (OR Rack 001 OR Cabinet 001) in Site pane; choose Splice Enclosure 01 in Content pane then click Edit button on toolbar
	# 2. On Properties window, change valid value for:
	# - Equipment Image
	# - Name
	# - Rack Units
	# - Location in Rack
	# - Description
	# - Location
	# - Slots Capacity
	# 3. Click OK button
	# - Splice Enclosure 01 can be edited with valid values
    # - Splice Enclosure 01's values display as user edited
    :FOR    ${i}    IN RANGE    0    len(${tree_node_contain_object})
    \    Edit Splice Enclosure    ${tree_node_contain_object}[${i}]/${splice_enclosure_names}[${i}]    description=${ENCLOSURE_1U}    rackUnits=${3}    
        ...    location=${tree_node_contain_object}[${i}]    equipmentImageName=${ENCLOSURE_2U}
    \    Check Object Properties On Properties Pane    ${properties_name_list}[${i}]    ${splice_enclosure_names}[${i}],${3},${FRONT},${1},${properties_rack_list}[${i}],${ENCLOSURE_1U},${tree_node_contain_object}[${i}],${6},${0},${6}
    \    Check Equipment Image Exist On Properties Pane    ${ENCLOSURE_2U_FILENAME}

SM-29525_04 Verify that there is a warning message for Enclosure in Rack if "Slots Usage" is more than "Slots Capacity"     
    
    ${Error_message}    Set Variable    Please enter an integer between 1 and 1.
    
    [Tags]    SM-29525    Manage Object
    
    [Setup]    Run Keywords    Set Test Variable    ${building_name_04}    Building_29525_04
    ...    AND    Open SM Login Page
    ...    AND    Login To SM    ${USERNAME}    ${PASSWORD}
    ...    AND    Delete Building    ${building_name_04}
    
    [Teardown]    Run Keywords    Reload Page
    ...    AND    Delete Building     ${building_name_04}    
    ...    AND    Close Browser
    
	# Launch and log into SM
	# 1.Add Building 01/Rack 01/Enclosure 01 with Slots Capacity = 6
    Select Main Menu    ${Site Manager}
    Create Object    ${SITE_NODE}    ${building_name_04}    ${FLOOR_NAME}
    ${tree_node_room} =    Add Room   ${SITE_NODE}/${building_name_04}/${FLOOR_NAME}    ${ROOM_NAME}
    ${tree_node_rack_bd} =     Add Rack    ${tree_node_room}    ${RACK_NAME}    position=${1}
    ${tray_name_list}    Create List    ${TRAY_NAME_1}    ${TRAY_NAME_2}    ${TRAY_NAME_3}    ${TRAY_NAME_4}    ${TRAY_NAME_5}
    Add Splice Enclosure    ${tree_node_rack_bd}    ${SPLICE_ENCLOSURE_1}    slotCapacity=${6}

	# 2.Add 5 Trays into Enclosure
    Add Splice Tray    ${tree_node_rack_bd}/${SPLICE_ENCLOSURE_1}    ${TRAY_NAME_1}    quantity=${5}

	# 3.Observe the result
	# _User can add 5 Trays into Enclosure 01 successfully
    :FOR    ${i}    IN RANGE    0    len(${tray_name_list})
    \    Check Object Exist On Content Table    ${tree_node_rack_bd}/${SPLICE_ENCLOSURE_1}    ${tray_name_list}[${i}]

	# 4.Add 2 more Trays into Enclosure
	Add Splice Tray    ${tree_node_rack_bd}/${SPLICE_ENCLOSURE_1}    ${TRAY_NAME_6}    quantity=${2}    confirmSave=${False}
	Click Save Quantity
	
	# 5.Observe the result
	# _There is a warning message: 
	# "Please enter an integer between 1 and 1."
    Check Popup Error Message    ${Error_message}    acceptPopup=${True}
    
	# 6.Add 1 Tray into Enclosure
    Input Quantity Add Object    ${1}	
    Click Save Quantity
    
	# 7.Observe the result    
	# _User can add 1 Tray into Enclosure 01 successfully 
    Check Object Exist On Content Table    ${tree_node_rack_bd}/${SPLICE_ENCLOSURE_1}    ${TRAY_NAME_6}

SM-29525_05 Verify that the Object Fields is shown in Enclosure properties when user set it in User-Defined Fields
    
    [Tags]    SM-29525    Manage Object
    
    [Setup]    Run Keywords    Set Test Variable    ${building_name_05}    Building_29525_05
    ...    AND    Open SM Login Page
    ...    AND    Login To SM    ${USERNAME}    ${PASSWORD}
    ...    AND    Delete Building    ${building_name_05}
	
    [TearDown]    Run Keywords    Select Main Menu    ${Administration}    ${Object Fields}
    ...    AND    Edit Object Fields Data    ${1}	${EMPTY}    objectFields=${Splice Enclosure}    objectFieldsStatus=${False}
    ...    AND    Delete Building    ${building_name_05}
    ...    AND    Close Browser

	# 1.Launch and log into SM
	# 2.Go to User-Defined Fields
	Select Main Menu    ${Administration}    ${Object Fields}
	
	# 3.Create Object Fields 01 with Splice Enclosure
    Edit Object Fields Data    ${1}    ${Object Fields} ${1}    objectFields=${Splice Enclosure}
    
	# 4.Go to Site Manager
	# 5.Add Building 01/Floor 01/Room 01/Rack 01
	# 6.Add Enclosure 01
	Select Main Menu    ${Site Manager}
    Create Object    ${SITE_NODE}    ${building_name_05}    ${FLOOR_NAME}
    ${tree_node_room} =    Add Room   ${SITE_NODE}/${building_name_05}/${FLOOR_NAME}    ${ROOM_NAME}
    ${tree_node_rack_bd} =     Add Rack    ${tree_node_room}    ${RACK_NAME}    position=${1}
    Add Splice Enclosure    ${tree_node_rack_bd}    ${SPLICE_ENCLOSURE_1}    confirmSave=${False}    waitForExist=${False}
    
	# 7.Observe the Add window
	# _There is Object Fields is shown at below:
	Check Object Properties On Properties Window    ${Object Fields} ${1}    ${EMPTY}    
	Click Save Add Object Button

SM-29525_06 Verify that user can Cut/Paste Splice Enclosure in Rack with same level by right click button in Site tree
    
    [Tags]    SM-29525    Manage Object
    
    [Setup]    Run Keywords    Set Test Variable    ${building_name_06_01}    Building_29525_06_01
    ...    AND    Set Test Variable    ${building_name_06_02}    Building_29525_06_02
    ...    AND    Set Test Variable    ${cable_vault_name_06_01}    CV_29525_06_01
    ...    AND    Set Test Variable    ${cable_vault_name_06_02}    CV_29525_06_02
    ...    AND    Open SM Login Page
    ...    AND    Login To SM    ${USERNAME}    ${PASSWORD}
    ...    AND    Delete Building    ${building_name_06_01}
    ...    AND    Delete Building    ${building_name_06_02}
    ...    AND    Delete Tree Node If Exist On Site Manager    ${Site Node}/${cable_vault_name_06_01}
    ...    AND    Delete Tree Node If Exist On Site Manager    ${Site Node}/${cable_vault_name_06_02}
	
    [TearDown]    Run Keywords    Select Main Menu    ${Site Manager}
    ...    AND    Delete Building    ${building_name_06_01}
    ...    AND    Delete Building    ${building_name_06_02}
    ...    AND    Delete Tree Node On Site Manager    ${Site Node}/${cable_vault_name_06_01}
    ...    AND    Delete Tree Node On Site Manager    ${Site Node}/${cable_vault_name_06_02}
    ...    AND    Close Browser
	
	# 1.Launch and log into SM
	# 2.Add CV 01/Rack 01-Cabinet 02-Auxiliary 03/Enclosure 01-Enclosure 02-Enclosure 03
	# 3.Add CV 02/Rack 02
	# 4.Add Building 03/Floor 03/Room 03/Rack 01-Cabinet 02-Auxiliary Rack 03/Enclosure 01-Enclosure 02-Enclosure 03
	# 5.Add Building 04/Floor 04/Room 04/Rack 04
	Select Main Menu    ${Site Manager}
    Create Object    ${SITE_NODE}    ${building_name_06_01}    ${FLOOR_NAME}    ${ROOM_NAME}
    Create Object    ${SITE_NODE}    ${building_name_06_02}    ${FLOOR_NAME}    ${ROOM_NAME}
    ${tree_node_room_1}    Set Variable    ${SITE_NODE}/${building_name_06_01}/${FLOOR_NAME}/${ROOM_NAME}
    ${tree_node_room_2}    Set Variable    ${SITE_NODE}/${building_name_06_02}/${FLOOR_NAME}/${ROOM_NAME}
    ${tree_node_cable_vault_1} =    Add Cable Vault    ${SITE_NODE}    ${cable_vault_name_06_01}    
    ${tree_node_cable_vault_2} =    Add Cable Vault    ${SITE_NODE}    ${cable_vault_name_06_02}  
    ${tree_node_rack_bd_1} =     Add Rack    ${tree_node_room_1}    ${RACK_NAME}    position=1
    ${tree_node_cabinet_bd_1} =    Add Rack    ${tree_node_room_1}    ${CABINET_NAME}    rackType=${Cabinet (42U)}    position=2
    ${tree_node_auxiliary_bd_1} =    Add Rack    ${tree_node_room_1}    ${AUXILIARY_NAME}    rackType=${Auxiliary Rack}    position=3    capacityU=42
    ${tree_node_rack_cv_1} =     Add Rack    ${tree_node_cable_vault_1}    ${RACK_NAME}    position=1
    ${tree_node_cabinet_cv_1} =    Add Rack    ${tree_node_cable_vault_1}    ${CABINET_NAME}    rackType=${Cabinet (42U)}    position=2
    ${tree_node_auxiliary_cv_1} =    Add Rack    ${tree_node_cable_vault_1}    ${AUXILIARY_NAME}    rackType=${Auxiliary Rack}    position=3    capacityU=42
    ${tree_node_rack_bd_2} =     Add Rack    ${tree_node_room_2}    ${RACK_NAME}    position=1
    ${tree_node_cabinet_bd_2} =    Add Rack    ${tree_node_room_2}    ${CABINET_NAME}    rackType=${Cabinet (42U)}    position=2
    ${tree_node_auxiliary_bd_2} =    Add Rack    ${tree_node_room_2}    ${AUXILIARY_NAME}    rackType=${Auxiliary Rack}    position=3    capacityU=42
    ${tree_node_rack_cv_2} =     Add Rack    ${tree_node_cable_vault_2}    ${RACK_NAME}    position=1
    ${tree_node_cabinet_cv_2} =    Add Rack    ${tree_node_cable_vault_2}    ${CABINET_NAME}    rackType=${Cabinet (42U)}    position=2
    ${tree_node_auxiliary_cv_2} =    Add Rack    ${tree_node_cable_vault_2}    ${AUXILIARY_NAME}    rackType=${Auxiliary Rack}    position=3    capacityU=42
    ${tree_node_contains_rack}    Create List    ${tree_node_rack_bd_1}    ${tree_node_cabinet_bd_1}    ${tree_node_auxiliary_bd_1}    ${tree_node_rack_cv_1}    ${tree_node_cabinet_cv_1}    ${tree_node_auxiliary_cv_1}
    ${splice_enclosure_names}    Create List    ${SPLICE_ENCLOSURE_1}    ${SPLICE_ENCLOSURE_2}    ${SPLICE_ENCLOSURE_3}    ${SPLICE_ENCLOSURE_1}    ${SPLICE_ENCLOSURE_2}    ${SPLICE_ENCLOSURE_3}
    
    :FOR    ${i}    IN RANGE    0    len(${tree_node_contains_rack})
    \    Add Splice Enclosure    ${tree_node_contains_rack}[${i}]    ${splice_enclosure_names}[${i}]
    \    Add Splice Tray    ${tree_node_contains_rack}[${i}]/${splice_enclosure_names}[${i}]    ${TRAY_NAME_1}    
    
	# 6.Create Cabling between:
	# -CV 01/Cabinet 02/Enclosure 02/Tray 01/01 In to Building 03/Cabinet 02/Enclosure 02/Tray 01/01 In
    # -CV 01/Auxiliary 03/Enclosure 03/Tray 01/01 In to Building 03/Auxiliary 03/Enclosure 03/Tray 01/01 In    
	Open Cabling Window    ${tree_node_auxiliary_bd_1}/${SPLICE_ENCLOSURE_3}/${TRAY_NAME_1}
    Create Cabling    cableFrom=${tree_node_auxiliary_bd_1}/${SPLICE_ENCLOSURE_3}/${TRAY_NAME_1}/${01} ${In}    cableTo=${tree_node_auxiliary_cv_1}/${SPLICE_ENCLOSURE_3}/${TRAY_NAME_1}    portsTo=${01} ${In}
    Create Cabling    cableFrom=${tree_node_cabinet_bd_1}/${SPLICE_ENCLOSURE_2}/${TRAY_NAME_1}/${01} ${In}    cableTo=${tree_node_cabinet_cv_1}/${SPLICE_ENCLOSURE_2}/${TRAY_NAME_1}    portsTo=${01} ${In}    
    Close Cabling Window

	# 7.Cut/Paste by right click button in Site tree
	# -Enclosure 01-02-03 from CV 01/Rack 01 to CV 02/Cabinet 02
	# -Enclosure 01-02-03 from Building 03/Auxiliary 03 to Building 04/Rack 01
	# 8.Observe the result
	# _Warning message:
	# "This object contains one or more ports and/or outlets that are Cabled.
	# You must remove these Cables before you can Cut the selected object."
	# _User can Cut/Paste successfully
	# _Cabling connection is removed
	Cut And Paste Object On Site Tree By Context Menu    ${tree_node_rack_cv_1}/${SPLICE_ENCLOSURE_1}    ${tree_node_cabinet_cv_1}
	Cut And Paste Object On Site Tree By Context Menu    ${tree_node_auxiliary_bd_1}/${SPLICE_ENCLOSURE_3}    ${tree_node_cabinet_cv_1}    confirmCut=${False}
	Check Warning Message    ${tree_node_auxiliary_bd_1}/${SPLICE_ENCLOSURE_3},This object contains one or more ports and/or outlets that are Cabled.,You must remove these Cables before you can Cut the selected object. 
    Reload Page
    Cut And Paste Object On Site Tree By Context Menu    ${tree_node_auxiliary_bd_1}/${SPLICE_ENCLOSURE_3}    ${tree_node_cabinet_cv_1}
    Check Tree Node Exist On Site Manager    ${tree_node_cabinet_cv_1}/${SPLICE_ENCLOSURE_3}
    Check Tree Node Exist On Site Manager    ${tree_node_cabinet_cv_1}/${SPLICE_ENCLOSURE_1}
    Check Tree Node Not Exist On Site Manager    ${tree_node_rack_cv_1}/${SPLICE_ENCLOSURE_1}    
    Check Tree Node Not Exist On Site Manager    ${tree_node_auxiliary_bd_1}/${SPLICE_ENCLOSURE_3}  
    Open Trace Window    ${tree_node_auxiliary_cv_1}/${SPLICE_ENCLOSURE_3}/${TRAY_NAME_1}    ${01} ${In}
    Check Trace Object On Site Manager    ${1}        objectPosition=${1}    objectPath=${tree_node_auxiliary_cv_1}/${SPLICE_ENCLOSURE_3}/${TRAY_NAME_1}/${01} ${In}    objectType=${TRACE_SPLICE_ENCLOSURE_IMAGE}
    ...    connectionType=${TRACE_SPLICE_INTERNAL_CONNECT_TYPE}
    Check Trace Object On Site Manager    ${2}        objectPosition=${2}    objectPath=${tree_node_auxiliary_cv_1}/${SPLICE_ENCLOSURE_3}/${TRAY_NAME_1}/${01} ${Out}    objectType=${TRACE_SPLICE_ENCLOSURE_IMAGE}
    Check Total Trace On Site Manager    ${2}
    Close Trace Window
    
    # 9.Cut/Paste by right click button in Content Table
	# -Enclosure 01-02-03 from CV 02/Cabinet 02 to CV 01/Rack 01
	# -Enclosure 01-02-03 from Building 03/Cabinet 02 to Building 04/Auxiliary 03
	# 10.Observe the result
    # _Warning message:
	# "This object contains one or more ports and/or outlets that are Cabled.
	# You must remove these Cables before you can Cut the selected object."
	# _User can Cut/Paste successfully
	# _Cabling connection is removed
	Cut And Paste Object On Content Table By Context Menu    ${tree_node_cabinet_cv_1}    ${SPLICE_ENCLOSURE_1}    ${tree_node_rack_cv_1}
	Cut And Paste Object On Content Table By Context Menu    ${tree_node_cabinet_cv_1}    ${SPLICE_ENCLOSURE_2}    ${tree_node_rack_cv_1}    confirmCut=${False}
	Check Warning Message    ${tree_node_cabinet_cv_1}/${SPLICE_ENCLOSURE_2},This object contains one or more ports and/or outlets that are Cabled.,You must remove these Cables before you can Cut the selected object. 
    Reload Page
    Cut And Paste Object On Content Table By Context Menu    ${tree_node_cabinet_cv_1}    ${SPLICE_ENCLOSURE_2}    ${tree_node_rack_cv_1}
    Check Tree Node Exist On Site Manager    ${tree_node_rack_cv_1}/${SPLICE_ENCLOSURE_2}
    Check Tree Node Exist On Site Manager    ${tree_node_rack_cv_1}/${SPLICE_ENCLOSURE_1}
    Check Tree Node Not Exist On Site Manager    ${tree_node_cabinet_cv_1}/${SPLICE_ENCLOSURE_1}
    Check Tree Node Not Exist On Site Manager    ${tree_node_cabinet_cv_1}/${SPLICE_ENCLOSURE_2}  
    Open Trace Window    ${tree_node_rack_cv_1}/${SPLICE_ENCLOSURE_2}/${TRAY_NAME_1}    ${01} ${In}
    Check Trace Object On Site Manager    ${1}        objectPosition=${1}    objectPath=${tree_node_rack_cv_1}/${SPLICE_ENCLOSURE_2}/${TRAY_NAME_1}/${01} ${In}    objectType=${TRACE_SPLICE_ENCLOSURE_IMAGE}
    ...    connectionType=${TRACE_SPLICE_INTERNAL_CONNECT_TYPE}
    Check Trace Object On Site Manager    ${2}        objectPosition=${2}    objectPath=${tree_node_rack_cv_1}/${SPLICE_ENCLOSURE_2}/${TRAY_NAME_1}/${01} ${Out}    objectType=${TRACE_SPLICE_ENCLOSURE_IMAGE}
    Check Total Trace On Site Manager    ${2}
    Close Trace Window
    
SM-29525_07 Verify that user can Cut/Paste Splice Enclosure in Rack with same level by Cut/Paste button
    
    [Tags]    SM-29525    Manage Object
    
    [Setup]    Run Keywords    Set Test Variable    ${building_name_07_01}    Building_29525_07_01
    ...    AND    Set Test Variable    ${building_name_07_02}    Building_29525_07_02
    ...    AND    Set Test Variable    ${cable_vault_name_07_01}    CV_29525_07_01
    ...    AND    Set Test Variable    ${cable_vault_name_07_02}    CV_29525_07_02
    ...    AND    Delete Building    ${building_name_07_01}
    ...    AND    Delete Building    ${building_name_07_02}
    ...    AND    Open SM Login Page
    ...    AND    Login To SM    ${USERNAME}    ${PASSWORD}
    ...    AND    Delete Tree Node If Exist On Site Manager    ${Site Node}/${cable_vault_name_07_01}
    ...    AND    Delete Tree Node If Exist On Site Manager    ${Site Node}/${cable_vault_name_07_02}
	
    [TearDown]    Run Keywords    Select Main Menu    ${Site Manager}
    ...    AND    Delete Building    ${building_name_07_01}
    ...    AND    Delete Building    ${building_name_07_02}
    ...    AND    Delete Tree Node On Site Manager    ${Site Node}/${cable_vault_name_07_01}
    ...    AND    Delete Tree Node On Site Manager    ${Site Node}/${cable_vault_name_07_02}
    ...    AND    Close Browser
	
	# 1.Launch and log into SM
	# 2.Add CV 01/Rack 01-Cabinet 02-Auxiliary 03/Enclosure 01-Enclosure 02-Enclosure 03
	# 3.Add CV 02/Rack 02
	# 4.Add Building 03/Floor 03/Room 03/Rack 01-Cabinet 02-Auxiliary Rack 03/Enclosure 01-Enclosure 02-Enclosure 03
	# 5.Add Building 04/Floor 04/Room 04/Rack 04
	Select Main Menu    ${Site Manager}
    Create Object    ${SITE_NODE}    ${building_name_07_01}    ${FLOOR_NAME}    ${ROOM_NAME}
    Create Object    ${SITE_NODE}    ${building_name_07_02}    ${FLOOR_NAME}    ${ROOM_NAME}
    ${tree_node_room_1}    Set Variable    ${SITE_NODE}/${building_name_07_01}/${FLOOR_NAME}/${ROOM_NAME}
    ${tree_node_room_2}    Set Variable    ${SITE_NODE}/${building_name_07_02}/${FLOOR_NAME}/${ROOM_NAME}
    ${tree_node_cable_vault_1} =    Add Cable Vault    ${SITE_NODE}    ${cable_vault_name_07_01}    
    ${tree_node_cable_vault_2} =    Add Cable Vault    ${SITE_NODE}    ${cable_vault_name_07_02}  
    ${tree_node_rack_bd_1} =     Add Rack    ${tree_node_room_1}    ${RACK_NAME}    position=1
    ${tree_node_cabinet_bd_1} =    Add Rack    ${tree_node_room_1}    ${CABINET_NAME}    rackType=${Cabinet (42U)}    position=2
    ${tree_node_auxiliary_bd_1} =    Add Rack    ${tree_node_room_1}    ${AUXILIARY_NAME}    rackType=${Auxiliary Rack}    position=3    capacityU=42
    ${tree_node_rack_cv_1} =     Add Rack    ${tree_node_cable_vault_1}    ${RACK_NAME}    position=1
    ${tree_node_cabinet_cv_1} =    Add Rack    ${tree_node_cable_vault_1}    ${CABINET_NAME}    rackType=${Cabinet (42U)}    position=2
    ${tree_node_auxiliary_cv_1} =    Add Rack    ${tree_node_cable_vault_1}    ${AUXILIARY_NAME}    rackType=${Auxiliary Rack}    position=3    capacityU=42
    ${tree_node_rack_bd_2} =     Add Rack    ${tree_node_room_2}    ${RACK_NAME}    position=1
    ${tree_node_cabinet_bd_2} =    Add Rack    ${tree_node_room_2}    ${CABINET_NAME}    rackType=${Cabinet (42U)}    position=2
    ${tree_node_auxiliary_bd_2} =    Add Rack    ${tree_node_room_2}    ${AUXILIARY_NAME}    rackType=${Auxiliary Rack}    position=3    capacityU=42
    ${tree_node_rack_cv_2} =     Add Rack    ${tree_node_cable_vault_2}    ${RACK_NAME}    position=1
    ${tree_node_cabinet_cv_2} =    Add Rack    ${tree_node_cable_vault_2}    ${CABINET_NAME}    rackType=${Cabinet (42U)}    position=2
    ${tree_node_auxiliary_cv_2} =    Add Rack    ${tree_node_cable_vault_2}    ${AUXILIARY_NAME}    rackType=${Auxiliary Rack}    position=3    capacityU=42
    ${tree_node_contains_rack}    Create List    ${tree_node_rack_bd_1}    ${tree_node_cabinet_bd_1}    ${tree_node_auxiliary_bd_1}    ${tree_node_rack_cv_1}    ${tree_node_cabinet_cv_1}    ${tree_node_auxiliary_cv_1}
    ${splice_enclosure_names}    Create List    ${SPLICE_ENCLOSURE_1}    ${SPLICE_ENCLOSURE_2}    ${SPLICE_ENCLOSURE_3}    ${SPLICE_ENCLOSURE_1}    ${SPLICE_ENCLOSURE_2}    ${SPLICE_ENCLOSURE_3}
    
    :FOR    ${i}    IN RANGE    0    len(${tree_node_contains_rack})
    \    Add Splice Enclosure    ${tree_node_contains_rack}[${i}]    ${splice_enclosure_names}[${i}]
    \    Add Splice Tray    ${tree_node_contains_rack}[${i}]/${splice_enclosure_names}[${i}]    ${TRAY_NAME_1}    
    
	# 6.Create Cabling between:
	# -CV 01/Cabinet 02/Enclosure 02/Tray 01/01 In to Building 03/Cabinet 02/Enclosure 02/Tray 01/01 In
    # -CV 01/Auxiliary 03/Enclosure 03/Tray 01/01 In to Building 03/Auxiliary 03/Enclosure 03/Tray 01/01 In    
	Open Cabling Window    ${tree_node_auxiliary_bd_1}/${SPLICE_ENCLOSURE_3}/${TRAY_NAME_1}
    Create Cabling    cableFrom=${tree_node_auxiliary_bd_1}/${SPLICE_ENCLOSURE_3}/${TRAY_NAME_1}/${01} ${In}    cableTo=${tree_node_auxiliary_cv_1}/${SPLICE_ENCLOSURE_3}/${TRAY_NAME_1}    portsTo=${01} ${In}
    Create Cabling    cableFrom=${tree_node_cabinet_bd_1}/${SPLICE_ENCLOSURE_2}/${TRAY_NAME_1}/${01} ${In}    cableTo=${tree_node_cabinet_cv_1}/${SPLICE_ENCLOSURE_2}/${TRAY_NAME_1}    portsTo=${01} ${In}    
    Close Cabling Window

	# 7.Cut/Paste by Cut/Paste button in Site tree
	# -Enclosure 01-02-03 from CV 01/Rack 01 to CV 02/Cabinet 02
	# -Enclosure 01-02-03 from Building 03/Auxiliary 03 to Building 04/Rack 01
	# 8.Observe the result
	# _Warning message:
	# "This object contains one or more ports and/or outlets that are Cabled.
	# You must remove these Cables before you can Cut the selected object."
	# _User can Cut/Paste successfully
	# _Cabling connection is removed
	Cut And Paste Object On Site Tree    ${tree_node_rack_cv_1}/${SPLICE_ENCLOSURE_1}    ${tree_node_cabinet_cv_1}
	Cut And Paste Object On Site Tree    ${tree_node_auxiliary_bd_1}/${SPLICE_ENCLOSURE_3}    ${tree_node_cabinet_cv_1}    confirmCut=${False}
	Check Warning Message    ${tree_node_auxiliary_bd_1}/${SPLICE_ENCLOSURE_3},This object contains one or more ports and/or outlets that are Cabled.,You must remove these Cables before you can Cut the selected object. 
    Reload Page
    Cut And Paste Object On Site Tree    ${tree_node_auxiliary_bd_1}/${SPLICE_ENCLOSURE_3}    ${tree_node_cabinet_cv_1}
    Check Tree Node Exist On Site Manager    ${tree_node_cabinet_cv_1}/${SPLICE_ENCLOSURE_1}
    Check Tree Node Exist On Site Manager    ${tree_node_cabinet_cv_1}/${SPLICE_ENCLOSURE_3}
    Check Tree Node Not Exist On Site Manager    ${tree_node_rack_cv_1}/${SPLICE_ENCLOSURE_1}    
    Check Tree Node Not Exist On Site Manager    ${tree_node_auxiliary_bd_1}/${SPLICE_ENCLOSURE_3}  
    Open Trace Window    ${tree_node_auxiliary_cv_1}/${SPLICE_ENCLOSURE_3}/${TRAY_NAME_1}    ${01} ${In}
    Check Trace Object On Site Manager    ${1}        objectPosition=${1}    objectPath=${tree_node_auxiliary_cv_1}/${SPLICE_ENCLOSURE_3}/${TRAY_NAME_1}/${01} ${In}    objectType=${TRACE_SPLICE_ENCLOSURE_IMAGE}
    ...    connectionType=${TRACE_SPLICE_INTERNAL_CONNECT_TYPE}
    Check Trace Object On Site Manager    ${2}        objectPosition=${2}    objectPath=${tree_node_auxiliary_cv_1}/${SPLICE_ENCLOSURE_3}/${TRAY_NAME_1}/${01} ${Out}    objectType=${TRACE_SPLICE_ENCLOSURE_IMAGE}
    Check Total Trace On Site Manager    ${2}
    Close Trace Window
    
    # 9.Cut/Paste by Cut/Paste button in Content Table
	# -Enclosure 01-02-03 from CV 02/Cabinet 02 to CV 01/Rack 01
	# -Enclosure 01-02-03 from Building 03/Cabinet 02 to Building 04/Auxiliary 03
	# 10.Observe the result
    # _Warning message:
	# "This object contains one or more ports and/or outlets that are Cabled.
	# You must remove these Cables before you can Cut the selected object."
	# _User can Cut/Paste successfully
	# _Cabling connection is removed
	Cut And Paste Object On Content Table    ${tree_node_cabinet_cv_1}    ${SPLICE_ENCLOSURE_1}    ${tree_node_rack_cv_1}
	Cut And Paste Object On Content Table    ${tree_node_cabinet_cv_1}    ${SPLICE_ENCLOSURE_2}    ${tree_node_rack_cv_1}    confirmCut=${False}
	Check Warning Message    ${tree_node_cabinet_cv_1}/${SPLICE_ENCLOSURE_2},This object contains one or more ports and/or outlets that are Cabled.,You must remove these Cables before you can Cut the selected object.
    Reload Page
    Cut And Paste Object On Content Table    ${tree_node_cabinet_cv_1}    ${SPLICE_ENCLOSURE_2}    ${tree_node_rack_cv_1}
    Check Tree Node Exist On Site Manager    ${tree_node_rack_cv_1}/${SPLICE_ENCLOSURE_1}
    Check Tree Node Exist On Site Manager    ${tree_node_rack_cv_1}/${SPLICE_ENCLOSURE_2}
    Check Tree Node Not Exist On Site Manager    ${tree_node_cabinet_cv_1}/${SPLICE_ENCLOSURE_1}
    Check Tree Node Not Exist On Site Manager    ${tree_node_cabinet_cv_1}/${SPLICE_ENCLOSURE_2}  
    Open Trace Window    ${tree_node_rack_cv_1}/${SPLICE_ENCLOSURE_2}/${TRAY_NAME_1}    ${01} ${In}
    Check Trace Object On Site Manager    ${1}        objectPosition=${1}    objectPath=${tree_node_rack_cv_1}/${SPLICE_ENCLOSURE_2}/${TRAY_NAME_1}/${01} ${In}    objectType=${TRACE_SPLICE_ENCLOSURE_IMAGE}
    ...    connectionType=${TRACE_SPLICE_INTERNAL_CONNECT_TYPE}
    Check Trace Object On Site Manager    ${2}        objectPosition=${2}    objectPath=${tree_node_rack_cv_1}/${SPLICE_ENCLOSURE_2}/${TRAY_NAME_1}/${01} ${Out}    objectType=${TRACE_SPLICE_ENCLOSURE_IMAGE}
    Check Total Trace On Site Manager    ${2}
    Close Trace Window

SM-29525_08 Verify that user can Drag/Drop Splice Enclosure in Rack with same level to another location 
    
    [Tags]    SM-29525    Manage Object
    
    [Setup]    Run Keywords    Set Test Variable    ${building_name_08}    Building_29525_08
    ...    AND    Set Test Variable    ${campus_name_08}    Campus_29525_08
    ...    AND    Set Test Variable    ${cable_vault_name_08_01}    CV_29525_08_01
    ...    AND    Set Test Variable    ${cable_vault_name_08_02}    CV_29525_08_02
    ...    AND    Delete Building    ${building_name_08}
    ...    AND    Open SM Login Page
    ...    AND    Login To SM    ${USERNAME}    ${PASSWORD}
    ...    AND    Delete Tree Node If Exist On Site Manager    ${Site Node}/${cable_vault_name_08_01}
    ...    AND    Delete Tree Node If Exist On Site Manager    ${Site Node}/${cable_vault_name_08_02}
    ...    AND    Delete Tree Node If Exist On Site Manager    ${Site Node}/${campus_name_08}
	
    [TearDown]    Run Keywords    Select Main Menu    ${Site Manager}
    ...    AND    Delete Building    ${building_name_08}
    ...    AND    Delete Tree Node On Site Manager    ${Site Node}/${cable_vault_name_08_01}
    ...    AND    Delete Tree Node On Site Manager    ${Site Node}/${cable_vault_name_08_02}
    ...    AND    Delete Tree Node On Site Manager    ${Site Node}/${campus_name_08}
    ...    AND    Close Browser

	# 1.Launch and log into SM
	# 2.Add CV 01/Rack 01/Enclosure 01/Tray 01
	# 3.Add CV 02/Rack 02
	# 3.Building 03/Cabinet 03
	# 4.Campus 04/Building 04/Auxiliary Rack 04/Enclosure 04/Tray 04
	Select Main Menu    ${Site Manager}
	Add Campus    ${SITE_NODE}    ${campus_name_08}    
    Create Object    ${SITE_NODE}    ${building_name_08}    ${FLOOR_NAME}    ${ROOM_NAME}
    Create Object    ${SITE_NODE}/${campus_name_08}    ${building_name}    ${FLOOR_NAME}    ${ROOM_NAME}
    ${tree_node_room_1}    Set Variable    ${SITE_NODE}/${building_name_08}/${FLOOR_NAME}/${ROOM_NAME}
    ${tree_node_room_2}    Set Variable    ${SITE_NODE}/${campus_name_08}/${building_name}/${FLOOR_NAME}/${ROOM_NAME}
    ${tree_node_cable_vault_1} =    Add Cable Vault    ${SITE_NODE}    ${cable_vault_name_08_01}    
    ${tree_node_cable_vault_2} =    Add Cable Vault    ${SITE_NODE}    ${cable_vault_name_08_02}  
    
    ${tree_node_rack_cv_1} =     Add Rack    ${tree_node_cable_vault_1}    ${RACK_NAME}
    ${tree_node_rack_cv_2} =     Add Rack    ${tree_node_cable_vault_2}    ${RACK_NAME}
    ${tree_node_cabinet_bd_1} =    Add Rack    ${tree_node_room_1}    ${CABINET_NAME}    rackType=${Cabinet (42U)}
    ${tree_node_auxiliary_bd_2} =    Add Rack    ${tree_node_room_2}    ${AUXILIARY_NAME}    rackType=${Auxiliary Rack}    capacityU=42
    ${tree_node_contains_rack}    Create List    ${tree_node_rack_cv_1}    ${tree_node_auxiliary_bd_2}
    ${splice_enclosure_names}    Create List    ${SPLICE_ENCLOSURE_1}    ${SPLICE_ENCLOSURE_2}
    
    :FOR    ${i}    IN RANGE    0    len(${tree_node_contains_rack})
    \    Add Splice Enclosure    ${tree_node_contains_rack}[${i}]    ${splice_enclosure_names}[${i}]
    \    Add Splice Tray    ${tree_node_contains_rack}[${i}]/${splice_enclosure_names}[${i}]    ${TRAY_NAME_1}    

	# 5.Create Cabling between:
	# Tray 01/01 in to Tray 04/01 In
    Open Cabling Window    ${tree_node_rack_cv_1}/${SPLICE_ENCLOSURE_1}/${TRAY_NAME_1}
    Create Cabling    cableFrom=${tree_node_rack_cv_1}/${SPLICE_ENCLOSURE_1}/${TRAY_NAME_1}/${01} ${In}    cableTo=${tree_node_auxiliary_bd_2}/${SPLICE_ENCLOSURE_2}/${TRAY_NAME_1}    portsTo=${01} ${In}
    Close Cabling Window

	# 6.Drag then drop Enclosure 01 to Cabinet 03, Enclosure 04 to Auxiliary Rack 02
	# 7.Observe the result
	# _Warning message: 
	# "This object contains one or more ports and/or outlets that are Cabled.
	# You must remove these Cables before you can Move the selected object."
	# _User can drag then drop Enclosure successfully
	# _Cabling connection is removed
	Drag And Drop Object On Site Tree    ${tree_node_rack_cv_1}/${SPLICE_ENCLOSURE_1}    ${tree_node_cabinet_bd_1}    isDrop=${False}
    Check Warning Message    ${tree_node_rack_cv_1}/${SPLICE_ENCLOSURE_1},This object contains one or more ports and/or outlets that are Cabled.,You must remove these Cables before you can Move the selected object.
    Reload Page
    Drag And Drop Object On Site Tree    ${tree_node_rack_cv_1}/${SPLICE_ENCLOSURE_1}    ${tree_node_cabinet_bd_1}
    Drag And Drop Object On Site Tree    ${tree_node_auxiliary_bd_2}/${SPLICE_ENCLOSURE_2}    ${tree_node_rack_cv_2}
    Check Tree Node Exist On Site Manager    ${tree_node_cabinet_bd_1}/${SPLICE_ENCLOSURE_1}
    Check Tree Node Exist On Site Manager    ${tree_node_rack_cv_2}/${SPLICE_ENCLOSURE_2}
    Check Tree Node Not Exist On Site Manager    ${tree_node_rack_cv_1}/${SPLICE_ENCLOSURE_1}
    Check Tree Node Not Exist On Site Manager    ${tree_node_auxiliary_bd_2}/${SPLICE_ENCLOSURE_2}  
    Open Trace Window    ${tree_node_rack_cv_2}/${SPLICE_ENCLOSURE_2}/${TRAY_NAME_1}    ${01} ${In}
    Check Trace Object On Site Manager    ${1}        objectPosition=${1}    objectPath=${tree_node_rack_cv_2}/${SPLICE_ENCLOSURE_2}/${TRAY_NAME_1}/${01} ${In}    objectType=${TRACE_SPLICE_ENCLOSURE_IMAGE}
    ...    connectionType=${TRACE_SPLICE_INTERNAL_CONNECT_TYPE}
    Check Trace Object On Site Manager    ${2}        objectPosition=${2}    objectPath=${tree_node_rack_cv_2}/${SPLICE_ENCLOSURE_2}/${TRAY_NAME_1}/${01} ${Out}    objectType=${TRACE_SPLICE_ENCLOSURE_IMAGE}
    Check Total Trace On Site Manager    ${2}
    Close Trace Window

SM-29525_09 Verify that user can delete Splice Enclosure in Auxiliary Rack/Rack/Cabinet on Site Tree by Delete button
    
    [Tags]    SM-29525    Manage Object
    
    [Setup]    Run Keywords    Set Test Variable    ${building_name_09}    Building_29525_09
    ...    AND    Set Test Variable    ${cable_vault_name_09}    CV_29525_09
    ...    AND    Delete Building    ${building_name_09}
    ...    AND    Open SM Login Page
    ...    AND    Login To SM    ${USERNAME}    ${PASSWORD}
    ...    AND    Delete Tree Node If Exist On Site Manager    ${Site Node}/${cable_vault_name_09}
	
    [TearDown]    Run Keywords    Select Main Menu    ${Site Manager}
    ...    AND    Delete Building    ${building_name_09}
    ...    AND    Delete Tree Node On Site Manager    ${Site Node}/${cable_vault_name_09}
    ...    AND    Close Browser

	# 1. Launch and log into SM
	# 2. Add Building 01/Floor 01/Room 01 OR Cable Vault 01 to Site
	# 3. Add Auxiliary Rack 001 (OR Rack 001 OR Cabinet 001) to Room 01 OR Cable Vault 01
	# 4. Add Splice Enclosure 01 to Auxiliary Rack 001 (OR Rack 001 OR Cabinet 001)
	Select Main Menu    ${Site Manager}
    Create Object    ${SITE_NODE}    ${building_name_09}    ${FLOOR_NAME}    ${ROOM_NAME}
    ${tree_node_room}    Set Variable    ${SITE_NODE}/${building_name_09}/${FLOOR_NAME}/${ROOM_NAME}
    ${tree_node_cable_vault} =    Add Cable Vault    ${SITE_NODE}    ${cable_vault_name_09}    
    ${tree_node_rack_bd} =     Add Rack    ${tree_node_room}    ${RACK_NAME}    position=1
    ${tree_node_cabinet_bd} =    Add Rack    ${tree_node_room}    ${CABINET_NAME}    rackType=${Cabinet (42U)}    position=2
    ${tree_node_rack_cv} =     Add Rack    ${tree_node_cable_vault}    ${RACK_NAME}    position=1
    ${tree_node_auxiliary_cv} =    Add Rack    ${tree_node_cable_vault}    ${AUXILIARY_NAME}    rackType=${Auxiliary Rack}    position=2    capacityU=42
    ${tree_node_contains_rack}    Create List    ${tree_node_rack_bd}    ${tree_node_cabinet_bd}    ${tree_node_rack_cv}    ${tree_node_auxiliary_cv}
    ${splice_enclosure_names}    Create List    ${SPLICE_ENCLOSURE_1}    ${SPLICE_ENCLOSURE_2}    ${SPLICE_ENCLOSURE_1}    ${SPLICE_ENCLOSURE_2}
    ${tree_node_delete_btn}    Create List    ${tree_node_rack_bd}/${SPLICE_ENCLOSURE_1}    ${tree_node_cabinet_bd}/${SPLICE_ENCLOSURE_2}
    ${tree_node_delete_context}    Create List    ${tree_node_rack_cv}/${SPLICE_ENCLOSURE_1}    ${tree_node_auxiliary_cv}/${SPLICE_ENCLOSURE_2}
    ${tree_node_deleted}    Create List    ${tree_node_rack_bd}/${SPLICE_ENCLOSURE_1}    ${tree_node_cabinet_bd}/${SPLICE_ENCLOSURE_2}    ${tree_node_rack_cv}/${SPLICE_ENCLOSURE_1}    ${tree_node_auxiliary_cv}/${SPLICE_ENCLOSURE_2}
	
    :FOR    ${i}    IN RANGE    0    len(${tree_node_contains_rack})
    \    Add Splice Enclosure    ${tree_node_contains_rack}[${i}]    ${splice_enclosure_names}[${i}]

	# 5. Select Splice Enclosure 01 on Site Tree then click Delete button on Toolbar
	# 6. Select OK on message
	# 7. Observe the result
	# Splice Enclosure 01 is deleted
	# 8. Select Splice Enclosure 01 on Site Tree, right click then select Delete option in context menu
	# 9. Select OK on message
	# 10. Observe the result
	# Splice Enclosure 01 is deleted
	:FOR    ${i}    IN RANGE    0    len(${tree_node_delete_btn})
    \    Delete Tree Node On Site Manager    ${tree_node_delete_btn}[${i}]    
    \    Delete Tree Node On Site Tree By Context Menu    ${tree_node_delete_context}[${i}]
    \    Check Tree Node Not Exist On Site Manager    ${tree_node_deleted}[${i}]    
    \    Check Object Not Exist On Content Table    ${tree_node_contains_rack}[${i}]    ${splice_enclosure_names}[${i}]
  
SM-29525_10 Verify that user can delete Splice Enclosure in Auxiliary Rack/Rack/Cabinet on Content pane by Delete button
    
    [Tags]    SM-29525    Manage Object
    
    [Setup]    Run Keywords    Set Test Variable    ${building_name_10}    Building_29525_10
    ...    AND    Set Test Variable    ${cable_vault_name_10}    CV_29525_10
    ...    AND    Delete Building    ${building_name_10}
    ...    AND    Open SM Login Page
    ...    AND    Login To SM    ${USERNAME}    ${PASSWORD}
    ...    AND    Delete Tree Node If Exist On Site Manager    ${Site Node}/${cable_vault_name_10}
	
    [TearDown]    Run Keywords    Select Main Menu    ${Site Manager}
    ...    AND    Delete Building    ${building_name_10}
    ...    AND    Delete Tree Node On Site Manager    ${Site Node}/${cable_vault_name_10}
    ...    AND    Close Browser

	# 1. Launch and log into SM
	# 2. Add Building 01/Floor 01/Room 01 OR Cable Vault 01 to Site
	# 3. Add Auxiliary Rack 001 (OR Rack 001 OR Cabinet 001) to Room 01 OR Cable Vault 01
	# 4. Add Splice Enclosure 01 to Auxiliary Rack 001 (OR Rack 001 OR Cabinet 001)
	Select Main Menu    ${Site Manager}
    Create Object    ${SITE_NODE}    ${building_name_10}    ${FLOOR_NAME}    ${ROOM_NAME}
    ${tree_node_room}    Set Variable    ${SITE_NODE}/${building_name_10}/${FLOOR_NAME}/${ROOM_NAME}
    ${tree_node_cable_vault} =    Add Cable Vault    ${SITE_NODE}    ${cable_vault_name_10}    
    ${tree_node_rack_bd} =     Add Rack    ${tree_node_room}    ${RACK_NAME}    position=1
    ${tree_node_cabinet_bd} =    Add Rack    ${tree_node_room}    ${CABINET_NAME}    rackType=${Cabinet (42U)}    position=2
    ${tree_node_rack_cv} =     Add Rack    ${tree_node_cable_vault}    ${RACK_NAME}    position=1
    ${tree_node_auxiliary_cv} =    Add Rack    ${tree_node_cable_vault}    ${AUXILIARY_NAME}    rackType=${Auxiliary Rack}    position=2    capacityU=42
    ${tree_node_contains_rack}    Create List    ${tree_node_rack_bd}    ${tree_node_cabinet_bd}    ${tree_node_rack_cv}    ${tree_node_auxiliary_cv}
    ${splice_enclosure_names}    Create List    ${SPLICE_ENCLOSURE_1}    ${SPLICE_ENCLOSURE_2}    ${SPLICE_ENCLOSURE_1}    ${SPLICE_ENCLOSURE_2}
    ${tree_node_delete}    Create List    ${tree_node_rack_bd}    ${tree_node_cabinet_bd}
    ${tree_node_contain_delete}    Create List    ${tree_node_rack_cv}    ${tree_node_auxiliary_cv}
    ${object_delete}    Create List    ${SPLICE_ENCLOSURE_1}    ${SPLICE_ENCLOSURE_2}
    ${tree_node_deleted}    Create List    ${tree_node_rack_bd}/${SPLICE_ENCLOSURE_1}    ${tree_node_cabinet_bd}/${SPLICE_ENCLOSURE_2}    ${tree_node_rack_cv}/${SPLICE_ENCLOSURE_1}    ${tree_node_auxiliary_cv}/${SPLICE_ENCLOSURE_2}
	
    :FOR    ${i}    IN RANGE    0    len(${tree_node_contains_rack})
    \    Add Splice Enclosure    ${tree_node_contains_rack}[${i}]    ${splice_enclosure_names}[${i}]

	# 5. Select Splice Enclosure 01 on Site Tree then click Delete button on Toolbar
	# 6. Select OK on message
	# 7. Observe the result
	# Splice Enclosure 01 is deleted
	# 8. Select Splice Enclosure 01 on Site Tree, right click then select Delete option in context menu
	# 9. Select OK on message
	# 10. Observe the result
	# Splice Enclosure 01 is deleted
	:FOR    ${i}    IN RANGE    0    len(${tree_node_delete})
    \    Delete Object On Content Table    ${tree_node_delete}[${i}]    ${object_delete}[${i}]
    \    Delete Object On Content Table By Context Menu    ${tree_node_contain_delete}[${i}]    ${object_delete}[${i}]
    \    Check Tree Node Not Exist On Site Manager    ${tree_node_deleted}[${i}]    
    \    Check Object Not Exist On Content Table    ${tree_node_contains_rack}[${i}]    ${splice_enclosure_names}[${i}]

