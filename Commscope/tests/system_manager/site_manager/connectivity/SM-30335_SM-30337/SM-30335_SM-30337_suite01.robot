*** Settings ***
Resource    ../../../../../resources/constants.robot
Resource    ../../../../../resources/icons_constants.robot

Library    ../../../../../py_sources/logigear/setup.py
Library    ../../../../../py_sources/logigear/api/SimulatorApi.py
Library    ../../../../../py_sources/logigear/api/BuildingApi.py    ${USERNAME}    ${PASSWORD}   
Library    ../../../../../py_sources/logigear/page_objects/system_manager/site_manager/SiteManagerPage${PLATFORM}.py        
Library    ../../../../../py_sources/logigear/page_objects/system_manager/login/LoginPage${PLATFORM}.py
Library    ../../../../../py_sources/logigear/page_objects/system_manager/site_manager/connections/cabling/CablingPage${PLATFORM}.py       
Library    ../../../../../py_sources/logigear/page_objects/system_manager/site_manager/connections/patching/PatchingPage${PLATFORM}.py
Library    ../../../../../py_sources/logigear/page_objects/system_manager/site_manager/connections/front_to_back_cabling/FrontToBackCablingPage${PLATFORM}.py    
Library    ../../../../../py_sources/logigear/page_objects/system_manager/site_manager/work_orders/create_work_order/CreateWorkOrderPage${PLATFORM}.py
Library    ../../../../../py_sources/logigear/page_objects/system_manager/site_manager/work_orders/work_order_queue/WorkOrderQueuePage${PLATFORM}.py   

Default Tags    Connectivity
Force Tags    SM-30335_SM-30337

*** Variables ***
${FLOOR_NAME}    Floor 01
${ROOM_NAME}    Room 01
${RACK_NAME}    Rack 001
${POSITION_RACK_NAME}    1:1 Rack 001
${PANEL_NAME_1}    Panel 01
${PANEL_NAME_2}    Panel 02
${PANEL_NAME_3}    Panel 03
${PANEL_NAME_4}    Panel 04
${SERVER_NAME_1}    Server 01
${SWITCH_NAME_1}    Switch 01   

*** Test Cases *** 
SM-30337-02_Verify that the user cannot cable Non DM24 Panel with new MPO24-12xLC EB assembly
    [Setup]    Run Keywords    Set Test Variable    ${building_name}    BD_SM30337_02
    ...    AND    Delete Building    name=${building_name}
    ...    AND    Add New Building    ${building_name}       
    ...    AND    Open SM Login Page
    ...    AND    Login To SM    ${USERNAME}    ${PASSWORD}
         
    [Teardown]    Run Keywords    Delete Building    name=${building_name}
    ...    AND    Close Browser
    
    Set Tags    SM-30337  
    
    ${tree_node_rack}    Set Variable    ${SITE_NODE}/${building_name}/${FLOOR_NAME}/${ROOM_NAME}/${POSITION_RACK_NAME}
	
	# 3. Add to Rack 001:
	# _MPO Generic Panel 01
	# _MPO24 Switch 01 Port
	# _Add DM24 Panel 02
    
    Add Floor    ${SITE_NODE}/${building_name}    ${FLOOR_NAME}
    Add Room    ${SITE_NODE}/${building_name}/${FLOOR_NAME}    ${ROOM_NAME}    
    Add Rack    ${SITE_NODE}/${building_name}/${FLOOR_NAME}/${ROOM_NAME}    ${RACK_NAME}
    Add Generic Panel    ${tree_node_rack}    ${PANEL_NAME_1}    portType=${MPO}
    Add Systimax Fiber Equipment    ${tree_node_rack}    ${360 G2 Fiber Shelf (1U)}    ${PANEL_NAME_2}    ${Module 1A},${True},${LC 12 Port},${DM24}
    Add Network Equipment    ${SITE_NODE}/${building_name}/${FLOOR_NAME}/${ROOM_NAME}/${POSITION_RACK_NAME}    ${SWITCH_NAME_1}    
    Add Network Equipment Port    ${tree_node_rack}/${SWITCH_NAME_1}    ${01}    ${MPO24}   

	# 4. Select Switch 01 / 01 and open Cabling window
	# VP: (MPO24-12xLC EB) in MPO24 Connection Type is disabled
	# 5. Close Cabling window
	# 6. Select Panel 01 / 01 and open Cabling window
	# VP: (MPO24-12xLC EB) in MPO24 Connection Type is disabled
	# 7. Select Switch 01 / 01 in From Tree
	# VP: (MPO24-12xLC EB) in MPO24 Connection Type is disabled
	# 8. Select Panel 01 / 01 in From Tree
	# VP: (MPO24-12xLC EB) in MPO24 Connection Type is disabled
	# 9. Select Panel 02 / Module 1A (MD24) / MPO 01 in From Tree
	# 10. Select MPO24-12xLC EB icon in MPO24 Connection Type 
	# 11. Select Switch 01 / 01 in To Tree
	# 12. Observe the state of Connect button
	# The Connect button is disabled.
	Open Cabling Window    ${tree_node_rack}/${SWITCH_NAME_1}    ${01}
	Check Mpo Connection Type State On Cabling Window    mpoConnectionType=${Mpo24_12xLC_EB}    isEnabled=${False}        
	Close Cabling Window
	Open Cabling Window    ${tree_node_rack}/${PANEL_NAME_1}    ${01}
	Check Mpo Connection Type State On Cabling Window    mpoConnectionType=${Mpo24_12xLC_EB}    isEnabled=${False}    mpoConnectionTab=${MPO24}
    Click Tree Node Cabling    From    ${tree_node_rack}/${SWITCH_NAME_1}/${01}    
    Check Mpo Connection Type State On Cabling Window    mpoConnectionType=${Mpo24_12xLC_EB}    isEnabled=${False}    mpoConnectionTab=${MPO24}	
	Click Tree Node Cabling    From    ${tree_node_rack}/${PANEL_NAME_1}/${01}    
    Check Mpo Connection Type State On Cabling Window    mpoConnectionType=${Mpo24_12xLC_EB}    isEnabled=${False}    mpoConnectionTab=${MPO24}	
    Click Tree Node Cabling    From    ${tree_node_rack}/${PANEL_NAME_2}/${Module 1A} (${DM24})/${MPO} ${01}
    Check Mpo Connection Type Information On Cabling Window    mpoConnectionTab=${MPO24}    mpoConnectionType=${Mpo24_12xLC_EB}
    Click Tree Node Cabling    To    ${tree_node_rack}/${SWITCH_NAME_1}/${01}
    Check Connect Button State On Cabling Window    isEnabled=${False}         
	Close Cabling Window
		    
SM-30337-03_Verify the new image, tooltip for MPO24-12xLC, MPO24-12xLC EB on Cabling is displayed only
    [Setup]    Run Keywords    Set Test Variable    ${building_name}    BD_SM30337_03
    ...    AND    Delete Building    name=${building_name}
    ...    AND    Add New Building    ${building_name}       
    ...    AND    Open SM Login Page
    ...    AND    Login To SM    ${USERNAME}    ${PASSWORD}
         
    [Teardown]    Run Keywords    Delete Building    name=${building_name}
    ...    AND    Close Browser
    
    Set Tags    SM-30337
    
    ${tree_node_rack}    Set Variable    ${SITE_NODE}/${building_name}/${FLOOR_NAME}/${ROOM_NAME}/${POSITION_RACK_NAME}
    
    # 3. Add DM24 Panel 01 into Rack
    
    Add Floor    ${SITE_NODE}/${building_name}    ${FLOOR_NAME}
    Add Room    ${SITE_NODE}/${building_name}/${FLOOR_NAME}    ${ROOM_NAME}    
    Add Rack    ${SITE_NODE}/${building_name}/${FLOOR_NAME}/${ROOM_NAME}    ${RACK_NAME}
    Add Systimax Fiber Equipment    ${tree_node_rack}    ${360 G2 Fiber Shelf (1U)}    ${PANEL_NAME_1}    ${Module 1A},${True},${LC 12 Port},${DM24}
    Add Generic Panel    ${tree_node_rack}    ${PANEL_NAME_2}    portType=${MPO}        
    
    # 4. Open cabling window for Panel 01/01
    # 5. Select Panel 01 / MPO 1
    # 6. Observe the icon in MPO24 Connection Type
    # VP: The new image for MPO24-12xLC EB on Cabling is displayed
    # 7. Hover over MPO24-12xLC EB icon
    # VP: Tooltip Method B Enhanced is displayed for  MPO24-12xLC EB
    # 8. Select MPO24-12xLC EB icon
    # 9. Observe the Pair list in Branch		
    # VP: Assembly Branch Naming is Pair 1, Pair 2, Pair 3, Pair 4, Pair 5, Pair 6, Pair 7, Pair 8, Pair 9,  Pair 10, Pair 11, Pair 12.   
    # 10. Select Panel 02 / 01
    # 11. Hover over MPO24-12xLC icon
    # VP: Tooltip Method B is displayed for MPO24-12xLC
    # 12. Close Cabling window
     
    Open Cabling Window    ${tree_node_rack}/${PANEL_NAME_1}/${Module 1A} (${DM24})    ${01}  
    Click Tree Node Cabling    From    ${tree_node_rack}/${PANEL_NAME_1}/${Module 1A} (${DM24})/${MPO} ${01}
    Check Mpo Connection Type Information On Cabling Window    mpoConnectionTab=${MPO24}    mpoConnectionType=${Mpo24_12xLC_EB}    title=${Method B Enhanced}
    ...    mpoBranches=${Pair 1},${Pair 2},${Pair 3},${Pair 4},${Pair 5},${Pair 6},${Pair 7},${Pair 8},${Pair 9},${ Pair 10},${Pair 11},${Pair 12}  
    Click Tree Node Cabling    From    ${tree_node_rack}/${PANEL_NAME_2}/${01}
    Check Mpo Connection Type Information On Cabling Window    mpoConnectionTab=${MPO24}    mpoConnectionType=${Mpo24_12xLC}    title=${Method B}    
    Close Cabling Window
    
	# 13. Select Panel 01/01 and open Patching window
	# 14. Observe the icons in MPO24 Connection Type tab
	# 15. Close Patching window
	# 16.Open Front to Back Cabling window
	# 17. Observe the icons in MPO24 Connection Type tab
	# VP: (MPO24-12xLC EB) is NOT displayed in  MPO24 Connection Type
	Open Patching Window    ${tree_node_rack}/${PANEL_NAME_1}/${Module 1A} (${DM24})    ${01}	
	Check Mpo Connection Type Icon Not Exist On Patching Window    mpoConnectionType=${Mpo24_12xLC_EB}    mpoConnectionTab=${MPO24}      
	Close Patching Window
	Open Front To Back Cabling Window    ${tree_node_rack}/${PANEL_NAME_1}/${Module 1A} (${DM24})    ${01}
	Report Bug    SM-30584
	Check Mpo Connection Type Icon Not Exist On Front To Back Cabling Window    mpoConnectionType=${Mpo24_12xLC_EB}    mpoConnectionTab=${MPO24}
	Close Front To Back Cabling Window
  
SM-30335_SM-30337_05_Verify that the service is propagated and trace correctly in new customer circuit (DGT)
    [Setup]    Run Keywords    Set Test Variable    ${building_name}    BD_SM30335_SM30337_05
    ...    AND    Delete Building    name=${building_name}
    ...    AND    Add New Building    ${building_name}          
    ...    AND    Open SM Login Page
    ...    AND    Login To SM    ${USERNAME}    ${PASSWORD}      
 
    [Teardown]    Run Keywords    Delete Building    name=${building_name}
    ...    AND    Close Browser
    
    Set Tags    SM-30335    SM-30337
    ${work_order_1}    Set Variable    WO_SM30335_05_01
    ${work_order_2}    Set Variable    WO_SM30335_05_02
    ${tree_node_room}    Set Variable    ${SITE_NODE}/${building_name}/${FLOOR_NAME}/${ROOM_NAME}
    ${tree_node_rack}    Set Variable    ${tree_node_room}/${POSITION_RACK_NAME}
    ${tree_node_panel_1_module_1a}    Set Variable    ${tree_node_rack}/${PANEL_NAME_1}/${Module 1A} (${DM24})
    ${tree_node_panel_2_module_1a}    Set Variable    ${tree_node_rack}/${PANEL_NAME_2}/${Module 1A} (${DM24})
    ${tree_node_panel_3_module_1a}    Set Variable    ${tree_node_rack}/${PANEL_NAME_3}/${Module 1A} (${DM24})    
    ${tree_node_server_1}    Set Variable    ${tree_node_rack}/${SERVER_NAME_1}
    ${tree_node_switch_1}    Set Variable    ${tree_node_rack}/${SWITCH_NAME_1}
    ${device_information_rack}    Set Variable    ${POSITION_RACK_NAME}->${ROOM_NAME}->${FLOOR_NAME}->${building_name}->${SITE_NODE}          

    ${port_list}    Create List    ${01}    ${02}    ${03}    ${04}    ${05}    ${06}    ${07}    ${08}    ${09}    ${10}    ${11}    ${12}
    ${object_position_list}    Create List    ${1}    ${2}    ${3}    ${4}    ${5}    ${6}    ${7}    ${8}    ${9}    ${10}    ${11}    ${12}
    ${pair_list}	Create List    ${Pair 1}    ${Pair 2}    ${Pair 3}    ${Pair 4}    ${Pair 5}    ${Pair 6}
    ...    ${Pair 7}    ${Pair 8}    ${Pair 9}    ${Pair 10}    ${Pair 11}    ${Pair 12}
    
    # LC Server -> patched to -> non iPatch DM24 -> cabled to -> non iPatch DM24 -> patched to -> non iPatch DM24 -> cabled (MPO24-12xLC EB) to -> LC Switch
    # 1. Launch and log into SM Web
	# 2. Go to "Site Manager" page
	# 3. Add to Rack 001: 
	# _non iPatch Panel 01 (DM24)
	# _non iPatch Panel 02 (DM24)
	# _non iPatch Panel 03 (DM24)
	# _LC Server 01 (12 ports)
	# _LC Switch 01 (12 ports)
	
    Add Floor    ${SITE_NODE}/${building_name}    ${FLOOR_NAME}
    Add Room    ${SITE_NODE}/${building_name}/${FLOOR_NAME}    ${ROOM_NAME}    
    Add Rack    ${SITE_NODE}/${building_name}/${FLOOR_NAME}/${ROOM_NAME}    ${RACK_NAME}
    Add Systimax Fiber Equipment    ${tree_node_rack}    ${360 G2 Fiber Shelf (1U)}    ${PANEL_NAME_1}    ${Module 1A},${True},${LC 12 Port},${DM24}
    Add Systimax Fiber Equipment    ${tree_node_rack}    ${UD/UHD - 2U}    ${PANEL_NAME_2}    ${Module 1A},${True},${LC 12 Port},${DM24}
    Add Systimax Fiber Equipment    ${tree_node_rack}    ${HD Fiber Shelf (1U)}    ${PANEL_NAME_3}    ${Module 1A},${True},${LC 12 Port},${DM24}
    Add Device In Rack    ${tree_node_rack}    ${SERVER_NAME_1}
    Add Device In Rack Port    ${tree_node_rack}/${SERVER_NAME_1}    ${01}    portType=${LC}    quantity=12    
    Add Network Equipment    ${tree_node_rack}    ${SWITCH_NAME_1}
    Add Network Equipment Port    ${tree_node_rack}/${SWITCH_NAME_1}    ${01}    portType=${LC}    quantity=12       

	# 4. Create patching from:
	# _ LC Server 01-12 -> Panel 01/Module 01/01-12
	# Do not complete job
	# 5. Create cabling from:
	# _ Panel 01/Module 01/MPO 01 - Panel 02/Module 01/MPO 01
	# 6. Create patching from:
	# _ Panel 02/Module 01/01-12 -> Panel 03/Module 01/01-12 Do not complete job
	# 7. Create cabling (MPO24-12xLC EB) from:
	# _ Panel 03/Module 01/MPO 01 -> LC Switch 01-12
	
    Open Patching Window    ${tree_node_server_1}    ${01}
    :FOR    ${i}    IN RANGE    0    len(${port_list})
    \    Create Patching    patchFrom=${tree_node_server_1}    patchTo=${tree_node_panel_1_module_1a}
    ...    portsFrom=${port_list}[${i}]    portsTo=${port_list}[${i}]
    ...    createWo=${True}    clickNext=${False}    
    Save Patching Window    	
    Create Work Order    ${work_order_1}
    Wait For Property On Properties Pane    ${Port Status}    ${In Use - Pending}

    Open Cabling Window    ${tree_node_panel_1_module_1a}    ${01}    
    Create Cabling    typeConnect=Connect    cableFrom=${tree_node_panel_1_module_1a}/${MPO} ${01}   
    ...    mpoTab=${MPO24}    mpoType=${Mpo24_Mpo24}    cableTo=${tree_node_panel_2_module_1a}    portsTo=${MPO} ${01}
    Close Cabling Window 
    Wait For Port Icon Exist On Content Table    ${01}    ${PortFDPanelCabledInUseScheduled}    
    
    Open Patching Window    ${tree_node_panel_2_module_1a}    ${01}
    :FOR    ${i}    IN RANGE    0    len(${port_list})
    \    Create Patching    patchFrom=${tree_node_panel_2_module_1a}    patchTo=${tree_node_panel_3_module_1a}
    ...    portsFrom=${port_list}[${i}]    portsTo=${port_list}[${i}]
    ...    createWo=${True}    clickNext=${False}
    Save Patching Window
    Create Work Order    ${work_order_2}
    Wait For Property On Properties Pane    ${Port Status}    ${In Use - Pending}

    Open Cabling Window    ${tree_node_panel_3_module_1a}    ${01}    
    Create Cabling    typeConnect=Connect    cableFrom=${tree_node_panel_3_module_1a}/${MPO} ${01}   
    ...    mpoTab=${MPO24}    mpoType=${Mpo24_12xLC_EB}    cableTo=${tree_node_switch_1}
    ...    portsTo=${01},${02},${03},${04},${05},${06},${07},${08},${09},${10},${11},${12}
    ...    mpoBranches=${Pair 1},${Pair 2},${Pair 3},${Pair 4},${Pair 5},${Pair 6},${Pair 7},${Pair 8},${Pair 9},${Pair 10},${Pair 11},${Pair 12}   
  
	# 8. Observe port icon and port status on Cabling window
	# VP: _Port icon for Panel 03/Module 01/MPO 01 : cabled and patched scheduled icon
	# VP: _Port icon for LC Switch 01-12: cabled icon
	# VP: _Port status for LC Switch 01-12: Cabled to a Panel port
	       
    Check Icon Object On Cabling Tree    From    ${tree_node_panel_3_module_1a}/${MPO} ${01}    ${PortMPOServiceCabledInUseScheduled}
    :FOR    ${i}    IN RANGE    0    len(${port_list})
    \    Check Icon Object On Cabling Tree    To    ${tree_node_switch_1}/${port_list}[${i}]    ${PortNEFDPanelCabled}
    \    Check Port Status On Cabling Window    To    ${tree_node_switch_1}/${port_list}[${i}]    ${Port Status}: ${In Use - Pending} ${Cabled to a panel port}       

	# 9. Trace Switch 01 / 01 Port in Cabling window and observe
	# VP: The trace circuit show:
	# + Current View:
	# _Service -> connected to -> Switch 01 / 01 -> cabled (MPO24-12xLC EB) to -> Panel 03 / 01
	# + Scheduled View
	# _Service -> connected to -> Switch 01 / 01 -> cabled (MPO24-12xLC EB) to -> Panel 03 / 01 -> patched to -> Panel 02 / 01 -> cabled to -> Panel 01 / 01 -> patched to -> LC Server 01 / 01
	
    Open Trace Window On Cabling    To    ${tree_node_switch_1}/${01}  		    
	Check Total Trace On Site Manager    3    	
	Check Trace Object On Site Manager    indexObject=1    objectPath=${Config}: /${VLAN}: /${Service}:    objectType=${Trace Service Object}
	...    connectionType=${Trace Assign}    informationDevice=${Service}:->${Empty}->${VLAN}:->${Config}:
	Check Trace Object On Site Manager    indexObject=2    objectPosition=${1} [${Pair 1}]    objectPath=${tree_node_switch_1}/${01}    objectType=${Trace Switch}
	...    portIcon=${PortNEFDPanelCabled}    connectionType=${TRACE_LC_12X_MPO24_FRONT_BACK_EB_CABLING}    informationDevice=${01}->${SWITCH_NAME_1}->${device_information_rack}
	Check Trace Object On Site Manager    indexObject=3    objectPosition=${1} (${MPO1})    objectPath=${tree_node_panel_3_module_1a}/${01}    objectType=${Trace DM24}
	...    portIcon=${PortNEFDServiceInUseScheduled}    informationDevice=${01}->${Module 1A} (${DM24})->${PANEL_NAME_3}->${device_information_rack}	
    Select View Type On Trace    ${Scheduled}
    Check Total Trace On Site Manager    6   	
	Check Trace Object On Site Manager    indexObject=1    objectPath=${Config}: /${VLAN}: /${Service}:    objectType=${Trace Service Object}
	...    connectionType=${Trace Assign}    informationDevice=${Service}:->${Empty}->${VLAN}:->${Config}:
	Check Trace Object On Site Manager    indexObject=2    objectPosition=${1} [${Pair 1}]    objectPath=${tree_node_switch_1}/${01}    objectType=${Trace Switch}
	...    portIcon=${PortNEFDPanelCabled}    connectionType=${TRACE_LC_12X_MPO24_FRONT_BACK_EB_CABLING}
	...    informationDevice=${01}->${SWITCH_NAME_1}->${device_information_rack}
	Check Trace Object On Site Manager    indexObject=3    objectPosition=${1} (${MPO1})    objectPath=${tree_node_panel_3_module_1a}/${01}    objectType=${Trace DM24}
	...    portIcon=${PortNEFDServiceInUseScheduled}    connectionType=${TRACE_FIBER_PATCHING}    scheduleIcon=${TRACE_SCHEDULED_IMAGE}
	...    informationDevice=${01}->${Module 1A} (${DM24})->${PANEL_NAME_3}->${device_information_rack}    
    Check Trace Object On Site Manager    indexObject=4    objectPosition=${1} (${MPO1})    objectPath=${tree_node_panel_2_module_1a}/${01}    objectType=${Trace DM24}
	...    portIcon=${PortFDPanelCabledInUseScheduled}    connectionType=${CABLE_MPO24_BACK_BACK_CABLING}
	...    informationDevice=${01}->${Module 1A} (${DM24})->${PANEL_NAME_2}->${device_information_rack}
	Check Trace Object On Site Manager    indexObject=5    objectPosition=${1} (${MPO1})    objectPath=${tree_node_panel_1_module_1a}/${01}    objectType=${Trace DM24}
	...    portIcon=${PortFDPanelCabledInUseScheduled}    connectionType=${TRACE_FIBER_PATCHING}    scheduleIcon=${TRACE_SCHEDULED_IMAGE}
	...    informationDevice=${01}->${Module 1A} (${DM24})->${PANEL_NAME_1}->${device_information_rack}
    Check Trace Object On Site Manager    indexObject=6    objectPosition=${1}    objectPath=${tree_node_server_1}/${01}    objectType=${Trace DM24}
	...    portIcon=${PortFDUncabledInUseScheduled}    informationDevice=${01}->${SERVER_NAME_1}->${device_information_rack}
	Close Trace Window
	Close Cabling Window 
	
	# 10. Close cabling window, observe port icon and status on content pane	  
	# VP: _Status of ports in circuit:
	  # + Panel 03// 01-12: In Use - Pending
	  # + Switch 01// 01-12: In Use - Pending
	# _Icon of ports in circuit:
	  # + Panel 03// 01-12:cabled and patched scheduled icon
	  # + Switch 01// 01-12: cabled scheduled icon
	     
    Wait For Port Icon Exist On Content Table    ${01}    ${PortNEFDServiceInUseScheduled}
    :FOR    ${i}    IN RANGE    0    len(${port_list})
    \    Check Icon Object Exist On Content Pane    ${port_list}[${01}]    ${PortNEFDServiceInUseScheduled}
    \    Check Table Row Map With Header On Content Table    ${Name},${Port Status}    ${port_list}[${i}],${In Use - Pending}    ${i+1}    
    
    Click Tree Node On Site Manager    ${tree_node_switch_1}
    :FOR    ${i}    IN RANGE    0    len(${port_list})
    \    Check Icon Object Exist On Content Pane    ${port_list}[${01}]    ${PortNEFDPanelCabled}
    \    Check Table Row Map With Header On Content Table    ${Name},${Port Status}    ${port_list}[${i}],${In Use - Pending}    ${i+1}       
        
	# 11. Trace Panel 03 / 01-06, Switch 01 / 07-12
	# VP: The trace circuit show in Vertical and Horizontal:
	# + Current View:
	# _Service -> connected to -> Switch 01 / 01-12 -> cabled (MPO24-12xLC EB) to -> Panel 03 / 01-12
	# + Scheduled View
	# _Service -> connected to -> Switch 01 / 01-12 -> cabled (MPO24-12xLC EB) to -> Panel 03 / 01-12 -> patched to -> Panel 02 / 01-12 -> cabled to -> Panel 01 / 01-12 -> patched to -> LC Server 01 / 01-12
	
    :FOR    ${i}    IN RANGE    0    6
    \    Open Trace Window    ${tree_node_panel_3_module_1a}    ${port_list}[${i}] 
	\    Check Total Trace On Site Manager    3    	
	\    Check Trace Object On Site Manager    indexObject=1    objectPath=${Config}: /${VLAN}: /${Service}:    objectType=${Trace Service Object}
	...    connectionType=${Trace Assign}    informationDevice=${Service}:->${Empty}->${VLAN}:->${Config}:
	\    Check Trace Object On Site Manager    indexObject=2    objectPosition=${object_position_list}[${i}] [${pair_list}[${i}]]    objectPath=${tree_node_switch_1}/${port_list}[${i}]    objectType=${Trace Switch}
	...    portIcon=${PortNEFDPanelCabled}    connectionType=${TRACE_LC_12X_MPO24_FRONT_BACK_EB_CABLING}    informationDevice=${port_list}[${i}]->${SWITCH_NAME_1}->${device_information_rack}
	\    Check Trace Object On Site Manager    indexObject=3    objectPosition=${object_position_list}[${i}] (${MPO1})    objectPath=${tree_node_panel_3_module_1a}/${port_list}[${i}]    objectType=${Trace DM24}
	...    portIcon=${PortNEFDServiceInUseScheduled}    informationDevice=${port_list}[${i}]->${Module 1A} (${DM24})->${PANEL_NAME_3}->${device_information_rack}	
    \    Select View Type On Trace    ${Scheduled}
    \    Check Total Trace On Site Manager    6    	
	\    Check Trace Object On Site Manager    indexObject=1    objectPath=${Config}: /${VLAN}: /${Service}:    objectType=${Trace Service Object}
	...    connectionType=${Trace Assign}    informationDevice=${Service}:->${Empty}->${VLAN}:->${Config}:
	\    Check Trace Object On Site Manager    indexObject=2    objectPosition=${object_position_list}[${i}] [${pair_list}[${i}]]    objectPath=${tree_node_switch_1}/${port_list}[${i}]    objectType=${Trace Switch}
	...    portIcon=${PortNEFDPanelCabled}    connectionType=${TRACE_LC_12X_MPO24_FRONT_BACK_EB_CABLING}    informationDevice=${port_list}[${i}]->${SWITCH_NAME_1}->${device_information_rack}
	\    Check Trace Object On Site Manager    indexObject=3    objectPosition=${object_position_list}[${i}] (${MPO1})    objectPath=${tree_node_panel_3_module_1a}/${port_list}[${i}]    objectType=${Trace DM24}
	...    portIcon=${PortNEFDServiceInUseScheduled}    connectionType=${TRACE_FIBER_PATCHING}    scheduleIcon=${TRACE_SCHEDULED_IMAGE}
	...    informationDevice=${port_list}[${i}]->${Module 1A} (${DM24})->${PANEL_NAME_3}->${device_information_rack}    
    \    Check Trace Object On Site Manager    indexObject=4    objectPosition=${object_position_list}[${i}] (${MPO1})    objectPath=${tree_node_panel_2_module_1a}/${port_list}[${i}]    objectType=${Trace DM24}
	...    portIcon=${PortFDPanelCabledInUseScheduled}    connectionType=${CABLE_MPO24_BACK_BACK_CABLING}
	...    informationDevice=${port_list}[${i}]->${Module 1A} (${DM24})->${PANEL_NAME_2}->${device_information_rack}
	\    Check Trace Object On Site Manager    indexObject=5    objectPosition=${object_position_list}[${i}] (${MPO1})    objectPath=${tree_node_panel_1_module_1a}/${port_list}[${i}]    objectType=${Trace DM24}
	...    portIcon=${PortFDPanelCabledInUseScheduled}    connectionType=${TRACE_FIBER_PATCHING}    scheduleIcon=${TRACE_SCHEDULED_IMAGE}
	...    informationDevice=${port_list}[${i}]->${Module 1A} (${DM24})->${PANEL_NAME_1}->${device_information_rack}
    \    Check Trace Object On Site Manager    indexObject=6    objectPosition=${object_position_list}[${i}]    objectPath=${tree_node_server_1}/${port_list}[${i}]    objectType=${Trace Server Fiber}
	...    portIcon=${PortFDUncabledInUseScheduled}    informationDevice=${port_list}[${i}]->${SERVER_NAME_1}->${device_information_rack}
	\    Switch View Mode On Trace    viewType=${Vertical}       	
	\    Check Vertical Trace Object    indexObject=1    objectPosition=${EMPTY}    objectName=${EMPTY}    objectType=${Trace Service Object}    objectDetails=${Config}:
	...    objectLocation=${EMPTY}    connectionType=${Trace Assign}
    \    Check Vertical Trace Object    indexObject=2    objectPosition=${object_position_list}[${i}] [${pair_list}[${i}]]    objectName=${port_list}[${i}]    objectType=${Trace Switch}    
    ...    objectDetails=${POSITION_RACK_NAME}/${SWITCH_NAME_1}     objectLocation=${tree_node_room}
    ...    portIcon=${PortNEFDPanelCabled}    connectionType=${TRACE_LC_12X_MPO24_FRONT_BACK_EB_CABLING}    
	\    Check Vertical Trace Object    indexObject=3    objectPosition=${object_position_list}[${i}] (${MPO1})    objectName=${port_list}[${i}]    objectType=${Trace DM24}    
    ...    objectDetails=${POSITION_RACK_NAME}/${PANEL_NAME_3}/${Module 1A} (${DM24})     objectLocation=${tree_node_room}
    ...    portIcon=${PortNEFDServiceInUseScheduled}    connectionType=${TRACE_FIBER_PATCHING}    scheduleIcon=${TRACE_SCHEDULED_IMAGE}	
    \    Check Vertical Trace Object    indexObject=4    objectPosition=${object_position_list}[${i}] (${MPO1})    objectName=${port_list}[${i}]    objectType=${Trace DM24}    
    ...    objectDetails=${POSITION_RACK_NAME}/${PANEL_NAME_2}/${Module 1A} (${DM24})     objectLocation=${tree_node_room}
    ...    portIcon=${PortFDPanelCabledInUseScheduled}    connectionType=${CABLE_MPO24_BACK_BACK_CABLING}	
    \    Check Vertical Trace Object    indexObject=5    objectPosition=${object_position_list}[${i}] (${MPO1})    objectName=${port_list}[${i}]    objectType=${Trace DM24}    
    ...    objectDetails=${POSITION_RACK_NAME}/${PANEL_NAME_1}/${Module 1A} (${DM24})     objectLocation=${tree_node_room}
    ...    portIcon=${PortFDPanelCabledInUseScheduled}    connectionType=${TRACE_FIBER_PATCHING}    scheduleIcon=${TRACE_SCHEDULED_IMAGE}
    \    Check Vertical Trace Object    indexObject=6    objectPosition=${object_position_list}[${i}]    objectName=${port_list}[${i}]    objectType=${Trace Server Fiber}    
    ...    objectDetails=${POSITION_RACK_NAME}/${SERVER_NAME_1}     objectLocation=${tree_node_room}
    ...    portIcon=${PortFDUncabledInUseScheduled}    
	\    Select View Type On Trace    ${Current}
    \    Check Vertical Trace Object    indexObject=1    objectPosition=${EMPTY}    objectName=${EMPTY}    objectType=${Trace Service Object}    objectDetails=${Config}:
	...    objectLocation=${EMPTY}    connectionType=${Trace Assign}
    \    Check Vertical Trace Object    indexObject=2    objectPosition=${object_position_list}[${i}] [${pair_list}[${i}]]    objectName=${port_list}[${i}]    objectType=${Trace Switch}    
    ...    objectDetails=${POSITION_RACK_NAME}/${SWITCH_NAME_1}     objectLocation=${tree_node_room}
    ...    portIcon=${PortNEFDPanelCabled}    connectionType=${TRACE_LC_12X_MPO24_FRONT_BACK_EB_CABLING}    
	\    Check Vertical Trace Object    indexObject=3    objectPosition=${object_position_list}[${i}] (${MPO1})    objectName=${port_list}[${i}]    objectType=${Trace DM24}    
    ...    objectDetails=${POSITION_RACK_NAME}/${PANEL_NAME_3}/${Module 1A} (${DM24})     objectLocation=${tree_node_room}
    ...    portIcon=${PortNEFDServiceInUseScheduled}
    \    Switch View Mode On Trace    viewType=${Horizontal}        	
	\    Close Trace Window 
        
    :FOR    ${i}    IN RANGE    6    12
    \    Open Trace Window    ${tree_node_switch_1}    ${port_list}[${i}]
	\    Check Total Trace On Site Manager    3    	
	\    Check Trace Object On Site Manager    indexObject=1    objectPath=${Config}: /${VLAN}: /${Service}:    objectType=${Trace Service Object}
	...    connectionType=${Trace Assign}    informationDevice=${Service}:->${Empty}->${VLAN}:->${Config}:
	\    Check Trace Object On Site Manager    indexObject=2    objectPosition=${object_position_list}[${i}] [${pair_list}[${i}]]    objectPath=${tree_node_switch_1}/${port_list}[${i}]    objectType=${Trace Switch}
	...    portIcon=${PortNEFDPanelCabled}    connectionType=${TRACE_LC_12X_MPO24_FRONT_BACK_EB_CABLING}    informationDevice=${port_list}[${i}]->${SWITCH_NAME_1}->${device_information_rack}
	\    Check Trace Object On Site Manager    indexObject=3    objectPosition=${object_position_list}[${i}] (${MPO1})    objectPath=${tree_node_panel_3_module_1a}/${port_list}[${i}]    objectType=${Trace DM24}
	...    portIcon=${PortNEFDServiceInUseScheduled}    informationDevice=${port_list}[${i}]->${Module 1A} (${DM24})->${PANEL_NAME_3}->${device_information_rack}	
    \    Select View Type On Trace    ${Scheduled}
    \    Check Total Trace On Site Manager    6    	
	\    Check Trace Object On Site Manager    indexObject=1    objectPath=${Config}: /${VLAN}: /${Service}:    objectType=${Trace Service Object}
	...    connectionType=${Trace Assign}    informationDevice=${Service}:->${Empty}->${VLAN}:->${Config}:
	\    Check Trace Object On Site Manager    indexObject=2    objectPosition=${object_position_list}[${i}] [${pair_list}[${i}]]    objectPath=${tree_node_switch_1}/${port_list}[${i}]    objectType=${Trace Switch}
	...    portIcon=${PortNEFDPanelCabled}    connectionType=${TRACE_LC_12X_MPO24_FRONT_BACK_EB_CABLING}    informationDevice=${port_list}[${i}]->${SWITCH_NAME_1}->${device_information_rack}
	\    Check Trace Object On Site Manager    indexObject=3    objectPosition=${object_position_list}[${i}] (${MPO1})    objectPath=${tree_node_panel_3_module_1a}/${port_list}[${i}]    objectType=${Trace DM24}
	...    portIcon=${PortNEFDServiceInUseScheduled}    connectionType=${TRACE_FIBER_PATCHING}    scheduleIcon=${TRACE_SCHEDULED_IMAGE}
	...    informationDevice=${port_list}[${i}]->${Module 1A} (${DM24})->${PANEL_NAME_3}->${device_information_rack}    
    \    Check Trace Object On Site Manager    indexObject=4    objectPosition=${object_position_list}[${i}] (${MPO1})    objectPath=${tree_node_panel_2_module_1a}/${port_list}[${i}]    objectType=${Trace DM24}
	...    portIcon=${PortFDPanelCabledInUseScheduled}    connectionType=${CABLE_MPO24_BACK_BACK_CABLING}
	...    informationDevice=${port_list}[${i}]->${Module 1A} (${DM24})->${PANEL_NAME_2}->${device_information_rack}
	\    Check Trace Object On Site Manager    indexObject=5    objectPosition=${object_position_list}[${i}] (${MPO1})    objectPath=${tree_node_panel_1_module_1a}/${port_list}[${i}]    objectType=${Trace DM24}
	...    portIcon=${PortFDPanelCabledInUseScheduled}    connectionType=${TRACE_FIBER_PATCHING}    scheduleIcon=${TRACE_SCHEDULED_IMAGE}
	...    informationDevice=${port_list}[${i}]->${Module 1A} (${DM24})->${PANEL_NAME_1}->${device_information_rack}
    \    Check Trace Object On Site Manager    indexObject=6    objectPosition=${object_position_list}[${i}]    objectPath=${tree_node_server_1}/${port_list}[${i}]    objectType=${Trace Server Fiber}
	...    portIcon=${PortFDUncabledInUseScheduled}    informationDevice=${port_list}[${i}]->${SERVER_NAME_1}->${device_information_rack}
	\    Switch View Mode On Trace    viewType=${Vertical}
    \    Check Vertical Trace Object    indexObject=1    objectPosition=${EMPTY}    objectName=${EMPTY}    objectType=${Trace Service Object}    objectDetails=${Config}:
	...    objectLocation=${EMPTY}    connectionType=${Trace Assign}
    \    Check Vertical Trace Object    indexObject=2    objectPosition=${object_position_list}[${i}] [${pair_list}[${i}]]    objectName=${port_list}[${i}]    objectType=${Trace Switch}    
    ...    objectDetails=${POSITION_RACK_NAME}/${SWITCH_NAME_1}     objectLocation=${tree_node_room}
    ...    portIcon=${PortNEFDPanelCabled}    connectionType=${TRACE_LC_12X_MPO24_FRONT_BACK_EB_CABLING}    
	\    Check Vertical Trace Object    indexObject=3    objectPosition=${object_position_list}[${i}] (${MPO1})    objectName=${port_list}[${i}]    objectType=${Trace DM24}    
    ...    objectDetails=${POSITION_RACK_NAME}/${PANEL_NAME_3}/${Module 1A} (${DM24})     objectLocation=${tree_node_room}
    ...    portIcon=${PortNEFDServiceInUseScheduled}    connectionType=${TRACE_FIBER_PATCHING}    scheduleIcon=${TRACE_SCHEDULED_IMAGE}	
    \    Check Vertical Trace Object    indexObject=4    objectPosition=${object_position_list}[${i}] (${MPO1})    objectName=${port_list}[${i}]    objectType=${Trace DM24}    
    ...    objectDetails=${POSITION_RACK_NAME}/${PANEL_NAME_2}/${Module 1A} (${DM24})     objectLocation=${tree_node_room}
    ...    portIcon=${PortFDPanelCabledInUseScheduled}    connectionType=${CABLE_MPO24_BACK_BACK_CABLING}	
    \    Check Vertical Trace Object    indexObject=5    objectPosition=${object_position_list}[${i}] (${MPO1})    objectName=${port_list}[${i}]    objectType=${Trace DM24}    
    ...    objectDetails=${POSITION_RACK_NAME}/${PANEL_NAME_1}/${Module 1A} (${DM24})     objectLocation=${tree_node_room}
    ...    portIcon=${PortFDPanelCabledInUseScheduled}    connectionType=${TRACE_FIBER_PATCHING}    scheduleIcon=${TRACE_SCHEDULED_IMAGE}
    \    Check Vertical Trace Object    indexObject=6    objectPosition=${object_position_list}[${i}]    objectName=${port_list}[${i}]    objectType=${Trace Server Fiber}    
    ...    objectDetails=${POSITION_RACK_NAME}/${SERVER_NAME_1}     objectLocation=${tree_node_room}
    ...    portIcon=${PortFDUncabledInUseScheduled}
	\    Select View Type On Trace    ${Current}
    \    Check Vertical Trace Object    indexObject=1    objectPosition=${EMPTY}    objectName=${EMPTY}    objectType=${Trace Service Object}    objectDetails=${Config}:
	...    objectLocation=${EMPTY}    connectionType=${Trace Assign}
    \    Check Vertical Trace Object    indexObject=2    objectPosition=${object_position_list}[${i}] [${pair_list}[${i}]]    objectName=${port_list}[${i}]    objectType=${Trace Switch}    
    ...    objectDetails=${POSITION_RACK_NAME}/${SWITCH_NAME_1}     objectLocation=${tree_node_room}
    ...    portIcon=${PortNEFDPanelCabled}    connectionType=${TRACE_LC_12X_MPO24_FRONT_BACK_EB_CABLING}    
	\    Check Vertical Trace Object    indexObject=3    objectPosition=${object_position_list}[${i}] (${MPO1})    objectName=${port_list}[${i}]    objectType=${Trace DM24}    
    ...    objectDetails=${POSITION_RACK_NAME}/${PANEL_NAME_3}/${Module 1A} (${DM24})     objectLocation=${tree_node_room}
    ...    portIcon=${PortNEFDServiceInUseScheduled}
    \    Switch View Mode On Trace    viewType=${Horizontal}        	
	\    Close Trace Window 	
	
	# 12. Open Patching window and observe the Curcuit Trace of Panel 03 / 01-12
	# _Service -> connected to -> Switch 01 / 01-12 -> cabled (MPO24-12xLC EB) to -> Panel 03 / 01-12 -> patched to -> Panel 02 / 01-12 -> cabled to -> Panel 01 / 01-12 -> patched to -> LC Server 01 / 01-12
	Open Patching Window    ${tree_node_panel_3_module_1a}    ${01}
	Check Trace Object On Patching    indexObject=1    objectType=${Trace Service Object}
	...    connectionType=${Trace Assign}    informationDevice=${Service}:->${Empty}->${VLAN}:->${Config}:
	Check Trace Object On Patching    indexObject=2    objectPosition=${1} [${Pair 1}]    objectType=${Trace Switch}
	...    portIcon=${PortNEFDPanelCabled}    connectionType=${TRACE_LC_12X_MPO24_FRONT_BACK_EB_CABLING}    informationDevice=${01}->${SWITCH_NAME_1}->${device_information_rack}
	Check Trace Object On Patching    indexObject=3    objectPosition=${1} (${MPO1})    objectType=${Trace DM24}
	...    portIcon=${PortNEFDServiceInUseScheduled}    connectionType=${TRACE_FIBER_PATCHING}    scheduleIcon=${TRACE_SCHEDULED_IMAGE}
	...    informationDevice=${01}->${Module 1A} (${DM24})->${PANEL_NAME_3}->${device_information_rack}    
    Check Trace Object On Patching    indexObject=4    objectPosition=${1} (${MPO1})    objectType=${Trace DM24}
	...    portIcon=${PortFDPanelCabledInUseScheduled}    connectionType=${CABLE_MPO24_BACK_BACK_CABLING}
	...    informationDevice=${01}->${Module 1A} (${DM24})->${PANEL_NAME_2}->${device_information_rack}
	Check Trace Object On Patching    indexObject=5    objectPosition=${1} (${MPO1})    objectType=${Trace DM24}
	...    portIcon=${PortFDPanelCabledInUseScheduled}    connectionType=${TRACE_FIBER_PATCHING}    scheduleIcon=${TRACE_SCHEDULED_IMAGE}
	...    informationDevice=${01}->${Module 1A} (${DM24})->${PANEL_NAME_1}->${device_information_rack}
    Check Trace Object On Patching    indexObject=6    objectPosition=${1}    objectType=${Trace Server Fiber}
	...    portIcon=${PortFDUncabledInUseScheduled}    informationDevice=${01}->${SERVER_NAME_1}->${device_information_rack}
	Close Patching Window
	
	# # 13. Complete all WO, observe port icon status
	
	Open Work Order Queue Window    
	Complete Work Order    ${work_order_1},${work_order_2}
	Close Work Order Queue	
	Wait For Property On Properties Pane    ${Port Status}    ${In Use}
		 
	# _Status of ports in circuit:
	  # + Panel 03/ 01-12: In Use
	  # + Switch 01/ 01-12: In Use
	# _Icon of ports in circuit:
	  # + Panel 03/ 01-12:cabled and patched icon
	  # + Switch 01/ 01-12: cabled icon
	  
	Click Tree Node On Site Manager    ${tree_node_panel_3_module_1a}
    :FOR    ${i}    IN RANGE    0    len(${port_list})
    \    Check Icon Object Exist On Content Pane    ${port_list}[${i}]    ${PortNEFDServiceInUse}
    \    Check Table Row Map With Header On Content Table    ${Name},${Port Status}    ${port_list}[${i}],${In Use}    ${i+1}    
    
    Click Tree Node On Site Manager    ${tree_node_switch_1}
    :FOR    ${i}    IN RANGE    0    len(${port_list})
    \    Check Icon Object Exist On Content Pane    ${port_list}[${i}]    ${PortNEFDPanelCabled}
    \    Check Table Row Map With Header On Content Table    ${Name},${Port Status}    ${port_list}[${i}],${In Use}    ${i+1}
  
	# 14. Trace Panel 03 / 01-06, Switch 01 / 07-12
	# VP: The trace circuit show in Vertical and Horizontal
	# _Service -> connected to -> Switch 01 / 01-12 -> cabled (MPO24-12xLC EB) to -> Panel 03 / 01-12 -> patched to -> Panel 02 / 01-12 -> cabled to -> Panel 01 / 01-12 -> patched to -> LC Server 01 / 01-12
    	
    :FOR    ${i}    IN RANGE    0    6
    \    Open Trace Window    ${tree_node_panel_3_module_1a}    ${port_list}[${i}]	    
    \    Check Total Trace On Site Manager    6    	
	\    Check Trace Object On Site Manager    indexObject=1    objectPath=${Config}: /${VLAN}: /${Service}:    objectType=${Trace Service Object}
	...    connectionType=${Trace Assign}    informationDevice=${Service}:->${Empty}->${VLAN}:->${Config}:
	\    Check Trace Object On Site Manager    indexObject=2    objectPosition=${object_position_list}[${i}] [${pair_list}[${i}]]    objectPath=${tree_node_switch_1}/${port_list}[${i}]    objectType=${Trace Switch}
	...    portIcon=${PortNEFDPanelCabled}    connectionType=${TRACE_LC_12X_MPO24_FRONT_BACK_EB_CABLING}    informationDevice=${port_list}[${i}]->${SWITCH_NAME_1}->${device_information_rack}
	\    Check Trace Object On Site Manager    indexObject=3    objectPosition=${object_position_list}[${i}] (${MPO1})    objectPath=${tree_node_panel_3_module_1a}/${port_list}[${i}]    objectType=${Trace DM24}
	...    portIcon=${PortNEFDServiceInUse}    connectionType=${TRACE_FIBER_PATCHING}
	...    informationDevice=${port_list}[${i}]->${Module 1A} (${DM24})->${PANEL_NAME_3}->${device_information_rack}    
    \    Check Trace Object On Site Manager    indexObject=4    objectPosition=${object_position_list}[${i}] (${MPO1})    objectPath=${tree_node_panel_2_module_1a}/${port_list}[${i}]    objectType=${Trace DM24}
	...    portIcon=${PortFDPanelCabledInUse}    connectionType=${CABLE_MPO24_BACK_BACK_CABLING}
	...    informationDevice=${port_list}[${i}]->${Module 1A} (${DM24})->${PANEL_NAME_2}->${device_information_rack}
	\    Check Trace Object On Site Manager    indexObject=5    objectPosition=${object_position_list}[${i}] (${MPO1})    objectPath=${tree_node_panel_1_module_1a}/${port_list}[${i}]    objectType=${Trace DM24}
	...    portIcon=${PortFDPanelCabledInUse}    connectionType=${TRACE_FIBER_PATCHING}
	...    informationDevice=${port_list}[${i}]->${Module 1A} (${DM24})->${PANEL_NAME_1}->${device_information_rack}
    \    Check Trace Object On Site Manager    indexObject=6    objectPosition=${object_position_list}[${i}]    objectPath=${tree_node_server_1}/${port_list}[${i}]    objectType=${Trace Server Fiber}
	...    portIcon=${PortFDUncabledInUse}    informationDevice=${port_list}[${i}]->${SERVER_NAME_1}->${device_information_rack}
	\    Switch View Mode On Trace    viewType=${Vertical}
	\    Check Vertical Trace Object    indexObject=1    objectPosition=${EMPTY}    objectName=${EMPTY}    objectType=${Trace Service Object}    objectDetails=${Config}:
	...    objectLocation=${EMPTY}    connectionType=${Trace Assign}
    \    Check Vertical Trace Object    indexObject=2    objectPosition=${object_position_list}[${i}] [${pair_list}[${i}]]    objectName=${port_list}[${i}]    objectType=${Trace Switch}    
    ...    objectDetails=${POSITION_RACK_NAME}/${SWITCH_NAME_1}     objectLocation=${tree_node_room}
    ...    portIcon=${PortNEFDPanelCabled}    connectionType=${TRACE_LC_12X_MPO24_FRONT_BACK_EB_CABLING}    
	\    Check Vertical Trace Object    indexObject=3    objectPosition=${object_position_list}[${i}] (${MPO1})    objectName=${port_list}[${i}]    objectType=${Trace DM24}    
    ...    objectDetails=${POSITION_RACK_NAME}/${PANEL_NAME_3}/${Module 1A} (${DM24})     objectLocation=${tree_node_room}
    ...    portIcon=${PortNEFDServiceInUse}    connectionType=${TRACE_FIBER_PATCHING}	
    \    Check Vertical Trace Object    indexObject=4    objectPosition=${object_position_list}[${i}] (${MPO1})    objectName=${port_list}[${i}]    objectType=${Trace DM24}    
    ...    objectDetails=${POSITION_RACK_NAME}/${PANEL_NAME_2}/${Module 1A} (${DM24})     objectLocation=${tree_node_room}
    ...    portIcon=${PortFDPanelCabledInUse}    connectionType=${CABLE_MPO24_BACK_BACK_CABLING}	
    \    Check Vertical Trace Object    indexObject=5    objectPosition=${object_position_list}[${i}] (${MPO1})    objectName=${port_list}[${i}]    objectType=${Trace DM24}    
    ...    objectDetails=${POSITION_RACK_NAME}/${PANEL_NAME_1}/${Module 1A} (${DM24})     objectLocation=${tree_node_room}
    ...    portIcon=${PortFDPanelCabledInUse}    connectionType=${TRACE_FIBER_PATCHING}
    \    Check Vertical Trace Object    indexObject=6    objectPosition=${object_position_list}[${i}]    objectName=${port_list}[${i}]    objectType=${Trace Server Fiber}    
    ...    objectDetails=${POSITION_RACK_NAME}/${SERVER_NAME_1}     objectLocation=${tree_node_room}
    ...    portIcon=${PortFDUncabledInUse}
    \    Switch View Mode On Trace    viewType=${Horizontal}
	\    Close Trace Window
	        
    :FOR    ${i}    IN RANGE    6    12
    \    Open Trace Window    ${tree_node_switch_1}    ${port_list}[${i}]	    
    \    Check Total Trace On Site Manager    6    	
	\    Check Trace Object On Site Manager    indexObject=1    objectPath=${Config}: /${VLAN}: /${Service}:    objectType=${Trace Service Object}
	...    connectionType=${Trace Assign}    informationDevice=${Service}:->${Empty}->${VLAN}:->${Config}:
	\    Check Trace Object On Site Manager    indexObject=2    objectPosition=${object_position_list}[${i}] [${pair_list}[${i}]]    objectPath=${tree_node_switch_1}/${port_list}[${i}]    objectType=${Trace Switch}
	...    portIcon=${PortNEFDPanelCabled}    connectionType=${TRACE_LC_12X_MPO24_FRONT_BACK_EB_CABLING}    informationDevice=${port_list}[${i}]->${SWITCH_NAME_1}->${device_information_rack}
	\    Check Trace Object On Site Manager    indexObject=3    objectPosition=${object_position_list}[${i}] (${MPO1})    objectPath=${tree_node_panel_3_module_1a}/${port_list}[${i}]    objectType=${Trace DM24}
	...    portIcon=${PortNEFDServiceInUse}    connectionType=${TRACE_FIBER_PATCHING}
	...    informationDevice=${port_list}[${i}]->${Module 1A} (${DM24})->${PANEL_NAME_3}->${device_information_rack}    
    \    Check Trace Object On Site Manager    indexObject=4    objectPosition=${object_position_list}[${i}] (${MPO1})    objectPath=${tree_node_panel_2_module_1a}/${port_list}[${i}]    objectType=${Trace DM24}
	...    portIcon=${PortFDPanelCabledInUse}    connectionType=${CABLE_MPO24_BACK_BACK_CABLING}
	...    informationDevice=${port_list}[${i}]->${Module 1A} (${DM24})->${PANEL_NAME_2}->${device_information_rack}
	\    Check Trace Object On Site Manager    indexObject=5    objectPosition=${object_position_list}[${i}] (${MPO1})    objectPath=${tree_node_panel_1_module_1a}/${port_list}[${i}]    objectType=${Trace DM24}
	...    portIcon=${PortFDPanelCabledInUse}    connectionType=${TRACE_FIBER_PATCHING}
	...    informationDevice=${port_list}[${i}]->${Module 1A} (${DM24})->${PANEL_NAME_1}->${device_information_rack}
    \    Check Trace Object On Site Manager    indexObject=6    objectPosition=${object_position_list}[${i}]    objectPath=${tree_node_server_1}/${port_list}[${i}]    objectType=${Trace Server Fiber}
	...    portIcon=${PortFDUncabledInUse}    informationDevice=${port_list}[${i}]->${SERVER_NAME_1}->${device_information_rack}	
	\    Switch View Mode On Trace    viewType=${Vertical}
	\    Check Vertical Trace Object    indexObject=1    objectPosition=${EMPTY}    objectName=${EMPTY}    objectType=${Trace Service Object}    objectDetails=${Config}:
	...    objectLocation=${EMPTY}    connectionType=${Trace Assign}
    \    Check Vertical Trace Object    indexObject=2    objectPosition=${object_position_list}[${i}] [${pair_list}[${i}]]    objectName=${port_list}[${i}]    objectType=${Trace Switch}    
    ...    objectDetails=${POSITION_RACK_NAME}/${SWITCH_NAME_1}     objectLocation=${tree_node_room}
    ...    portIcon=${PortNEFDPanelCabled}    connectionType=${TRACE_LC_12X_MPO24_FRONT_BACK_EB_CABLING}    
	\    Check Vertical Trace Object    indexObject=3    objectPosition=${object_position_list}[${i}] (${MPO1})    objectName=${port_list}[${i}]    objectType=${Trace DM24}    
    ...    objectDetails=${POSITION_RACK_NAME}/${PANEL_NAME_3}/${Module 1A} (${DM24})     objectLocation=${tree_node_room}
    ...    portIcon=${PortNEFDServiceInUse}    connectionType=${TRACE_FIBER_PATCHING}	
    \    Check Vertical Trace Object    indexObject=4    objectPosition=${object_position_list}[${i}] (${MPO1})    objectName=${port_list}[${i}]    objectType=${Trace DM24}    
    ...    objectDetails=${POSITION_RACK_NAME}/${PANEL_NAME_2}/${Module 1A} (${DM24})     objectLocation=${tree_node_room}
    ...    portIcon=${PortFDPanelCabledInUse}    connectionType=${CABLE_MPO24_BACK_BACK_CABLING}	
    \    Check Vertical Trace Object    indexObject=5    objectPosition=${object_position_list}[${i}] (${MPO1})    objectName=${port_list}[${i}]    objectType=${Trace DM24}    
    ...    objectDetails=${POSITION_RACK_NAME}/${PANEL_NAME_1}/${Module 1A} (${DM24})     objectLocation=${tree_node_room}
    ...    portIcon=${PortFDPanelCabledInUse}    connectionType=${TRACE_FIBER_PATCHING}
    \    Check Vertical Trace Object    indexObject=6    objectPosition=${object_position_list}[${i}]    objectName=${port_list}[${i}]    objectType=${Trace Server Fiber}    
    ...    objectDetails=${POSITION_RACK_NAME}/${SERVER_NAME_1}     objectLocation=${tree_node_room}
    ...    portIcon=${PortFDUncabledInUse}
    \    Switch View Mode On Trace    viewType=${Horizontal}
	\    Close Trace Window

	# 15. Right click on Panel 03 and select Remove Cabling
	# 16. Observe port icon and status on content pane of Panel 03 & Switch 01
	# _Status of ports in circuit:
	  # + Panel 03/ 01-12: In Use
	  # + Switch 01/ 01-12: Available
	# _Icon of ports in circuit:
	  # + Panel 03/ 01-12: uncabled and patched icon
	  # + Switch 01/ 01-12: availabel icon
	
	Remove Cabling By Context Menu On Site Tree    ${tree_node_panel_3_module_1a}
    Wait For Port Icon Exist On Content Table    ${01}    ${PortFDUncabledInUse}
    :FOR    ${i}    IN RANGE    0    len(${port_list})
    \    Check Icon Object Exist On Content Pane    ${port_list}[${i}]    ${PortFDUncabledInUse}
    \    Check Table Row Map With Header On Content Table    ${Name},${Port Status}    ${port_list}[${i}],${In Use}    ${i+1}    
    
    Click Tree Node On Site Manager    ${tree_node_switch_1}
    :FOR    ${i}    IN RANGE    0    len(${port_list})
    \    Check Icon Object Exist On Content Pane    ${port_list}[${i}]    ${PortFDUncabledAvailable}
    \    Check Table Row Map With Header On Content Table    ${Name},${Port Status}    ${port_list}[${i}],${Available}    ${i+1} 
	
	# 17. Trace Panel 03 / 01 and open Trace
	# Panel 03 / 01 -> patched to -> Panel 02 / 01 -> cabled to -> Panel 01 / 01 -> patched to -> LC Server 01 / 01
	
	Open Trace Window    ${tree_node_panel_3_module_1a}    ${01}
    Check Total Trace On Site Manager    4  	
    Check Trace Object On Site Manager    indexObject=1    objectPosition=${1}    objectPath=${tree_node_panel_3_module_1a}/${01}    objectType=${Trace DM24}
	...    portIcon=${PortFDUncabledInUse}    connectionType=${TRACE_FIBER_PATCHING}
	...    informationDevice=${01}->${Module 1A} (${DM24})->${PANEL_NAME_3}->${device_information_rack}    
    Check Trace Object On Site Manager    indexObject=2    objectPosition=${1} (${MPO1})    objectPath=${tree_node_panel_2_module_1a}/${01}    objectType=${Trace DM24}
	...    portIcon=${PortFDPanelCabledInUse}    connectionType=${CABLE_MPO24_BACK_BACK_CABLING}
	...    informationDevice=${01}->${Module 1A} (${DM24})->${PANEL_NAME_2}->${device_information_rack}
	Check Trace Object On Site Manager    indexObject=3    objectPosition=${1} (${MPO1})    objectPath=${tree_node_panel_1_module_1a}/${01}    objectType=${Trace DM24}
	...    portIcon=${PortFDPanelCabledInUse}    connectionType=${TRACE_FIBER_PATCHING}
	...    informationDevice=${01}->${Module 1A} (${DM24})->${PANEL_NAME_1}->${device_information_rack}
    Check Trace Object On Site Manager    indexObject=4    objectPosition=${1}    objectPath=${tree_node_server_1}/${01}    objectType=${Trace Server Fiber}
	...    portIcon=${PortFDUncabledInUse}    informationDevice=${01}->${SERVER_NAME_1}->${device_information_rack}
	Close Trace Window
			
	# 18. Trace Switch 01 / 01 and open Trace
	# _Service -> connected to -> Switch 01 / 01
	
	Open Trace Window    ${tree_node_switch_1}    ${01} 
    Check Total Trace On Site Manager    2    	
	Check Trace Object On Site Manager    indexObject=1    objectPath=${Config}: /${VLAN}: /${Service}:    objectType=${Trace Service Object}
	...    connectionType=${Trace Assign}    informationDevice=${Service}:->${Empty}->${VLAN}:->${Config}:
	Check Trace Object On Site Manager    indexObject=2    objectPosition=${1}    objectPath=${tree_node_switch_1}/${01}    objectType=${Trace Switch}
	...    portIcon=${PortFDUncabledAvailable}    informationDevice=${01}->${SWITCH_NAME_1}->${device_information_rack}
	Close Trace Window
