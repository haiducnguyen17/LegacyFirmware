*** Settings ***
Resource    ../../../../../resources/constants.robot
Resource    ../../../../../resources/icons_constants.robot

Library   ../../../../../py_sources/logigear/setup.py  
Library   ../../../../../py_sources/logigear/api/BuildingApi.py    ${USERNAME}    ${PASSWORD}
Library   ../../../../../py_sources/logigear/page_objects/system_manager/site_manager/SiteManagerPage${PLATFORM}.py        
Library   ../../../../../py_sources/logigear/page_objects/system_manager/login/LoginPage${PLATFORM}.py
Library   ../../../../../py_sources/logigear/page_objects/system_manager/site_manager/snmp/SNMPPage${PLATFORM}.py
Library   ../../../../../py_sources/logigear/page_objects/system_manager/site_manager/connections/cabling/CablingPage${PLATFORM}.py       
Library   ../../../../../py_sources/logigear/page_objects/system_manager/site_manager/connections/patching/PatchingPage${PLATFORM}.py   
Library   ../../../../../py_sources/logigear/page_objects/system_manager/site_manager/work_orders/create_work_order/CreateWorkOrderPage${PLATFORM}.py
Library   ../../../../../py_sources/logigear/page_objects/system_manager/site_manager/work_orders/work_order_queue/WorkOrderQueuePage${PLATFORM}.py     
Library    ../../../../../py_sources/logigear/page_objects/system_manager/site_manager/connections/front_to_back_cabling/FrontToBackCablingPageDesktop.py    

Default Tags    Connectivity
Force Tags    SM-30335_SM-30337

*** Variables ***
${FLOOR_NAME}    Floor 01
${ROOM_NAME}    Room 01
${RACK_NAME}    Rack 001
${SWITCH_NAME}    Switch 01
${SERVER_NAME}    Server 01
${PANEL_NAME_1}    DM24 Panel 01
${PANEL_NAME_2}    DM24 Panel 02
${PANEL_NAME_3}    DM24 Panel 03
${PANEL_NAME_4}    LC Panel
${POSITION_RACK_NAME}    1:1 Rack 001
${IP_ADD}    10.50.2.19
${ORIGIN_HARNESS_SYNC_LOCA}    ${DIR_PRECONDITION_SNMP}\\Original\\Blending\\TM_BLE_219\\SyncSwitch 10.50.2.19.txt
${ORIGIN_HARNESS_DD_LOCA}    ${DIR_PRECONDITION_SNMP}\\Original\\Blending\\TM_BLE_219\\DiscDevicesSwitch 10.50.2.19.txt

*** Test Cases ***
SM-30337_SM-30335_06-Verify that the service is propagated and trace correctly in new customer circuit (DGT): LC Server -> patched to -> non iPatch DM24 -> cabled to -> non iPatch DM24 -> patched to -> non iPatch DM24 -> cabled (MPO24-12xLC EB) to -> LC MNE
    [Setup]    Run Keywords    Set Test Variable    ${building_name}    BD_SM-30337_SM-30335_06
    ...    AND    Delete Building    name=${building_name}
    ...    AND    Open SM Login Page
    ...    AND    Login To SM    ${USERNAME}    ${PASSWORD}
    
    [Teardown]    Run Keywords    Close Browser
    ...    AND    Delete Building     ${building_name}

    Set Tags    SM-30335    SM-30337    
    
    ${tree_node_rack}    Set Variable    ${SITE_NODE}/${building_name}/${FLOOR_NAME}/${ROOM_NAME}/${POSITION_RACK_NAME}
    ${tc_wo_name}    Set Variable    WO_30337_30335_06_01
    ${tc_wo_name_2}    Set Variable    WO_30337_30335_06_02
    ${port_list}    Create List    ${01}    ${02}    ${03}    ${04}    ${05}    ${06}    ${07}    ${08}    ${09}    ${10}    ${11}    ${12}
    ${vlan_list}	Set Variable    111  
    ${today}    Get Current Date Time    resultFormat=%Y-%m-%d
    Copy File    ${ORIGIN_HARNESS_SYNC_LOCA}    ${TEST_DATA_PATH}
      
	# 1. Launch and log into SM Web
    # 2. Go to "Site Manager" page
    Create Object    buildingName=${building_name}    floorName=${FLOOR_NAME}    roomName=${ROOM_NAME}    rackName=${RACK_NAME}
        
    # 3. Add to Rack 001: 
        # _non iPatch Panel 01 (DM24)
        # _non iPatch Panel 02 (DM24)
        # _non iPatch Panel 03 (DM24)
        # _LC Server 01 (12 ports)
    Add Systimax Fiber Equipment    ${SITE_NODE}/${building_name}/${FLOOR_NAME}/${ROOM_NAME}/${POSITION_RACK_NAME}    ${360 G2 Fiber Shelf (1U)}    ${PANEL_NAME_1}    
    ...    Module 1A,${TRUE},LC 12 Port,${DM 24};Module 1B,${FALSE};Module 1C,${FALSE};Module 1D,${FALSE}    3
        
    # Add to Room 01: 
        # _LC MNE 01 (12 ports) (10.50.2.19 in C:\Pre Condition\SNMP\Original\Blending\TM_BLE_219) - Switch 01
    Add Managed Switch    ${SITE_NODE}/${BUILDING_NAME}/${FLOOR_NAME}/${ROOM_NAME}    ${SWITCH_NAME}    ${IP_ADD}    IPv4    ${False}
    Synchronize Managed Switch    ${SITE_NODE}/${BUILDING_NAME}/${FLOOR_NAME}/${ROOM_NAME}/${SWITCH_NAME}
    Open Synchronize Status Window
    Wait Until Synchronize Successfully    ${SWITCH_NAME}    ${IP_ADD}
    
    Add Device In Rack    ${SITE_NODE}/${building_name}/${FLOOR_NAME}/${ROOM_NAME}/${POSITION_RACK_NAME}    ${SERVER_NAME}
    Add Device In Rack Port    ${SITE_NODE}/${building_name}/${FLOOR_NAME}/${ROOM_NAME}/${POSITION_RACK_NAME}/${SERVER_NAME}    01    portType=LC    quantity=12
    
    # 4. Create patching from:
        # _ LC Server 01-12 -> Panel 01/Module 01/01-12
        # Do not complete job
    Open Patching Window    ${SITE_NODE}/${building_name}/${FLOOR_NAME}/${ROOM_NAME}/${POSITION_RACK_NAME}/${PANEL_NAME_1}/${Module 1A} (${DM 24})
    :FOR    ${i}    IN RANGE    0    len(${port_list})    
    \    Create Patching    ${SITE_NODE}/${building_name}/${FLOOR_NAME}/${ROOM_NAME}/${POSITION_RACK_NAME}/${PANEL_NAME_1}/${Module 1A} (${DM 24})    ${SITE_NODE}/${building_name}/${FLOOR_NAME}/${ROOM_NAME}/${POSITION_RACK_NAME}/${SERVER_NAME}        
    ...    ${port_list}[${i}]    ${port_list}[${i}]    clickNext=${False}
    Save Patching Window
    Create Work Order    ${tc_wo_name}    
        
    # 5. Create cabling from:
        # _ Panel 01/Module 01/MPO 01 - Panel 02/Module 01/MPO 01
    Open Cabling Window    ${SITE_NODE}/${building_name}/${FLOOR_NAME}/${ROOM_NAME}/${POSITION_RACK_NAME}/${PANEL_NAME_1}
    Create Cabling    Connect    ${SITE_NODE}/${building_name}/${FLOOR_NAME}/${ROOM_NAME}/${POSITION_RACK_NAME}/${PANEL_NAME_1}/${Module 1A} (${DM 24})/${MPO} ${01}    ${SITE_NODE}/${building_name}/${FLOOR_NAME}/${ROOM_NAME}/${POSITION_RACK_NAME}/${PANEL_NAME_2}/${Module 1A} (${DM 24})
    ...    portsTo=${MPO} ${01}    
    Close Cabling Window
    
    # 6. Create patching from:
        # _ Panel 02/Module 01/01-12 -> Panel 03/Module 01/01-12 Do not complete job
    Open Patching Window    ${SITE_NODE}/${building_name}/${FLOOR_NAME}/${ROOM_NAME}/${POSITION_RACK_NAME}/${PANEL_NAME_2}/${Module 1A} (${DM 24})
    :FOR    ${i}    IN RANGE    0    len(${port_list})    
    \    Create Patching    ${SITE_NODE}/${building_name}/${FLOOR_NAME}/${ROOM_NAME}/${POSITION_RACK_NAME}/${PANEL_NAME_2}/${Module 1A} (${DM 24})    ${SITE_NODE}/${building_name}/${FLOOR_NAME}/${ROOM_NAME}/${POSITION_RACK_NAME}/${PANEL_NAME_3}/${Module 1A} (${DM 24})        
    ...    ${port_list}[${i}]    ${port_list}[${i}]    clickNext=${False}
    Save Patching Window
    Create Work Order    ${tc_wo_name_2} 
    
    # 7. Create cabling (MPO24-12xLC EB) from:
        # _ Panel 03/Module 01/MPO 01 -> LC Switch 01-12
    Open Cabling Window    ${SITE_NODE}/${building_name}/${FLOOR_NAME}/${ROOM_NAME}/${POSITION_RACK_NAME}/${PANEL_NAME_3}
    Create Cabling    Connect    ${SITE_NODE}/${building_name}/${FLOOR_NAME}/${ROOM_NAME}/${POSITION_RACK_NAME}/${PANEL_NAME_3}/${Module 1A} (${DM 24})/${MPO} ${01}    ${SITE_NODE}/${BUILDING_NAME}/${FLOOR_NAME}/${ROOM_NAME}/${SWITCH_NAME}
    ...    mpoTab=${MPO24}    mpoType=${Mpo24_12xLC_EB}    mpoBranches=${Pair 1},${Pair 2},${Pair 3},${Pair 4},${Pair 5},${Pair 6},${Pair 7},${Pair 8},${Pair 9},${Pair 10},${Pair 11},${Pair 12}
    ...    portsTo=${01},${02},${03},${04},${05},${06},${07},${08},${09},${10},${11},${12}            
    
    # 8. Observe port icon and port status on Cabling window
    # * Step 8: 
        # _Port icon for Panel 03/Module 01/MPO 01 : cabled and patched scheduled icon
        # _Port icon for LC Switch 01-12: cabled icon
        # _Port status for LC Switch 01-12: Cabled to a Panel port
    Check Icon Object On Cabling Tree    From    ${SITE_NODE}/${building_name}/${FLOOR_NAME}/${ROOM_NAME}/${POSITION_RACK_NAME}/${PANEL_NAME_3}/${Module 1A} (${DM 24})/${MPO} ${01}    ${PortMPOServiceCabledInUseScheduled}    
    :FOR    ${i}    IN RANGE    0    len(${port_list})
    \    Check Icon Object On Cabling Tree    To    ${SITE_NODE}/${BUILDING_NAME}/${FLOOR_NAME}/${ROOM_NAME}/${SWITCH_NAME}/${port_list}[${i}]    ${PortNEFDPanelCabled}
    \    Check Port Status On Cabling Window    To    ${SITE_NODE}/${BUILDING_NAME}/${FLOOR_NAME}/${ROOM_NAME}/${SWITCH_NAME}/${port_list}[${i}]    ${Cabled to a panel port}
            
    # 9. Trace Panel 03 / Module 01A / 01 Port in Cabling window and observe
    Open Trace Window On Cabling    To    ${SITE_NODE}/${BUILDING_NAME}/${FLOOR_NAME}/${ROOM_NAME}/${SWITCH_NAME}/${01}
    # + Current View:
        # _Service -> connected to -> Switch 01 / 01 -> cabled (MPO24-12xLC EB) to -> Panel 03 / 01
    Check Total Trace On Site Manager    3
    Check Trace Object On Site Manager    indexObject=1    objectPosition=since:${today}    objectPath=${Config}: /${VLAN}: ${vlan_list}/${Service}:    objectType=${Trace Service Object}
	...    connectionType=${Trace Assign}    informationDevice=${Service}:->->${VLAN}: ${vlan_list}->${Config}:
    Check Trace Object On Site Manager    indexObject=2    objectPosition=1    objectPath=${SITE_NODE}/${BUILDING_NAME}/${FLOOR_NAME}/${ROOM_NAME}/${SWITCH_NAME}/${01}    objectType=${Trace Managed Switch}
	...    connectionType=${TRACE_LC_12X_MPO24_FRONT_BACK_EB_CABLING}    informationDevice=${01}->IP: ${IP_ADD}->${SWITCH_NAME}->${ROOM_NAME}->${FLOOR_NAME}->${building_name}->${SITE_NODE}
    Check Trace Object On Site Manager    indexObject=3    objectPosition=1 (${MPO1})    objectPath=${SITE_NODE}/${building_name}/${FLOOR_NAME}/${ROOM_NAME}/${POSITION_RACK_NAME}/${PANEL_NAME_3}/${Module 1A} (${DM 24})/${01}    objectType=${Trace DM24}
	...    connectionType=None    informationDevice=${01}->${Module 1A} (${DM 24})->${PANEL_NAME_3}->${POSITION_RACK_NAME}->${ROOM_NAME}->${FLOOR_NAME}->${building_name}->${SITE_NODE}
	
    # + Scheduled View
        # _Service -> connected to -> Switch 01 / 01 -> cabled (MPO24-12xLC EB) to -> Panel 03 / 01 -> patched to -> Panel 02 / 01 -> cabled to -> Panel 01 / 01 -> patched to -> LC Server 01 / 01
    Select View Type On Trace    Scheduled
    Check Total Trace On Site Manager    6
    Check Trace Object On Site Manager    indexObject=1    objectPosition=since:${today}    objectPath=${Config}: /${VLAN}: ${vlan_list}/${Service}:    objectType=${Trace Service Object}
	...    connectionType=${Trace Assign}    informationDevice=${Service}:->->${VLAN}: ${vlan_list}->${Config}:
    Check Trace Object On Site Manager    indexObject=2    objectPosition=1    objectPath=${SITE_NODE}/${BUILDING_NAME}/${FLOOR_NAME}/${ROOM_NAME}/${SWITCH_NAME}/${01}    objectType=${Trace Managed Switch}
	...    connectionType=${TRACE_LC_12X_MPO24_FRONT_BACK_EB_CABLING}    informationDevice=${01}->IP: ${IP_ADD}->${SWITCH_NAME}->${ROOM_NAME}->${FLOOR_NAME}->${building_name}->${SITE_NODE}
    Check Trace Object On Site Manager    indexObject=3    objectPosition=1 (${MPO1})    objectPath=${SITE_NODE}/${building_name}/${FLOOR_NAME}/${ROOM_NAME}/${POSITION_RACK_NAME}/${PANEL_NAME_3}/${Module 1A} (${DM 24})/${01}    objectType=${Trace DM24}
	...    connectionType=${TRACE_FIBER_PATCHING}    scheduleIcon=${TRACE_SCHEDULED_WORK_TIME_PERIOD}    informationDevice=${01}->${Module 1A} (${DM 24})->${PANEL_NAME_3}->${POSITION_RACK_NAME}->${ROOM_NAME}->${FLOOR_NAME}->${building_name}->${SITE_NODE}    
    Check Trace Object On Site Manager    indexObject=4    objectPosition=1 (${MPO1})    objectPath=${SITE_NODE}/${building_name}/${FLOOR_NAME}/${ROOM_NAME}/${POSITION_RACK_NAME}/${PANEL_NAME_2}/${Module 1A} (${DM 24})/${01}    objectType=${Trace DM24}
	...    connectionType=${CABLE_MPO24_BACK_BACK_CABLING}    informationDevice=${01}->${Module 1A} (${DM 24})->${PANEL_NAME_2}->${POSITION_RACK_NAME}->${ROOM_NAME}->${FLOOR_NAME}->${building_name}->${SITE_NODE}
	Check Trace Object On Site Manager    indexObject=5    objectPosition=1 (${MPO1})    objectPath=${SITE_NODE}/${building_name}/${FLOOR_NAME}/${ROOM_NAME}/${POSITION_RACK_NAME}/${PANEL_NAME_1}/${Module 1A} (${DM 24})/${01}    objectType=${Trace DM24}
	...    connectionType=${TRACE_FIBER_PATCHING}    scheduleIcon=${TRACE_SCHEDULED_WORK_TIME_PERIOD}    informationDevice=${01}->${Module 1A} (${DM 24})->${PANEL_NAME_1}->${POSITION_RACK_NAME}->${ROOM_NAME}->${FLOOR_NAME}->${building_name}->${SITE_NODE}
	Check Trace Object On Site Manager    indexObject=6    objectPosition=1    objectPath=${SITE_NODE}/${building_name}/${FLOOR_NAME}/${ROOM_NAME}/${POSITION_RACK_NAME}/${SERVER_NAME}/${01}    objectType=${Trace Server Fiber}
	...    connectionType=None    informationDevice=${01}->${SERVER_NAME}->${POSITION_RACK_NAME}->${ROOM_NAME}->${FLOOR_NAME}->${building_name}->${SITE_NODE}
    Close Trace Window
       
    # 10. Close cabling window, observe port icon and status on content pane
    Close Cabling Window
    
    # _Status of ports in circuit:
      # + Panel 03// 01-12: In Use - Pending
      # + Switch 01// 01-12: In Use - Pending
    # _Icon of ports in circuit:
      # + Panel 03// 01-12:cabled and patched scheduled icon
      # + Switch 01// 01-12: cabled scheduled icon
    Click Tree Node On Site Manager    ${SITE_NODE}/${building_name}/${FLOOR_NAME}/${ROOM_NAME}/${POSITION_RACK_NAME}/${PANEL_NAME_3}/${Module 1A} (${DM 24})    
    :FOR    ${i}    IN RANGE    0    len(${port_list})
    \    Check Icon Object Exist On Content Pane    ${port_list}[${i}]    ${PortNEFDServiceInUseScheduled}  
	\    Check Table Row Map With Header On Content Table    ${Name},${Port Status}    ${port_list}[${i}],${In Use - Pending}    ${i+1}
   
    Click Tree Node On Site Manager    ${SITE_NODE}/${BUILDING_NAME}/${FLOOR_NAME}/${ROOM_NAME}/${SWITCH_NAME}    
    :FOR    ${i}    IN RANGE    0    len(${port_list})
    \    Check Icon Object Exist On Content Pane    ${port_list}[${i}]    ${PortNEFDPanelCabled} 
	\    Check Table Row Map With Header On Content Table    ${Name},${Port Status}    ${port_list}[${i}],${In Use - Pending}    ${i+1} 

    # 11. Trace Panel 03 / 01-06, Switch 01 / 07-12
    Open Trace Window    ${SITE_NODE}/${building_name}/${FLOOR_NAME}/${ROOM_NAME}/${POSITION_RACK_NAME}/${PANEL_NAME_3}/${Module 1A} (${DM 24})    ${01}
    # * Step 11: The trace circuit show in Vertical and Horizontal:
    # + Current View:
        # _Service -> connected to -> Switch 01 / 01-12 -> cabled (MPO24-12xLC EB) to -> Panel 03 / 01-12
    Check Total Trace On Site Manager    3
    Check Trace Object On Site Manager    indexObject=1    objectPosition=since:${today}    objectPath=${Config}: /${VLAN}: ${vlan_list}/${Service}:    objectType=${Trace Service Object}
	...    connectionType=${Trace Assign}    informationDevice=${Service}:->->${VLAN}: ${vlan_list}->${Config}:
    Check Trace Object On Site Manager    indexObject=2    objectPosition=1    objectPath=${SITE_NODE}/${BUILDING_NAME}/${FLOOR_NAME}/${ROOM_NAME}/${SWITCH_NAME}/${01}    objectType=${Trace Managed Switch}
	...    connectionType=${TRACE_LC_12X_MPO24_FRONT_BACK_EB_CABLING}    informationDevice=${01}->IP: ${IP_ADD}->${SWITCH_NAME}->${ROOM_NAME}->${FLOOR_NAME}->${building_name}->${SITE_NODE}
    Check Trace Object On Site Manager    indexObject=3    objectPosition=1 (${MPO1})    objectPath=${SITE_NODE}/${building_name}/${FLOOR_NAME}/${ROOM_NAME}/${POSITION_RACK_NAME}/${PANEL_NAME_3}/${Module 1A} (${DM 24})/${01}    objectType=${Trace DM24}
	...    connectionType=None    informationDevice=${01}->${Module 1A} (${DM 24})->${PANEL_NAME_3}->${POSITION_RACK_NAME}->${ROOM_NAME}->${FLOOR_NAME}->${building_name}->${SITE_NODE}
    # + Scheduled View
        # _Service -> connected to -> Switch 01 / 01-12 -> cabled (MPO24-12xLC EB) to -> Panel 03 / 01-12 -> patched to -> Panel 02 / 01-12 -> cabled to -> Panel 01 / 01-12 -> patched to -> LC Server 01 / 01-12
    Select View Type On Trace    Scheduled
    Check Total Trace On Site Manager    6
    Check Trace Object On Site Manager    indexObject=1    objectPosition=since:${today}    objectPath=${Config}: /${VLAN}: ${vlan_list}/${Service}:    objectType=${Trace Service Object}
	...    connectionType=${Trace Assign}    informationDevice=${Service}:->->${VLAN}: ${vlan_list}->${Config}:
    Check Trace Object On Site Manager    indexObject=2    objectPosition=1    objectPath=${SITE_NODE}/${BUILDING_NAME}/${FLOOR_NAME}/${ROOM_NAME}/${SWITCH_NAME}/${01}    objectType=${Trace Managed Switch}
	...    connectionType=${TRACE_LC_12X_MPO24_FRONT_BACK_EB_CABLING}    informationDevice=${01}->IP: ${IP_ADD}->${SWITCH_NAME}->${ROOM_NAME}->${FLOOR_NAME}->${building_name}->${SITE_NODE}
    Check Trace Object On Site Manager    indexObject=3    objectPosition=1 (${MPO1})    objectPath=${SITE_NODE}/${building_name}/${FLOOR_NAME}/${ROOM_NAME}/${POSITION_RACK_NAME}/${PANEL_NAME_3}/${Module 1A} (${DM 24})/${01}    objectType=${Trace DM24}
	...    connectionType=${TRACE_FIBER_PATCHING}    scheduleIcon=${TRACE_SCHEDULED_WORK_TIME_PERIOD}    informationDevice=${01}->${Module 1A} (${DM 24})->${PANEL_NAME_3}->${POSITION_RACK_NAME}->${ROOM_NAME}->${FLOOR_NAME}->${building_name}->${SITE_NODE}    
    Check Trace Object On Site Manager    indexObject=4    objectPosition=1 (${MPO1})    objectPath=${SITE_NODE}/${building_name}/${FLOOR_NAME}/${ROOM_NAME}/${POSITION_RACK_NAME}/${PANEL_NAME_2}/${Module 1A} (${DM 24})/${01}    objectType=${Trace DM24}
	...    connectionType=${CABLE_MPO24_BACK_BACK_CABLING}    informationDevice=${01}->${Module 1A} (${DM 24})->${PANEL_NAME_2}->${POSITION_RACK_NAME}->${ROOM_NAME}->${FLOOR_NAME}->${building_name}->${SITE_NODE}
	Check Trace Object On Site Manager    indexObject=5    objectPosition=1 (${MPO1})    objectPath=${SITE_NODE}/${building_name}/${FLOOR_NAME}/${ROOM_NAME}/${POSITION_RACK_NAME}/${PANEL_NAME_1}/${Module 1A} (${DM 24})/${01}    objectType=${Trace DM24}
	...    connectionType=${TRACE_FIBER_PATCHING}    scheduleIcon=${TRACE_SCHEDULED_WORK_TIME_PERIOD}    informationDevice=${01}->${Module 1A} (${DM 24})->${PANEL_NAME_1}->${POSITION_RACK_NAME}->${ROOM_NAME}->${FLOOR_NAME}->${building_name}->${SITE_NODE}
	Check Trace Object On Site Manager    indexObject=6    objectPosition=1    objectPath=${SITE_NODE}/${building_name}/${FLOOR_NAME}/${ROOM_NAME}/${POSITION_RACK_NAME}/${SERVER_NAME}/${01}    objectType=${Trace Server Fiber}
	...    connectionType=None    informationDevice=${01}->${SERVER_NAME}->${POSITION_RACK_NAME}->${ROOM_NAME}->${FLOOR_NAME}->${building_name}->${SITE_NODE}
    Close Trace Window
        
    # 12. Open Patching window and observe the Curcuit Trace of Panel 03 / 01-12
    # * Step 12: 
        # _Service -> connected to -> Switch 01 / 01-12 -> cabled (MPO24-12xLC EB) to -> Panel 03 / 01-12 -> patched to -> Panel 02 / 01-12 -> cabled to -> Panel 01 / 01-12 -> patched to -> LC Server 01 / 01-12
    Open Patching Window    ${SITE_NODE}/${building_name}/${FLOOR_NAME}/${ROOM_NAME}/${POSITION_RACK_NAME}/${PANEL_NAME_3}/${Module 1A} (${DM 24})    ${01}
    Check Trace Object On Patching    indexObject=1    objectPosition=since:${today}    objectType=${Trace Service Object}
	...    connectionType=${Trace Assign}    informationDevice=${Service}:->->${VLAN}: ${vlan_list}->${Config}:
    Check Trace Object On Patching    indexObject=2    objectPosition=1    objectType=${Trace Managed Switch}
	...    connectionType=${TRACE_LC_12X_MPO24_FRONT_BACK_EB_CABLING}    informationDevice=${01}->IP: ${IP_ADD}->${SWITCH_NAME}->${ROOM_NAME}->${FLOOR_NAME}->${building_name}->${SITE_NODE}
    Check Trace Object On Patching    indexObject=3    objectPosition=1 (${MPO1})    objectType=${Trace DM24}
	...    connectionType=${TRACE_FIBER_PATCHING}    scheduleIcon=${TRACE_SCHEDULED_WORK_TIME_PERIOD}    informationDevice=${01}->${Module 1A} (${DM 24})->${PANEL_NAME_3}->${POSITION_RACK_NAME}->${ROOM_NAME}->${FLOOR_NAME}->${building_name}->${SITE_NODE}    
    Check Trace Object On Patching    indexObject=4    objectPosition=1 (${MPO1})    objectType=${Trace DM24}
	...    connectionType=${CABLE_MPO24_BACK_BACK_CABLING}    informationDevice=${01}->${Module 1A} (${DM 24})->${PANEL_NAME_2}->${POSITION_RACK_NAME}->${ROOM_NAME}->${FLOOR_NAME}->${building_name}->${SITE_NODE}
	Check Trace Object On Patching    indexObject=5    objectPosition=1 (${MPO1})    objectType=${Trace DM24}
	...    connectionType=${TRACE_FIBER_PATCHING}    scheduleIcon=${TRACE_SCHEDULED_WORK_TIME_PERIOD}    informationDevice=${01}->${Module 1A} (${DM 24})->${PANEL_NAME_1}->${POSITION_RACK_NAME}->${ROOM_NAME}->${FLOOR_NAME}->${building_name}->${SITE_NODE}
	Check Trace Object On Patching    indexObject=6    objectPosition=1    objectType=${Trace Server Fiber}
	...    connectionType=None    informationDevice=${01}->${SERVER_NAME}->${POSITION_RACK_NAME}->${ROOM_NAME}->${FLOOR_NAME}->${building_name}->${SITE_NODE}
	Close Patching Window
	        
    # 13. Complete all WO, observe port icon status
    Open Work Order Queue Window
    Complete Work Order    ${tc_wo_name},${tc_wo_name_2}
    Close Work Order Queue
        
    # * Step 13: 
    # _Status of ports in circuit:
      # + Panel 03/ 01-12: In Use
      # + Switch 01/ 01-12: In Use
    # _Icon of ports in circuit:
      # + Panel 03/ 01-12:cabled and patched icon
      # + Switch 01/ 01-12: cabled icon
    Click Tree Node On Site Manager    ${SITE_NODE}/${building_name}/${FLOOR_NAME}/${ROOM_NAME}/${POSITION_RACK_NAME}/${PANEL_NAME_3}/${Module 1A} (${DM 24})    
    :FOR    ${i}    IN RANGE    0    len(${port_list})
    \    Check Icon Object Exist On Content Pane    ${port_list}[${i}]    ${PortNEFDServiceInUse}  
	\    Check Table Row Map With Header On Content Table    ${Name},${Port Status}    ${port_list}[${i}],${In Use}    ${i+1}
   
    Click Tree Node On Site Manager    ${SITE_NODE}/${BUILDING_NAME}/${FLOOR_NAME}/${ROOM_NAME}/${SWITCH_NAME}    
    :FOR    ${i}    IN RANGE    0    len(${port_list})
    \    Check Icon Object Exist On Content Pane    ${port_list}[${i}]    ${PortNEFDPanelCabled} 
	\    Check Table Row Map With Header On Content Table    ${Name},${Port Status}    ${port_list}[${i}],${In Use}    ${i+1} 
      
    # 14. Trace Panel 03 / 01-06, Switch 01 / 07-12
    # * Step 14: The trace circuit show in Vertical and Horizontal
        # _Service -> connected to -> Switch 01 / 01-12 -> cabled (MPO24-12xLC EB) to -> Panel 03 / 01-12 -> patched to -> Panel 02 / 01-12 -> cabled to -> Panel 01 / 01-12 -> patched to -> LC Server 01 / 01-12
    Open Trace Window    ${SITE_NODE}/${building_name}/${FLOOR_NAME}/${ROOM_NAME}/${POSITION_RACK_NAME}/${PANEL_NAME_3}/${Module 1A} (${DM 24})    ${01}
    Check Total Trace On Site Manager    6
    Check Trace Object On Site Manager    indexObject=1    objectPosition=since:${today}    objectPath=${Config}: /${VLAN}: ${vlan_list}/${Service}:    objectType=${Trace Service Object}
	...    connectionType=${Trace Assign}    informationDevice=${Service}:->->${VLAN}: ${vlan_list}->${Config}:
    Check Trace Object On Site Manager    indexObject=2    objectPosition=1    objectPath=${SITE_NODE}/${BUILDING_NAME}/${FLOOR_NAME}/${ROOM_NAME}/${SWITCH_NAME}/${01}    objectType=${Trace Managed Switch}
	...    connectionType=${TRACE_LC_12X_MPO24_FRONT_BACK_EB_CABLING}    informationDevice=${01}->IP: ${IP_ADD}->${SWITCH_NAME}->${ROOM_NAME}->${FLOOR_NAME}->${building_name}->${SITE_NODE}
    Check Trace Object On Site Manager    indexObject=3    objectPosition=1 (${MPO1})    objectPath=${SITE_NODE}/${building_name}/${FLOOR_NAME}/${ROOM_NAME}/${POSITION_RACK_NAME}/${PANEL_NAME_3}/${Module 1A} (${DM 24})/${01}    objectType=${Trace DM24}
	...    connectionType=${TRACE_FIBER_PATCHING}    informationDevice=${01}->${Module 1A} (${DM 24})->${PANEL_NAME_3}->${POSITION_RACK_NAME}->${ROOM_NAME}->${FLOOR_NAME}->${building_name}->${SITE_NODE}    
    Check Trace Object On Site Manager    indexObject=4    objectPosition=1 (${MPO1})    objectPath=${SITE_NODE}/${building_name}/${FLOOR_NAME}/${ROOM_NAME}/${POSITION_RACK_NAME}/${PANEL_NAME_2}/${Module 1A} (${DM 24})/${01}    objectType=${Trace DM24}
	...    connectionType=${CABLE_MPO24_BACK_BACK_CABLING}    informationDevice=${01}->${Module 1A} (${DM 24})->${PANEL_NAME_2}->${POSITION_RACK_NAME}->${ROOM_NAME}->${FLOOR_NAME}->${building_name}->${SITE_NODE}
	Check Trace Object On Site Manager    indexObject=5    objectPosition=1 (${MPO1})    objectPath=${SITE_NODE}/${building_name}/${FLOOR_NAME}/${ROOM_NAME}/${POSITION_RACK_NAME}/${PANEL_NAME_1}/${Module 1A} (${DM 24})/${01}    objectType=${Trace DM24}
	...    connectionType=${TRACE_FIBER_PATCHING}    informationDevice=${01}->${Module 1A} (${DM 24})->${PANEL_NAME_1}->${POSITION_RACK_NAME}->${ROOM_NAME}->${FLOOR_NAME}->${building_name}->${SITE_NODE}
	Check Trace Object On Site Manager    indexObject=6    objectPosition=1    objectPath=${SITE_NODE}/${building_name}/${FLOOR_NAME}/${ROOM_NAME}/${POSITION_RACK_NAME}/${SERVER_NAME}/${01}    objectType=${Trace Server Fiber}
	...    connectionType=None    informationDevice=${01}->${SERVER_NAME}->${POSITION_RACK_NAME}->${ROOM_NAME}->${FLOOR_NAME}->${building_name}->${SITE_NODE}
    Close Trace Window
        
    # 15. Right click on LC MNE and select Delete
    Delete Tree Node On Site Manager    ${SITE_NODE}/${BUILDING_NAME}/${FLOOR_NAME}/${ROOM_NAME}/${SWITCH_NAME}
    
    # 16. Observe port icon and status on content pane of Panel 03
    # * Step 16: 
    # _Status of ports in circuit:
      # + Panel 03/ 01-12: In Use
    # _Icon of ports in circuit:
      # + Panel 03/ 01-12: uncabled and patched icon
    Click Tree Node On Site Manager    ${SITE_NODE}/${building_name}/${FLOOR_NAME}/${ROOM_NAME}/${POSITION_RACK_NAME}/${PANEL_NAME_3}/${Module 1A} (${DM 24})    
    :FOR    ${i}    IN RANGE    0    len(${port_list})
    \    Check Icon Object Exist On Content Pane    ${port_list}[${i}]    ${PortFDUncabledInUse}  
	\    Check Table Row Map With Header On Content Table    ${Name},${Port Status}    ${port_list}[${i}],${In Use}    ${i+1}

    # 17. Trace Panel 03 / 01 and open Trace
    Open Trace Window    ${SITE_NODE}/${building_name}/${FLOOR_NAME}/${ROOM_NAME}/${POSITION_RACK_NAME}/${PANEL_NAME_3}/${Module 1A} (${DM 24})    ${01}
    Check Total Trace On Site Manager    4
    Check Trace Object On Site Manager    indexObject=1    objectPosition=1    objectPath=${SITE_NODE}/${building_name}/${FLOOR_NAME}/${ROOM_NAME}/${POSITION_RACK_NAME}/${PANEL_NAME_3}/${Module 1A} (${DM 24})/${01}    objectType=${Trace DM24}
	...    connectionType=${TRACE_FIBER_PATCHING}    informationDevice=${01}->${Module 1A} (${DM 24})->${PANEL_NAME_3}->${POSITION_RACK_NAME}->${ROOM_NAME}->${FLOOR_NAME}->${building_name}->${SITE_NODE}
	Check Trace Object On Site Manager    indexObject=2    objectPosition=1 (${MPO1})    objectPath=${SITE_NODE}/${building_name}/${FLOOR_NAME}/${ROOM_NAME}/${POSITION_RACK_NAME}/${PANEL_NAME_2}/${Module 1A} (${DM 24})/${01}    objectType=${Trace DM24}
	...    connectionType=${CABLE_MPO24_BACK_BACK_CABLING}    informationDevice=${01}->${Module 1A} (${DM 24})->${PANEL_NAME_2}->${POSITION_RACK_NAME}->${ROOM_NAME}->${FLOOR_NAME}->${building_name}->${SITE_NODE}
	Check Trace Object On Site Manager    indexObject=3    objectPosition=1 (${MPO1})    objectPath=${SITE_NODE}/${building_name}/${FLOOR_NAME}/${ROOM_NAME}/${POSITION_RACK_NAME}/${PANEL_NAME_1}/${Module 1A} (${DM 24})/${01}    objectType=${Trace DM24}
	...    connectionType=${TRACE_FIBER_PATCHING}    informationDevice=${01}->${Module 1A} (${DM 24})->${PANEL_NAME_1}->${POSITION_RACK_NAME}->${ROOM_NAME}->${FLOOR_NAME}->${building_name}->${SITE_NODE}
	Check Trace Object On Site Manager    indexObject=4    objectPosition=1    objectPath=${SITE_NODE}/${building_name}/${FLOOR_NAME}/${ROOM_NAME}/${POSITION_RACK_NAME}/${SERVER_NAME}/${01}    objectType=${Trace Server Fiber}
	...    connectionType=None    informationDevice=${01}->${SERVER_NAME}->${POSITION_RACK_NAME}->${ROOM_NAME}->${FLOOR_NAME}->${building_name}->${SITE_NODE}
	  
SM-30337_SM-30335_07-Verify that the service is propagated and trace correctly in new customer circuit (DGT): LC Server -> patched to -> iPatch DM24 -> cabled to -> iPatch DM24 -> patched to -> iPatch DM24 -> cabled (MPO24-12xLC EB) to -> LC Panel
    [Setup]    Run Keywords    Set Test Variable    ${building_name}    BD_SM-30337_SM-30335_07
    ...    AND    Delete Building     ${building_name}  
    ...    AND    Open SM Login Page
    ...    AND    Login To SM    ${USERNAME}    ${PASSWORD}
    
    [Teardown]    Run Keywords    Close Browser
    ...    AND    Delete Building     ${building_name}
    
    Set Tags    SM-30335    SM-30337     

    ${tree_node_rack}    Set Variable    ${SITE_NODE}/${building_name}/${FLOOR_NAME}/${ROOM_NAME}/${POSITION_RACK_NAME}
    ${tc_wo_name}    Set Variable    WO_30337_30335_07_01
    ${tc_wo_name_2}    Set Variable    WO_30337_30335_07_02
    ${port_list}    Create List    ${01}    ${02}    ${03}    ${04}    ${05}    ${06}    ${07}    ${08}    ${09}    ${10}    ${11}    ${12}
    ${vlan_list}	Set Variable    111  
      
	# 1. Launch and log into SM Web
    # 2. Go to "Site Manager" page
    Create Object    buildingName=${building_name}    floorName=${FLOOR_NAME}    roomName=${ROOM_NAME}    rackName=${RACK_NAME}
        
    # 3. Add to Rack 001: 
        # _non iPatch Panel 01 (DM24)
        # _non iPatch Panel 02 (DM24)
        # _non iPatch Panel 03 (DM24)
        # _LC Server 01 (12 ports)
    Add Systimax Fiber Equipment    ${SITE_NODE}/${building_name}/${FLOOR_NAME}/${ROOM_NAME}/${POSITION_RACK_NAME}    ${360 G2 Fiber Shelf (1U)}    ${PANEL_NAME_1}    
    ...    Module 1A,${TRUE},LC 12 Port,${DM 24};Module 1B,${FALSE};Module 1C,${FALSE};Module 1D,${FALSE}    3
    
    Add Device In Rack    ${SITE_NODE}/${building_name}/${FLOOR_NAME}/${ROOM_NAME}/${POSITION_RACK_NAME}    ${SERVER_NAME}
    Add Device In Rack Port    ${SITE_NODE}/${building_name}/${FLOOR_NAME}/${ROOM_NAME}/${POSITION_RACK_NAME}/${SERVER_NAME}    01    portType=LC    quantity=12
        
        # _LC Panel 04 (12 ports)
    Add Generic Panel    ${SITE_NODE}/${building_name}/${FLOOR_NAME}/${ROOM_NAME}/${POSITION_RACK_NAME}    ${PANEL_NAME_4}    portType=${LC}    
    
    # 4. Create patching from:
        # _ LC Server 01-12 -> Panel 01/Module 01/01-12
        # Do not complete job
    Open Patching Window    ${SITE_NODE}/${building_name}/${FLOOR_NAME}/${ROOM_NAME}/${POSITION_RACK_NAME}/${PANEL_NAME_1}/${Module 1A} (${DM 24})
    :FOR    ${i}    IN RANGE    0    len(${port_list})    
    \    Create Patching    ${SITE_NODE}/${building_name}/${FLOOR_NAME}/${ROOM_NAME}/${POSITION_RACK_NAME}/${PANEL_NAME_1}/${Module 1A} (${DM 24})    ${SITE_NODE}/${building_name}/${FLOOR_NAME}/${ROOM_NAME}/${POSITION_RACK_NAME}/${SERVER_NAME}        
    ...    ${port_list}[${i}]    ${port_list}[${i}]    clickNext=${False}
    Save Patching Window
    Create Work Order    ${tc_wo_name}    
        
    # 5. Create cabling from:
        # _ Panel 01/Module 01/MPO 01 - Panel 02/Module 01/MPO 01
    Open Cabling Window    ${SITE_NODE}/${building_name}/${FLOOR_NAME}/${ROOM_NAME}/${POSITION_RACK_NAME}/${PANEL_NAME_1}
    Create Cabling    Connect    ${SITE_NODE}/${building_name}/${FLOOR_NAME}/${ROOM_NAME}/${POSITION_RACK_NAME}/${PANEL_NAME_1}/${Module 1A} (${DM 24})/${MPO} ${01}    ${SITE_NODE}/${building_name}/${FLOOR_NAME}/${ROOM_NAME}/${POSITION_RACK_NAME}/${PANEL_NAME_2}/${Module 1A} (${DM 24})
    ...    portsTo=${MPO} ${01}    
    Close Cabling Window
    
    # 6. Create patching from:
        # _ Panel 02/Module 01/01-12 -> Panel 03/Module 01/01-12 Do not complete job
    Open Patching Window    ${SITE_NODE}/${building_name}/${FLOOR_NAME}/${ROOM_NAME}/${POSITION_RACK_NAME}/${PANEL_NAME_2}/${Module 1A} (${DM 24})
    :FOR    ${i}    IN RANGE    0    len(${port_list})    
    \    Create Patching    ${SITE_NODE}/${building_name}/${FLOOR_NAME}/${ROOM_NAME}/${POSITION_RACK_NAME}/${PANEL_NAME_2}/${Module 1A} (${DM 24})    ${SITE_NODE}/${building_name}/${FLOOR_NAME}/${ROOM_NAME}/${POSITION_RACK_NAME}/${PANEL_NAME_3}/${Module 1A} (${DM 24})        
    ...    ${port_list}[${i}]    ${port_list}[${i}]    clickNext=${False}
    Save Patching Window
    Create Work Order    ${tc_wo_name_2} 
    
    # 7. Create cabling (MPO24-12xLC EB) from:
        # _ Panel 03/Module 01/MPO 01 -> LC Panel 01-12
    Open Cabling Window    ${SITE_NODE}/${building_name}/${FLOOR_NAME}/${ROOM_NAME}/${POSITION_RACK_NAME}/${PANEL_NAME_3}
    Create Cabling    Connect    ${SITE_NODE}/${building_name}/${FLOOR_NAME}/${ROOM_NAME}/${POSITION_RACK_NAME}/${PANEL_NAME_3}/${Module 1A} (${DM 24})/${MPO} ${01}    ${SITE_NODE}/${BUILDING_NAME}/${FLOOR_NAME}/${ROOM_NAME}/${POSITION_RACK_NAME}/${PANEL_NAME_4}
    ...    mpoTab=${MPO24}    mpoType=${Mpo24_12xLC_EB}    mpoBranches=${Pair 1},${Pair 2},${Pair 3},${Pair 4},${Pair 5},${Pair 6},${Pair 7},${Pair 8},${Pair 9},${Pair 10},${Pair 11},${Pair 12}
    ...    portsTo=${01},${02},${03},${04},${05},${06},${07},${08},${09},${10},${11},${12}            
    
    # 8. Observe port icon and port status on Cabling window
    # * Step 8: 
        # _Port icon for Panel 03/Module 01/MPO 01 : cabled and patched scheduled icon
        # _Port icon for LC Panel 01-12: cabled icon
        # _Port status for LC Panel 01-12: Cabled to a Panel port
    Check Icon Object On Cabling Tree    From    ${SITE_NODE}/${building_name}/${FLOOR_NAME}/${ROOM_NAME}/${POSITION_RACK_NAME}/${PANEL_NAME_3}/${Module 1A} (${DM 24})/${MPO} ${01}    ${PortMPOPanelCabledInUseScheduled}    
    :FOR    ${i}    IN RANGE    0    len(${port_list})
    \    Check Icon Object On Cabling Tree    To    ${SITE_NODE}/${BUILDING_NAME}/${FLOOR_NAME}/${ROOM_NAME}/${POSITION_RACK_NAME}/${PANEL_NAME_4}/${port_list}[${i}]    ${PortFDPanelCabledAvailable}
    \    Check Port Status On Cabling Window    To    ${SITE_NODE}/${BUILDING_NAME}/${FLOOR_NAME}/${ROOM_NAME}/${POSITION_RACK_NAME}/${PANEL_NAME_4}/${port_list}[${i}]    ${Cabled to a panel port}
            
    # 9. Trace Panel 03 / Module 01A / 01 Port in Cabling window and observe
    Open Trace Window On Cabling    To    ${SITE_NODE}/${BUILDING_NAME}/${FLOOR_NAME}/${ROOM_NAME}/${POSITION_RACK_NAME}/${PANEL_NAME_4}/${01}
    # + Current View:
        # _Service -> connected to -> Switch 01 / 01 -> cabled (MPO24-12xLC EB) to -> Panel 03 / 01
    Check Total Trace On Site Manager    2
    Check Trace Object On Site Manager    indexObject=1    objectPosition=1    objectPath=${SITE_NODE}/${BUILDING_NAME}/${FLOOR_NAME}/${ROOM_NAME}/${POSITION_RACK_NAME}/${PANEL_NAME_4}/${01}    objectType=${TRACE_GENERIC_LC_PANEL_IMAGE}
	...    connectionType=${TRACE_LC_12X_MPO24_BACK_BACK_EB_CABLING}    informationDevice=${01}->${PANEL_NAME_4}->${POSITION_RACK_NAME}->${ROOM_NAME}->${FLOOR_NAME}->${building_name}->${SITE_NODE}
    Check Trace Object On Site Manager    indexObject=2    objectPosition=1 (${MPO1})    objectPath=${SITE_NODE}/${building_name}/${FLOOR_NAME}/${ROOM_NAME}/${POSITION_RACK_NAME}/${PANEL_NAME_3}/${Module 1A} (${DM 24})/${01}    objectType=${Trace DM24}
	...    connectionType=None    informationDevice=${01}->${Module 1A} (${DM 24})->${PANEL_NAME_3}->${POSITION_RACK_NAME}->${ROOM_NAME}->${FLOOR_NAME}->${building_name}->${SITE_NODE}
	
    # + Scheduled View
        # _Service -> connected to -> Switch 01 / 01 -> cabled (MPO24-12xLC EB) to -> Panel 03 / 01 -> patched to -> Panel 02 / 01 -> cabled to -> Panel 01 / 01 -> patched to -> LC Panel 01 / 01
    Select View Type On Trace    Scheduled
    Check Total Trace On Site Manager    5
    Check Trace Object On Site Manager    indexObject=1    objectPosition=1    objectPath=${SITE_NODE}/${BUILDING_NAME}/${FLOOR_NAME}/${ROOM_NAME}/${POSITION_RACK_NAME}/${PANEL_NAME_4}/${01}    objectType=${TRACE_GENERIC_LC_PANEL_IMAGE}
	...    connectionType=${TRACE_LC_12X_MPO24_BACK_BACK_EB_CABLING}    informationDevice=${01}->${PANEL_NAME_4}->${POSITION_RACK_NAME}->${ROOM_NAME}->${FLOOR_NAME}->${building_name}->${SITE_NODE}
    Check Trace Object On Site Manager    indexObject=2    objectPosition=1 (${MPO1})    objectPath=${SITE_NODE}/${building_name}/${FLOOR_NAME}/${ROOM_NAME}/${POSITION_RACK_NAME}/${PANEL_NAME_3}/${Module 1A} (${DM 24})/${01}    objectType=${Trace DM24}
	...    connectionType=${TRACE_FIBER_PATCHING}    scheduleIcon=${TRACE_SCHEDULED_WORK_TIME_PERIOD}    informationDevice=${01}->${Module 1A} (${DM 24})->${PANEL_NAME_3}->${POSITION_RACK_NAME}->${ROOM_NAME}->${FLOOR_NAME}->${building_name}->${SITE_NODE}    
    Check Trace Object On Site Manager    indexObject=3    objectPosition=1 (${MPO1})    objectPath=${SITE_NODE}/${building_name}/${FLOOR_NAME}/${ROOM_NAME}/${POSITION_RACK_NAME}/${PANEL_NAME_2}/${Module 1A} (${DM 24})/${01}    objectType=${Trace DM24}
	...    connectionType=${CABLE_MPO24_BACK_BACK_CABLING}    informationDevice=${01}->${Module 1A} (${DM 24})->${PANEL_NAME_2}->${POSITION_RACK_NAME}->${ROOM_NAME}->${FLOOR_NAME}->${building_name}->${SITE_NODE}
	Check Trace Object On Site Manager    indexObject=4    objectPosition=1 (${MPO1})    objectPath=${SITE_NODE}/${building_name}/${FLOOR_NAME}/${ROOM_NAME}/${POSITION_RACK_NAME}/${PANEL_NAME_1}/${Module 1A} (${DM 24})/${01}    objectType=${Trace DM24}
	...    connectionType=${TRACE_FIBER_PATCHING}    scheduleIcon=${TRACE_SCHEDULED_WORK_TIME_PERIOD}    informationDevice=${01}->${Module 1A} (${DM 24})->${PANEL_NAME_1}->${POSITION_RACK_NAME}->${ROOM_NAME}->${FLOOR_NAME}->${building_name}->${SITE_NODE}
	Check Trace Object On Site Manager    indexObject=5    objectPosition=1    objectPath=${SITE_NODE}/${building_name}/${FLOOR_NAME}/${ROOM_NAME}/${POSITION_RACK_NAME}/${SERVER_NAME}/${01}    objectType=${Trace Server Fiber}
	...    connectionType=None    informationDevice=${01}->${SERVER_NAME}->${POSITION_RACK_NAME}->${ROOM_NAME}->${FLOOR_NAME}->${building_name}->${SITE_NODE}
    Close Trace Window
       
    # 10. Close cabling window, observe port icon and status on content pane
    Close Cabling Window
    
    # _Status of ports in circuit:
      # + Panel 03// 01-12: In Use - Pending
      # + Panel 04// 01-12: Available
    # _Icon of ports in circuit:
      # + Panel 03// 01-12:cabled and patched scheduled icon
      # + Panel 04// 01-12: cabled scheduled icon
    Click Tree Node On Site Manager    ${SITE_NODE}/${building_name}/${FLOOR_NAME}/${ROOM_NAME}/${POSITION_RACK_NAME}/${PANEL_NAME_3}/${Module 1A} (${DM 24})    
    :FOR    ${i}    IN RANGE    0    len(${port_list})
    \    Check Icon Object Exist On Content Pane    ${port_list}[${i}]    ${PortFDPanelCabledInUseScheduled}  
	\    Check Table Row Map With Header On Content Table    ${Name},${Port Status}    ${port_list}[${i}],${In Use - Pending}    ${i+1}
   
    Click Tree Node On Site Manager    ${SITE_NODE}/${BUILDING_NAME}/${FLOOR_NAME}/${ROOM_NAME}/${POSITION_RACK_NAME}/${PANEL_NAME_4}    
    :FOR    ${i}    IN RANGE    0    len(${port_list})
    \    Check Icon Object Exist On Content Pane    ${port_list}[${i}]    ${PortFDPanelCabledAvailable} 
	\    Check Table Row Map With Header On Content Table    ${Name},${Port Status}    ${port_list}[${i}],${Available}    ${i+1} 

    # 11. Trace Panel 03 / 01-06, Switch 01 / 07-12
    Open Trace Window    ${SITE_NODE}/${building_name}/${FLOOR_NAME}/${ROOM_NAME}/${POSITION_RACK_NAME}/${PANEL_NAME_3}/${Module 1A} (${DM 24})    ${01}
    # * Step 11: The trace circuit show in Vertical and Horizontal:
    # + Current View:
        # _Service -> connected to -> Switch 01 / 01-12 -> cabled (MPO24-12xLC EB) to -> Panel 03 / 01-12
    Check Total Trace On Site Manager    2
    Check Trace Object On Site Manager    indexObject=2    objectPosition=1    objectPath=${SITE_NODE}/${BUILDING_NAME}/${FLOOR_NAME}/${ROOM_NAME}/${POSITION_RACK_NAME}/${PANEL_NAME_4}/${01}    objectType=${TRACE_GENERIC_LC_PANEL_IMAGE}
	...    connectionType=None    informationDevice=${01}->${PANEL_NAME_4}->${POSITION_RACK_NAME}->${ROOM_NAME}->${FLOOR_NAME}->${building_name}->${SITE_NODE}
    Check Trace Object On Site Manager    indexObject=1    objectPosition=1 (${MPO1})    objectPath=${SITE_NODE}/${building_name}/${FLOOR_NAME}/${ROOM_NAME}/${POSITION_RACK_NAME}/${PANEL_NAME_3}/${Module 1A} (${DM 24})/${01}    objectType=${Trace DM24}
	...    connectionType=${TRACE_MPO24_12X_LC_BACK_BACK_EB_CABLING}    informationDevice=${01}->${Module 1A} (${DM 24})->${PANEL_NAME_3}->${POSITION_RACK_NAME}->${ROOM_NAME}->${FLOOR_NAME}->${building_name}->${SITE_NODE}
    # + Scheduled View
        # _Service -> connected to -> Switch 01 / 01-12 -> cabled (MPO24-12xLC EB) to -> Panel 03 / 01-12 -> patched to -> Panel 02 / 01-12 -> cabled to -> Panel 01 / 01-12 -> patched to -> LC Server 01 / 01-12
    Select View Type On Trace    Scheduled
    Check Total Trace On Site Manager    5
    Check Trace Object On Site Manager    indexObject=1    objectPosition=1    objectPath=${SITE_NODE}/${BUILDING_NAME}/${FLOOR_NAME}/${ROOM_NAME}/${POSITION_RACK_NAME}/${PANEL_NAME_4}/${01}    objectType=${TRACE_GENERIC_LC_PANEL_IMAGE}
	...    connectionType=${TRACE_LC_12X_MPO24_BACK_BACK_EB_CABLING}    informationDevice=${01}->${PANEL_NAME_4}->${POSITION_RACK_NAME}->${ROOM_NAME}->${FLOOR_NAME}->${building_name}->${SITE_NODE}
    Check Trace Object On Site Manager    indexObject=2    objectPosition=1 (${MPO1})    objectPath=${SITE_NODE}/${building_name}/${FLOOR_NAME}/${ROOM_NAME}/${POSITION_RACK_NAME}/${PANEL_NAME_3}/${Module 1A} (${DM 24})/${01}    objectType=${Trace DM24}
	...    connectionType=${TRACE_FIBER_PATCHING}    scheduleIcon=${TRACE_SCHEDULED_WORK_TIME_PERIOD}    informationDevice=${01}->${Module 1A} (${DM 24})->${PANEL_NAME_3}->${POSITION_RACK_NAME}->${ROOM_NAME}->${FLOOR_NAME}->${building_name}->${SITE_NODE}    
    Check Trace Object On Site Manager    indexObject=3    objectPosition=1 (${MPO1})    objectPath=${SITE_NODE}/${building_name}/${FLOOR_NAME}/${ROOM_NAME}/${POSITION_RACK_NAME}/${PANEL_NAME_2}/${Module 1A} (${DM 24})/${01}    objectType=${Trace DM24}
	...    connectionType=${CABLE_MPO24_BACK_BACK_CABLING}    informationDevice=${01}->${Module 1A} (${DM 24})->${PANEL_NAME_2}->${POSITION_RACK_NAME}->${ROOM_NAME}->${FLOOR_NAME}->${building_name}->${SITE_NODE}
	Check Trace Object On Site Manager    indexObject=4    objectPosition=1 (${MPO1})    objectPath=${SITE_NODE}/${building_name}/${FLOOR_NAME}/${ROOM_NAME}/${POSITION_RACK_NAME}/${PANEL_NAME_1}/${Module 1A} (${DM 24})/${01}    objectType=${Trace DM24}
	...    connectionType=${TRACE_FIBER_PATCHING}    scheduleIcon=${TRACE_SCHEDULED_WORK_TIME_PERIOD}    informationDevice=${01}->${Module 1A} (${DM 24})->${PANEL_NAME_1}->${POSITION_RACK_NAME}->${ROOM_NAME}->${FLOOR_NAME}->${building_name}->${SITE_NODE}
	Check Trace Object On Site Manager    indexObject=5    objectPosition=1    objectPath=${SITE_NODE}/${building_name}/${FLOOR_NAME}/${ROOM_NAME}/${POSITION_RACK_NAME}/${SERVER_NAME}/${01}    objectType=${Trace Server Fiber}
	...    connectionType=None    informationDevice=${01}->${SERVER_NAME}->${POSITION_RACK_NAME}->${ROOM_NAME}->${FLOOR_NAME}->${building_name}->${SITE_NODE}
    Close Trace Window
        
    # 12. Open Patching window and observe the Curcuit Trace of Panel 03 / 01-12
    # * Step 12: 
        # _Service -> connected to -> Switch 01 / 01-12 -> cabled (MPO24-12xLC EB) to -> Panel 03 / 01-12 -> patched to -> Panel 02 / 01-12 -> cabled to -> Panel 01 / 01-12 -> patched to -> LC Server 01 / 01-12
    Open Patching Window    ${SITE_NODE}/${building_name}/${FLOOR_NAME}/${ROOM_NAME}/${POSITION_RACK_NAME}/${PANEL_NAME_3}/${Module 1A} (${DM 24})    ${01}
    Check Trace Object On Patching    indexObject=1    objectPosition=1    objectType=${TRACE_GENERIC_LC_PANEL_IMAGE}
	...    connectionType=${TRACE_LC_12X_MPO24_BACK_BACK_EB_CABLING}    informationDevice=${01}->${PANEL_NAME_4}->${POSITION_RACK_NAME}->${ROOM_NAME}->${FLOOR_NAME}->${building_name}->${SITE_NODE}
    Check Trace Object On Patching    indexObject=2    objectPosition=1 (${MPO1})    objectType=${Trace DM24}
	...    connectionType=${TRACE_FIBER_PATCHING}    scheduleIcon=${TRACE_SCHEDULED_WORK_TIME_PERIOD}    informationDevice=${01}->${Module 1A} (${DM 24})->${PANEL_NAME_3}->${POSITION_RACK_NAME}->${ROOM_NAME}->${FLOOR_NAME}->${building_name}->${SITE_NODE}    
    Check Trace Object On Patching    indexObject=3    objectPosition=1 (${MPO1})    objectType=${Trace DM24}
	...    connectionType=${CABLE_MPO24_BACK_BACK_CABLING}    informationDevice=${01}->${Module 1A} (${DM 24})->${PANEL_NAME_2}->${POSITION_RACK_NAME}->${ROOM_NAME}->${FLOOR_NAME}->${building_name}->${SITE_NODE}
	Check Trace Object On Patching    indexObject=4    objectPosition=1 (${MPO1})    objectType=${Trace DM24}
	...    connectionType=${TRACE_FIBER_PATCHING}    scheduleIcon=${TRACE_SCHEDULED_WORK_TIME_PERIOD}    informationDevice=${01}->${Module 1A} (${DM 24})->${PANEL_NAME_1}->${POSITION_RACK_NAME}->${ROOM_NAME}->${FLOOR_NAME}->${building_name}->${SITE_NODE}
	Check Trace Object On Patching    indexObject=5    objectPosition=1    objectType=${Trace Server Fiber}
	...    connectionType=None    informationDevice=${01}->${SERVER_NAME}->${POSITION_RACK_NAME}->${ROOM_NAME}->${FLOOR_NAME}->${building_name}->${SITE_NODE}
	Close Patching Window
	        
    # 13. Complete all WO, observe port icon status
    Open Work Order Queue Window
    Complete Work Order    ${tc_wo_name},${tc_wo_name_2}
    Close Work Order Queue
        
    # * Step 13: 
    # _Status of ports in circuit:
      # + Panel 03/ 01-12: In Use
      # + Switch 01/ 01-12: In Use
    # _Icon of ports in circuit:
      # + Panel 03/ 01-12:cabled and patched icon
      # + Switch 01/ 01-12: cabled icon
    Click Tree Node On Site Manager    ${SITE_NODE}/${building_name}/${FLOOR_NAME}/${ROOM_NAME}/${POSITION_RACK_NAME}/${PANEL_NAME_3}/${Module 1A} (${DM 24})    
    :FOR    ${i}    IN RANGE    0    len(${port_list})
    \    Check Icon Object Exist On Content Pane    ${port_list}[${i}]    ${PortFDPanelCabledInUse}  
	\    Check Table Row Map With Header On Content Table    ${Name},${Port Status}    ${port_list}[${i}],${In Use}    ${i+1}
   
    Click Tree Node On Site Manager    ${SITE_NODE}/${BUILDING_NAME}/${FLOOR_NAME}/${ROOM_NAME}/${POSITION_RACK_NAME}/${PANEL_NAME_4}    
    :FOR    ${i}    IN RANGE    0    len(${port_list})
    \    Check Icon Object Exist On Content Pane    ${port_list}[${i}]    ${PortFDPanelCabledAvailable} 
	\    Check Table Row Map With Header On Content Table    ${Name},${Port Status}    ${port_list}[${i}],${Available}    ${i+1} 
      
    # 14. Trace Panel 03 / 01-06, Switch 01 / 07-12
    # * Step 14: The trace circuit show in Vertical and Horizontal
        # _Service -> connected to -> Switch 01 / 01-12 -> cabled (MPO24-12xLC EB) to -> Panel 03 / 01-12 -> patched to -> Panel 02 / 01-12 -> cabled to -> Panel 01 / 01-12 -> patched to -> LC Server 01 / 01-12
    Open Trace Window    ${SITE_NODE}/${building_name}/${FLOOR_NAME}/${ROOM_NAME}/${POSITION_RACK_NAME}/${PANEL_NAME_3}/${Module 1A} (${DM 24})    ${01}
    Check Total Trace On Site Manager    5
    Check Trace Object On Site Manager    indexObject=1    objectPosition=1    objectPath=${SITE_NODE}/${BUILDING_NAME}/${FLOOR_NAME}/${ROOM_NAME}/${POSITION_RACK_NAME}/${PANEL_NAME_4}/${01}    objectType=${TRACE_GENERIC_LC_PANEL_IMAGE}
	...    connectionType=${TRACE_LC_12X_MPO24_BACK_BACK_EB_CABLING}    informationDevice=${01}->${PANEL_NAME_4}->${POSITION_RACK_NAME}->${ROOM_NAME}->${FLOOR_NAME}->${building_name}->${SITE_NODE}
    Check Trace Object On Site Manager    indexObject=2    objectPosition=1 (${MPO1})    objectPath=${SITE_NODE}/${building_name}/${FLOOR_NAME}/${ROOM_NAME}/${POSITION_RACK_NAME}/${PANEL_NAME_3}/${Module 1A} (${DM 24})/${01}    objectType=${Trace DM24}
	...    connectionType=${TRACE_FIBER_PATCHING}    informationDevice=${01}->${Module 1A} (${DM 24})->${PANEL_NAME_3}->${POSITION_RACK_NAME}->${ROOM_NAME}->${FLOOR_NAME}->${building_name}->${SITE_NODE}    
    Check Trace Object On Site Manager    indexObject=3    objectPosition=1 (${MPO1})    objectPath=${SITE_NODE}/${building_name}/${FLOOR_NAME}/${ROOM_NAME}/${POSITION_RACK_NAME}/${PANEL_NAME_2}/${Module 1A} (${DM 24})/${01}    objectType=${Trace DM24}
	...    connectionType=${CABLE_MPO24_BACK_BACK_CABLING}    informationDevice=${01}->${Module 1A} (${DM 24})->${PANEL_NAME_2}->${POSITION_RACK_NAME}->${ROOM_NAME}->${FLOOR_NAME}->${building_name}->${SITE_NODE}
	Check Trace Object On Site Manager    indexObject=4    objectPosition=1 (${MPO1})    objectPath=${SITE_NODE}/${building_name}/${FLOOR_NAME}/${ROOM_NAME}/${POSITION_RACK_NAME}/${PANEL_NAME_1}/${Module 1A} (${DM 24})/${01}    objectType=${Trace DM24}
	...    connectionType=${TRACE_FIBER_PATCHING}    informationDevice=${01}->${Module 1A} (${DM 24})->${PANEL_NAME_1}->${POSITION_RACK_NAME}->${ROOM_NAME}->${FLOOR_NAME}->${building_name}->${SITE_NODE}
	Check Trace Object On Site Manager    indexObject=5    objectPosition=1    objectPath=${SITE_NODE}/${building_name}/${FLOOR_NAME}/${ROOM_NAME}/${POSITION_RACK_NAME}/${SERVER_NAME}/${01}    objectType=${Trace Server Fiber}
	...    connectionType=None    informationDevice=${01}->${SERVER_NAME}->${POSITION_RACK_NAME}->${ROOM_NAME}->${FLOOR_NAME}->${building_name}->${SITE_NODE}
    Close Trace Window
        
    # 15. Go to Cabling window and remove cabling (MPO24-12xLC EB) from:_ Panel 03/Module 01/MPO 01 ->Panel 04 /  01-12
    Open Cabling Window    ${SITE_NODE}/${building_name}/${FLOOR_NAME}/${ROOM_NAME}/${POSITION_RACK_NAME}/${PANEL_NAME_4}
    Create Cabling    DisConnect    ${SITE_NODE}/${building_name}/${FLOOR_NAME}/${ROOM_NAME}/${POSITION_RACK_NAME}/${PANEL_NAME_3}/${Module 1A} (${DM 24})/${MPO} ${01}    ${SITE_NODE}/${BUILDING_NAME}/${FLOOR_NAME}/${ROOM_NAME}/${POSITION_RACK_NAME}/${PANEL_NAME_4}
    ...    mpoTab=${MPO24}    mpoType=${Mpo24_12xLC_EB}    mpoBranches=${Pair 1},${Pair 2},${Pair 3},${Pair 4},${Pair 5},${Pair 6},${Pair 7},${Pair 8},${Pair 9},${Pair 10},${Pair 11},${Pair 12}
    ...    portsTo=${01},${02},${03},${04},${05},${06},${07},${08},${09},${10},${11},${12}    
    Close Cabling Window
    
    # 16. Observe port icon and status on content pane of Panel 03
    # * Step 16: 
    # _Status of ports in circuit:
      # + Panel 03/ 01-12: In Use
      # + Panel 04/ 01-12: Available
    # _Icon of ports in circuit:
      # + Panel 03/ 01-12: uncabled and patched icon
      # + Panel 04/ 01-12: available icon
    Click Tree Node On Site Manager    ${SITE_NODE}/${building_name}/${FLOOR_NAME}/${ROOM_NAME}/${POSITION_RACK_NAME}/${PANEL_NAME_3}/${Module 1A} (${DM 24})    
    :FOR    ${i}    IN RANGE    0    len(${port_list})
    \    Check Icon Object Exist On Content Pane    ${port_list}[${i}]    ${PortFDUncabledInUse}  
	\    Check Table Row Map With Header On Content Table    ${Name},${Port Status}    ${port_list}[${i}],${In Use}    ${i+1}
	
	Click Tree Node On Site Manager    ${SITE_NODE}/${building_name}/${FLOOR_NAME}/${ROOM_NAME}/${POSITION_RACK_NAME}/${PANEL_NAME_4}    
    :FOR    ${i}    IN RANGE    0    len(${port_list})
    \    Check Icon Object Exist On Content Pane    ${port_list}[${i}]    ${PortFDUncabledAvailable}  
	\    Check Table Row Map With Header On Content Table    ${Name},${Port Status}    ${port_list}[${i}],${Available}    ${i+1}

    # 17. Trace Panel 03 / 01 and open Trace
    Open Trace Window    ${SITE_NODE}/${building_name}/${FLOOR_NAME}/${ROOM_NAME}/${POSITION_RACK_NAME}/${PANEL_NAME_3}/${Module 1A} (${DM 24})    ${01}
    Check Total Trace On Site Manager    4
    Check Trace Object On Site Manager    indexObject=1    objectPosition=1    objectPath=${SITE_NODE}/${building_name}/${FLOOR_NAME}/${ROOM_NAME}/${POSITION_RACK_NAME}/${PANEL_NAME_3}/${Module 1A} (${DM 24})/${01}    objectType=${Trace DM24}
	...    connectionType=${TRACE_FIBER_PATCHING}    informationDevice=${01}->${Module 1A} (${DM 24})->${PANEL_NAME_3}->${POSITION_RACK_NAME}->${ROOM_NAME}->${FLOOR_NAME}->${building_name}->${SITE_NODE}
	Check Trace Object On Site Manager    indexObject=2    objectPosition=1 (${MPO1})    objectPath=${SITE_NODE}/${building_name}/${FLOOR_NAME}/${ROOM_NAME}/${POSITION_RACK_NAME}/${PANEL_NAME_2}/${Module 1A} (${DM 24})/${01}    objectType=${Trace DM24}
	...    connectionType=${CABLE_MPO24_BACK_BACK_CABLING}    informationDevice=${01}->${Module 1A} (${DM 24})->${PANEL_NAME_2}->${POSITION_RACK_NAME}->${ROOM_NAME}->${FLOOR_NAME}->${building_name}->${SITE_NODE}
	Check Trace Object On Site Manager    indexObject=3    objectPosition=1 (${MPO1})    objectPath=${SITE_NODE}/${building_name}/${FLOOR_NAME}/${ROOM_NAME}/${POSITION_RACK_NAME}/${PANEL_NAME_1}/${Module 1A} (${DM 24})/${01}    objectType=${Trace DM24}
	...    connectionType=${TRACE_FIBER_PATCHING}    informationDevice=${01}->${Module 1A} (${DM 24})->${PANEL_NAME_1}->${POSITION_RACK_NAME}->${ROOM_NAME}->${FLOOR_NAME}->${building_name}->${SITE_NODE}
	Check Trace Object On Site Manager    indexObject=4    objectPosition=1    objectPath=${SITE_NODE}/${building_name}/${FLOOR_NAME}/${ROOM_NAME}/${POSITION_RACK_NAME}/${SERVER_NAME}/${01}    objectType=${Trace Server Fiber}
	...    connectionType=None    informationDevice=${01}->${SERVER_NAME}->${POSITION_RACK_NAME}->${ROOM_NAME}->${FLOOR_NAME}->${building_name}->${SITE_NODE}  