*** Settings ***
Resource    ../../../../../resources/constants.robot
Resource    ../../../../../resources/icons_constants.robot

Library    ../../../../../py_sources/logigear/setup.py
Library    ../../../../../py_sources/logigear/api/SimulatorApi.py
Library    ../../../../../py_sources/logigear/api/BuildingApi.py    ${USERNAME}    ${PASSWORD}   
Library    ../../../../../py_sources/logigear/page_objects/system_manager/site_manager/SiteManagerPage${PLATFORM}.py        
Library    ../../../../../py_sources/logigear/page_objects/system_manager/login/LoginPage${PLATFORM}.py
Library    ../../../../../py_sources/logigear/page_objects/system_manager/site_manager/connections/cabling/CablingPage${PLATFORM}.py     
Library    ../../../../../py_sources/logigear/page_objects/quareo_simulator/QuareoDevicePage${PLATFORM}.py    
Library    ../../../../../py_sources/logigear/page_objects/system_manager/site_manager/synchronization/SynchronizationPage${PLATFORM}.py   

*** Variables ***
${FLOOR_NAME}    Floor 01
${ROOM_NAME}    Room 01
${RACK_NAME}    Rack 001
${POSITION_RACK_NAME}    1:1 Rack 001
${PANEL_NAME_1}    Panel 01
${SWITCH_NAME_1}    Switch 01   

*** Test Cases *** 
SM-30337-01_Verify that the user cannot cable Quareo port with new MPO24-12xLC EB assembly

    [Tags]    Quareo Connectivity    SM-30337        

    [Setup]    Run Keywords    Set Test Variable    ${building_name}    BD_SM30337_01
    ...    AND    Set Test Variable    ${ip_address_1}    37.1.1.1
    ...    AND    Set Test Variable    ${ip_address_2}    37.1.1.2
    ...    AND    Delete Building    name=${building_name}
    ...    AND    Add New Building    ${building_name}    
    ...    AND    Delete Simulator    ${ip_address_1}
    ...    AND    Delete Simulator    ${ip_address_2}  
             
    [Teardown]    Run Keywords    Delete Building    name=${building_name}
    ...    AND    Delete Simulator    ${ip_address_1}
    ...    AND    Delete Simulator    ${ip_address_2}   
    ...    AND    Close Browser
    
    ${quareo_name_1}    Set Variable    Quareo 01
    ${quareo_name_2}    Set Variable    Quareo 02
    ${tree_node_rack}    Set Variable    ${SITE_NODE}/${building_name}/${FLOOR_NAME}/${ROOM_NAME}/${POSITION_RACK_NAME}
    
    # * On Simulator:
	# _Enable middleware config
	# _Add a LC Quareo (e.g. Q4000 Device 01 -F24LCDuplex-R24LCDuplex)
	# _Add a MPO24 Quareo (e.g. QHDEP Device 02 -F8MPO24-R8MPO24)
	
	Open Simulator Quareo Page	
	Add Simulator Quareo Device    ${ip_address_1}    deviceType=${Q4000}    modules=${F24LCDuplex-R24LCDuplex}
	Add Simulator Quareo Device    ${ip_address_2}    deviceType=${QHDEP}    modules=${F8MPO24-R8MPO24}
	Close Browser
	
    # * On SM:
	# 3. Add Quareo device in pre-condition to Rack 001 and sync successfully
	# 4. Add DM24 Panel 01 to Rack 001
	# 5. Set Static (Rear) for port: Device 01 / Module 01 / 01,  Device 02 / Module 01 / 01	 
	   
    Open SM Login Page
    Login To SM    ${USERNAME}    ${PASSWORD}    
    Add Floor    ${SITE_NODE}/${building_name}    ${FLOOR_NAME}
    Add Room    ${SITE_NODE}/${building_name}/${FLOOR_NAME}    ${ROOM_NAME}    
    Add Rack    ${SITE_NODE}/${building_name}/${FLOOR_NAME}/${ROOM_NAME}    ${RACK_NAME}
    Add Systimax Fiber Equipment    ${tree_node_rack}    ${360 G2 Fiber Shelf (1U)}    ${PANEL_NAME_1}    ${Module 1A},${True},${LC 12 Port},${DM24}	
    Add Quareo    ${tree_node_rack}    ${Q4000 1U Chassis}    ${quareo_name_1}    ${ip_address_1}
    Add Quareo    ${tree_node_rack}    ${QHDEP Chassis}    ${quareo_name_2}    ${ip_address_2}    
    Synchronize By Context Menu On Site Tree    ${tree_node_rack}/${quareo_name_1}
    Wait Until Device Synchronize Successfully    ${ip_address_1}
    Close Quareo Synchronize Window    
    Wait For Object Exist On Content Table    ${Module 01} (${Pass-Through})
    Synchronize By Context Menu On Site Tree    ${tree_node_rack}/${quareo_name_2}
    Wait Until Device Synchronize Successfully    ${ip_address_2}
    Close Quareo Synchronize Window    
    Wait For Object Exist On Content Table    ${Module 01} (${Pass-Through})
    Edit Port On Content Table    ${tree_node_rack}/${quareo_name_1}/${Module 01} (${Pass-Through})    name=${01}    staticRear=${True}
    Wait For Property On Properties Pane    ${Static (Rear)}    ${Yes}        
    Edit Port On Content Table    ${tree_node_rack}/${quareo_name_2}/${Module 01} (${Pass-Through})    name=${01}    staticRear=${True}    
    Wait For Property On Properties Pane    ${Static (Rear)}    ${Yes}

    # 6. Select Panel 01 / 01 and open Cabling window
	# 7. Select Panel 01 / MPO 1 in From Panel 
	# 8. Select Connecction type (MPO24-12xLC EB)
	# 9. Select Device 01 / Module 01 / r01 in To Panel
	# 10. Observe the state of Connect button
	# VP: The Connect button is disabled.
	# 11. Select Device 02 / Module 01 / r01
	# 12. Observe (MPO24-12xLC EB) icon in MPO24 Connection Type tab
	# VP: The new connection assembly (MPO24-12xLC EB) is disabled
	    
    Open Cabling Window    ${tree_node_rack}/${PANEL_NAME_1}/${Module 1A} (${DM24})    ${01}
    Click Tree Node Cabling    From    ${tree_node_rack}/${PANEL_NAME_1}/${Module 1A} (${DM24})/${MPO} ${01}
    Check Mpo Connection Type Information On Cabling Window    mpoConnectionTab=${MPO24}    mpoConnectionType=${Mpo24_12xLC_EB}
    Click Tree Node Cabling    To    ${tree_node_rack}/${quareo_name_1}/${Module 01} (${Pass-Through})/${r01}
    Check Connect Button State On Cabling Window    isEnabled=${False}
    Click Tree Node Cabling    From    ${tree_node_rack}/${quareo_name_2}/${Module 01} (${Pass-Through})/${r01}
    Check Mpo Connection Type State On Cabling Window    mpoConnectionType=${Mpo24_12xLC_EB}    isEnabled=${False}    mpoConnectionTab=${MPO24}    
	Close Cabling Window
