from logigear.page_objects.system_manager.GeneralPage import GeneralPage
from logigear.core.drivers import driver
from logigear.page_objects.system_manager.administration import administration
from logigear.core.elements.table_element import TableElement

class AdmObjectFieldsPageDesktop(GeneralPage):
    def __init__(self):
        GeneralPage.__init__(self)
        self.saveBtn = self.element("saveBtn")
        self.dynamicObjectFieldsChk = self.element("dynamicObjectFieldsChk")
        self.objectFieldsLabelTxt = self.element("objectFieldsLabelTxt")
        self.objectFieldsTbl = self.element("objectFieldsTbl", TableElement)
    
    def edit_object_fields_data(self, fieldIndex, fieldLabel, required=False, objectFields=None, objectFieldsStatus=True, delimiter=",", confirmSave=True):    
        driver.select_frame(self.uniqueIframe)
        administration._wait_for_sub_page_display("Object Fields")  
        
        indexRow = self.objectFieldsTbl._get_table_row_map_with_header("Index", fieldIndex)
        self.objectFieldsTbl._click_table_cell(indexRow, 2)
        
        administration.editBtn.click_enabled_element()
        
        self.objectFieldsLabelTxt.wait_until_element_is_visible()
        self.objectFieldsLabelTxt.input_text(fieldLabel)
        
        self.dynamicObjectFieldsChk.arguments = ["Required"]
        self.dynamicObjectFieldsChk.select_checkbox(required)
        
        if objectFields is not None:
            objectFieldsList = objectFields.split(delimiter) 
            for i in (objectFieldsList):
                self.dynamicObjectFieldsChk.arguments = [i]
                self.dynamicObjectFieldsChk.select_checkbox(objectFieldsStatus)
                
        if confirmSave:
            self.saveBtn.click_element()
        driver.unselect_frame() 
        self.dialogOkBtn.click_element_if_exist()
        self.dialogOkBtn.wait_until_element_is_not_visible()
        
