from logigear.page_objects.system_manager.GeneralPage import GeneralPage
from logigear.core.elements.table_element import TableElement
from logigear.page_objects.system_manager.administration import administration
from logigear.core.drivers import driver
from logigear.core.elements.element import Element
from logigear.core.elements.tree_element import TreeElement
from webtest.sel import SeleniumApp
from logigear.core.assertion import SeleniumAssert

class AdmSystemManagerUsersPageDesktop(GeneralPage):
    
    def __init__(self):
        GeneralPage.__init__(self)
        self.userTbl = self.element("userTbl", TableElement)
        self.technicianChk = self.element("technicianChk")
        self.firstNameTxt = self.element("firstNameTxt")
        self.lastNameTxt = self.element("lastNameTxt")
        self.userNameTxt = self.element("userNameTxt")
        self.passWordTxt = self.element("passWordTxt")
        self.confirmPasswordTxt = self.element("confirmPasswordTxt")
        self.userPrivilegesBtn = self.element("userPrivilegesBtn")
        self.dynamicHeaderPrivileges = self.element("dynamicHeaderPrivileges")
        self.dynamicItemPrivilegesChk = self.element("dynamicItemPrivilegesChk")
        self.locationsBtn = self.element("locationsBtn")
        self.locationTreeDiv = self.element("locationTreeDiv", TreeElement)
        self.userLDAPAuthChk = self.element("userLDAPAuthChk")
        
    def get_table_row_map_with_header_on_user_table(self, headers, values, delimiter=","):
        return self.userTbl._get_table_row_map_with_header(headers, values, delimiter)
    
    def add_user(self, firstName=None, lastName=None, userName=None, passWord=None, technician=None, userPrivileges=None, userPrivilegesItems=None, privilegesState=True, locations=None, LDAP=None, delimiter=';', delimiterTree='/'):
        self._select_iframe(self.uniqueIframe, self.userTbl)  
        administration.addBtn.click_visible_element()
        self.firstNameTxt.wait_until_element_is_enabled()
        self._fill_user_information(firstName, lastName, userName, passWord, technician, userPrivileges, userPrivilegesItems, privilegesState, locations, LDAP, delimiter, delimiterTree)
        self.confirmDialogBtn.click_visible_element()
        driver.unselect_frame()
        
    def delete_user(self,userName):
        self._select_iframe(self.uniqueIframe, self.userTbl)
        self.userTbl.wait_until_page_contains_element()
        self.userTbl.wait_for_element_outer_html_not_change()
        tableRow = self.userTbl._get_table_row_map_with_header("Username", userName)
        if tableRow > 0:
            self.userTbl._click_table_cell(tableRow,1)
            administration.deleteBtn.click_visible_element()
            self.twoChoiceYesDiv.click_visible_element()
            driver.unselect_frame()
            self.userTbl.wait_for_object_disappear_on_table("Username", userName)
        else:
            driver.unselect_frame() 
         
    def edit_user_information(self, firstName=None, lastName=None, userName=None, passWord=None, technician=None, userPrivileges=None, userPrivilegesItems=None, privilegesState=True, locations=None, LDAP=None, delimiter=';', delimiterTree='/'):
        self._select_iframe(self.uniqueIframe, self.userTbl)
        returnRow = self.get_table_row_map_with_header_on_user_table("Username", userName)
        self.userTbl._click_table_cell(returnRow,1)
        administration.editBtn.click_visible_element()
        self._fill_user_information(firstName, lastName, userName, passWord, technician, userPrivileges, userPrivilegesItems, privilegesState, locations, LDAP, delimiter, delimiterTree)
        self.confirmDialogBtn.click_visible_element()
        driver.unselect_frame()
        
    def check_user_privileges_state(self, userName, userPrivileges, userPrivilegesItems, privilegesState=True):
        self._select_iframe(self.uniqueIframe, self.userTbl)
        returnRow = self.get_table_row_map_with_header_on_user_table("Username", userName)
        self.userTbl._click_table_cell(returnRow,1)
        administration.editBtn.click_visible_element(
            ) 
        self.userPrivilegesBtn.click_visible_element()
        self.dynamicHeaderPrivileges.arguments = [userPrivileges]
        if userPrivilegesItems is not None:
            self.dynamicHeaderPrivileges.click_visible_element()
            items=userPrivilegesItems.split(',')
            for item in items:
                self.dynamicItemPrivilegesChk.arguments = [item]
                if privilegesState:
                    SeleniumAssert.checkbox_should_be_selected(self.dynamicItemPrivilegesChk)
                else:
                    SeleniumAssert.checkbox_should_not_be_selected(self.dynamicItemPrivilegesChk)
        else:
            privilegesHeaderChkXpath = self.dynamicHeaderPrivileges.locator() + "/preceding-sibling::span//input"     
            if privilegesState:
                SeleniumAssert.checkbox_should_be_selected(Element(privilegesHeaderChkXpath))
            else:
                SeleniumAssert.checkbox_should_not_be_selected(Element(privilegesHeaderChkXpath))
                
        self.confirmDialogBtn.click_visible_element()
        self.confirmDialogBtn.click_visible_element()
        driver.unselect_frame()
    
    def _fill_user_information(self, firstName=None, lastName=None, userName=None, passWord=None, technician=None, userPrivileges=None, userPrivilegesItems=None, privilegesState=True, locations=None, LDAP=None, delimiter=';', delimiterTree='/'):
        if firstName is not None:
            self.firstNameTxt.input_text(firstName)
        if lastName is not None:
            self.lastNameTxt.input_text(lastName)
        if userName is not None:    
            self.userNameTxt.input_text("")
            self.userNameTxt.input_text(userName)
        if passWord is not None:     
            self.passWordTxt.input_text(passWord)
            self.confirmPasswordTxt.input_text(passWord)
        if technician is not None:
            self.technicianChk.select_checkbox(technician)
        if LDAP is not None:
            self.userLDAPAuthChk.select_checkbox(LDAP)
        if userPrivileges is not None:
            self.userPrivilegesBtn.click_visible_element()
            self.dynamicHeaderPrivileges.arguments = [userPrivileges]
            if userPrivilegesItems is not None:
                self.dynamicHeaderPrivileges.click_visible_element()
                items=userPrivilegesItems.split(',')
                for item in items:
                    self.dynamicItemPrivilegesChk.arguments = [item]
                    self.dynamicItemPrivilegesChk.select_checkbox(privilegesState)
            else:
                privilegesHeaderChkXpath = self.dynamicHeaderPrivileges.locator() + "/preceding-sibling::span//input"     
                Element(privilegesHeaderChkXpath).select_checkbox(privilegesState)
            self.confirmDialogBtn.click_visible_element()
        if locations is not None:
            self.locationsBtn.click_visible_element()
            treeNodesList = locations.split(delimiter)
            for treeNodes in treeNodesList:
                self.locationTreeDiv.mouse_over()
                self.locationTreeDiv.click_tree_node_check_box(treeNodes, delimiterTree)
            self.confirmDialogBtn.click_visible_element()            