from logigear.page_objects.system_manager.GeneralPage import GeneralPage

class ReplaceImagePageDesktop(GeneralPage):
    
    def __init__(self):
        GeneralPage.__init__(self)
        self.replaceImagePreviewBtn = self.element("replaceImagePreviewBtn")
        self.replaceImageEditBtn = self.element("replaceImageEditBtn")
        self.replaceImageSaveBtn = self.element("replaceImageSaveBtn")
        
    def preview_and_submit_replace_image(self):
        self.replaceImagePreviewBtn.click_visible_element()
        self.replaceImageEditBtn.wait_until_element_is_visible()
        self.replaceImageSaveBtn.click_visible_element()
        self.replaceImageSaveBtn.wait_until_element_is_not_visible()