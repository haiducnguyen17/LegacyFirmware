from logigear.page_objects.system_manager.GeneralPage import GeneralPage
from logigear.core.elements.simulator_tree_element import SimulatorTreeElement
from logigear.core.elements.simulator_table_element import SimulatorTableElement
from logigear.core.assertion import SeleniumAssert, Assert

class SmSimulatorPageDesktop(GeneralPage):
    
    def __init__(self):
        GeneralPage.__init__(self)
        self.simTreeDiv = self.element("simTreeDiv", SimulatorTreeElement)
        self.centerPanelContentList = self.element("centerPanelContentList")
        self.dynamicDropdownMenuItem = self.element("dynamicDropdownMenuItem")
        self.dynamicMenuItem = self.element("dynamicMenuItem")
        self.dynamicSubMenuItem = self.element("dynamicSubMenuItem")
        self.dynamicIpV4Txt = self.element("dynamicIpV4Txt")
        self.dynamicAddChildrenTxt = self.element("dynamicAddChildrenTxt")
        self.dynamicAddChildrenBtn = self.element("dynamicAddChildrenBtn")
        self.dynamicAddChildrenCbb = self.element("dynamicAddChildrenCbb")
        self.dynamicPopupBtn = self.element("dynamicPopupBtn")
        self.contentPaneTable = self.element("contentPaneTable", SimulatorTableElement)
        self.propertiesPaneTable = self.element("propertiesPaneTable", SimulatorTableElement)
        self.contentsPaneDiv = self.element("contentsPaneDiv")
        self.propertiesPaneDiv = self.element("propertiesPaneDiv")
        self.dynamicContentsTabDiv = self.element("dynamicContentsTabDiv")
        self.dynamicSelectedContentsTabDiv = self.element("dynamicSelectedContentsTabDiv")
        
    def does_tree_node_exist_on_sm_simulator (self, treeNode, delimiter="/"):
        return self.simTreeDiv.does_tree_node_exist(treeNode, delimiter)
    
    def check_tree_node_exist_on_sm_simulator(self, treeNode, delimiter="/"):
        Assert.should_be_true(self.does_tree_node_exist_on_sm_simulator(treeNode, delimiter), treeNode + " does not exist")
        
    def check_tree_node_not_exist_on_sm_simulator(self, treeNode, delimiter="/"):
        Assert.should_not_be_true(self.does_tree_node_exist_on_sm_simulator(treeNode, delimiter), treeNode + " still exist")
        
    def click_tree_node_on_sm_simulator(self, treeNode, delimiter="/"):
        self.simTreeDiv.click_tree_node(treeNode, delimiter)
        self.centerPanelContentList.wait_for_element_outer_html_not_change()
        
    def check_dropdown_menu_item_exist_on_main_menu(self, menuName):
        self.dynamicDropdownMenuItem.arguments = [menuName]
        SeleniumAssert.element_should_be_visible(self.dynamicDropdownMenuItem)

    def check_menu_item_exist_on_main_menu(self, menuName):
        self.dynamicMenuItem.arguments = [menuName]
        SeleniumAssert.element_should_be_visible(self.dynamicMenuItem)
        
    def check_sub_menu_item_exist(self, menuName, subItemName):
        self.dynamicDropdownMenuItem.arguments = [menuName]
        self.dynamicSubMenuItem.arguments = [menuName, subItemName]
        self.dynamicDropdownMenuItem.mouse_over()
        self.centerPanelContentList.wait_for_element_outer_html_not_change()
        SeleniumAssert.element_should_be_visible(self.dynamicSubMenuItem)
        
    def select_sub_menu_item(self, menuName, subItemName):
        self.dynamicDropdownMenuItem.arguments = [menuName]
        self.dynamicSubMenuItem.arguments = [menuName, subItemName]
        
        self.dynamicDropdownMenuItem.mouse_over()
        self.dynamicSubMenuItem.click_visible_element()
        self.dynamicSubMenuItem.wait_until_element_is_not_visible()
        
    def _fill_ipv4 (self, ipAddress):
        subIp = ipAddress.split(".")
        tempCount = 1
        for sub in subIp:
            self.dynamicIpV4Txt.arguments = [tempCount]
            self.dynamicIpV4Txt.click_element()
            self.dynamicIpV4Txt.input_text(sub)
            tempCount = tempCount + 1
    
    def check_fw_version_textbox_exist(self):
        self.dynamicAddChildrenTxt.arguments = ['FWVersion']
        SeleniumAssert.element_should_be_visible(self.dynamicAddChildrenTxt)
        
    def check_serial_number_textbox_exist(self):
        self.dynamicAddChildrenTxt.arguments = ['SerialNumber']
        SeleniumAssert.element_should_be_visible(self.dynamicAddChildrenTxt)
        
    def check_panel_serial_number_textbox_exist(self):
        self.dynamicAddChildrenTxt.arguments = ['Panel SerialNumber']
        SeleniumAssert.element_should_be_visible(self.dynamicAddChildrenTxt)
        
    def add_children_for_object_site(self, ipAddress, fwVersion, nmVersion, communication):
        self._fill_ipv4(ipAddress)
        self.dynamicAddChildrenTxt.arguments = ['FWVersion']
        self.dynamicAddChildrenTxt.input_text(fwVersion)
        
        self.dynamicAddChildrenTxt.arguments = ['NMVersion']
        self.dynamicAddChildrenTxt.input_text(nmVersion)
        
        self.dynamicAddChildrenTxt.arguments = ['Communication']
        self.dynamicAddChildrenTxt.input_text(communication)
        
        self.click_add_children_submit_button()
        self.dynamicAddChildrenBtn.wait_until_element_is_not_visible()
        
    def click_add_children_submit_button(self):
        self.dynamicAddChildrenBtn.arguments = ['Submit']
        self.dynamicAddChildrenBtn.click_visible_element()
        
    def delete_tree_node_on_sm_simulator(self, treeNode, delimiter="/"):
        self.click_tree_node_on_sm_simulator(treeNode, delimiter)
        self.select_sub_menu_item('Manage', 'Delete')
        self.dynamicPopupBtn.arguments = ['OK']
        self.dynamicPopupBtn.click_visible_element()
        self.dynamicPopupBtn.wait_until_element_is_not_visible()
        
    def delete_tree_node_if_exist_on_sm_simulator(self, treeNode, delimiter="/"):
        isExist = self.does_tree_node_exist_on_sm_simulator(treeNode, delimiter)
        if isExist:
            self.delete_tree_node_on_sm_simulator(treeNode, delimiter)
        
    def get_table_row_map_with_header_on_sm_simulator_content_table(self, headers, values, delimiter=","):
        return self.contentPaneTable._get_table_row_map_with_header(headers, values, delimiter)
    
    def check_table_row_map_with_header_on_sm_simulator_content_table(self, headers, values, expected):
        Assert.should_be_equal_as_integers(self.get_table_row_map_with_header_on_sm_simulator_content_table(headers, values), int(expected))
        
    def get_table_row_map_with_header_on_sm_simulator_properties_table(self, headers, values, delimiter=","):
        return self.propertiesPaneTable._get_table_row_map_with_header(headers, values, delimiter)
    
    def check_table_row_map_with_header_on_sm_simulator_properties_table(self, headers, values, expected):
        Assert.should_be_equal_as_integers(self.get_table_row_map_with_header_on_sm_simulator_properties_table(headers, values), int(expected))
        
    def check_sim_site_tree_exist(self):
        SeleniumAssert.element_should_be_visible(self.simTreeDiv)
    
    def check_sim_contents_pane_exist(self):
        SeleniumAssert.element_should_be_visible(self.contentsPaneDiv)
    
    def check_sim_properties_pane_exist(self):
        SeleniumAssert.element_should_be_visible(self.propertiesPaneDiv)
        
    def select_sim_contents_tab(self, tabName):
        self.dynamicContentsTabDiv.arguments = [tabName]
        self.dynamicSelectedContentsTabDiv.arguments = [tabName]
        self.dynamicContentsTabDiv.click_visible_element()
        SeleniumAssert.element_should_be_visible(self.dynamicSelectedContentsTabDiv)
        
    def select_menu_item_on_main_menu(self, menuName):
        """ menuName = Synchronize or Help"""
        self.dynamicMenuItem.arguments = [menuName]
        self.dynamicMenuItem.click_visible_element()