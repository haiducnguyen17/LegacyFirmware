from logigear.core import browserManagementKeywords
# from selenium import webdriver
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from logigear.core.drivers.browser.base_driver import BaseDriver
from webdriver_manager.microsoft import EdgeChromiumDriverManager

class EdgeDriver(BaseDriver):

    def create_driver(self, config, alias=None):
#         edge_options = webdriver.EdgeOptions()
        capabilities = DesiredCapabilities.EDGE.copy()
        
#         for argument in config.arguments:
#             edge_options.add_argument(argument)
        
        capabilities.update(config.capabilities)
        driver_path = EdgeChromiumDriverManager().install()

        browserManagementKeywords.create_webdriver(
            "Edge", alias, executable_path=driver_path, desired_capabilities=capabilities)
#             options=edge_options, desired_capabilities=capabilities)
